import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RolesListComponent } from './roles-list/roles-list.component';
import { ViewRoleComponent } from './view-role/view-role.component';

const routes: Routes = [
  {
      path: '',
      component: RolesListComponent,
      data: {
          title: 'Roles List'
      }
  },
  {
      path: ':id',
      component: ViewRoleComponent,
      data: {
          breadcrumb: 'View Role',
          title: 'Role'
      }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RolesRoutingModule { }

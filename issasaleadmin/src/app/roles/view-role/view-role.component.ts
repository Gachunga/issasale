import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../../shared/services/http.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-view-role',
  templateUrl: './view-role.component.html',
  styleUrls: ['./view-role.component.scss']
})
export class ViewRoleComponent implements OnInit {
  public _parameters: any;
  public _id: any;
  public data: any;
  constructor(private _activatedRoute: ActivatedRoute, private _httpService: HttpService, public toastrService: ToastrService) { }

  ngOnInit() {
    this._parameters = this._activatedRoute.params.subscribe(params => {
      if (typeof params['id'] !== 'undefined') {
        this._id = params['id'];
      }
    });
    this.loadData();
  }
  private loadData(): any {
    this._httpService.get('roles/'+this._id).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.data = result.data;
          this.data = this.data;
        } else {
          this.toastrService.error(result.Message);
        }
      },
      error => {
        console.log(error)
        this.toastrService.error();
      },
      complete => {
      }
    );
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { RolesRoutingModule } from './roles-routing.module';
import { RolesListComponent } from './roles-list/roles-list.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { AddRoleComponent } from './add-role/add-role.component';
import { ViewRoleComponent } from './view-role/view-role.component';


@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    RolesRoutingModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    RolesListComponent,
    AddRoleComponent,
    ViewRoleComponent,
  ],
  entryComponents: [
    AddRoleComponent
  ],
})
export class RolesModule { }

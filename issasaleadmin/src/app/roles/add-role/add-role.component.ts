import { HttpService } from 'src/app/shared/services/http.service';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-role',
  templateUrl: './add-role.component.html',
  styleUrls: ['./add-role.component.scss']
})
export class AddRoleComponent implements OnInit {

  @Input() title;
  @Input() formData;
  public loading = false;
  public hasErrors = false;
  public errorMessages;
  public form: FormGroup;
  public permissionsData;
  constructor(
    public activeModal: NgbActiveModal,
    public fb: FormBuilder,
    private _httpService: HttpService,
    public toastrService: ToastrService) { }
  ngOnInit() {
    this.loadPermissions();
    this.form = this.fb.group({
      name: [
        this.formData ? this.formData.name : '', Validators.compose([Validators.required])],
      permissions: new FormArray([], this.minSelectedCheckboxes(1)),
    });
  }
  get permissionsFromArray() {
    return this.form.controls.permissions as FormArray;
  }
  private addCheckboxes() {
    this.permissionsData.forEach(() => this.permissionsFromArray.push(new FormControl(false)));
  }
  public submitData(): void {
    this.loading = true;
    const selectedPermissionIds = this.form.value.permissions
      .map((checked, i) => checked ? this.permissionsData[i].id : null)
      .filter(v => v !== null);
    
    this._httpService.post('roles', {name : this.form.value.name, permissions : selectedPermissionIds}).subscribe(
      result => {
        if (result.success) {
          this.toastrService.success('Record created successfully!', 'Created Successfully!');
          this.activeModal.close('success');
        } else {
          this.handleErrorsFromServer(result);
        }
      },
      error => {
        this.errorMessages = error.error.error_messages;
      },
      complete => {
        this.loading = false;
      }

    );
  }
  public saveChanges(): void {
    this.loading = true;
    this.form.value.id = this.formData.id;
    this._httpService.post('clients/update', this.form.value).subscribe(
      result => {
        if (result.response.response_status.response_code === 200) {
          this.toastrService.success('Changes saved!', 'Saved Successfully!');
          this.activeModal.close('success');
        } else {
          this.handleErrorsFromServer(result);
        }
      },
      error => {
        this.errorMessages = error.error.error_messages;
      },
      complete => {
        this.loading = false;
      }

    );
  }
  public handleErrorsFromServer(response: any) {
    this.loading = false;
    this.hasErrors = true;
    this.errorMessages = [];
    Object.entries(response.response.error).forEach(
      ([key, value]) => // console.log(key, value)
        this.errorMessages.push(value)
    );
  }

  public closeModal(): void {
    this.activeModal.dismiss('Cross click');
  }
 private loadPermissions() {
  this._httpService.get('permissions').subscribe(
    result => {
      if (result.success) {
        this.permissionsData = result.data;
        this.addCheckboxes();
      } else {
      }
    },
    error => {
    },
    complete => {
    }
  );
 }
 public minSelectedCheckboxes(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      // get a list of checkbox values (boolean)
      .map(control => control.value)
      // total up the number of checked checkboxes
      .reduce((prev, next) => next ? prev + next : prev, 0);

    // if the total is not greater than the minimum, return the error message
    return totalSelected >= min ? null : { required: true };
  };

  return validator;
}
}

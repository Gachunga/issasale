import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrandsListComponent } from './brands-list/brands-list.component';
import { ViewBrandComponent } from './view-brand/view-brand.component';


const routes: Routes = [
  {
    path: '',
    component: BrandsListComponent,
    data: {
        title: 'Brands List'
    }
},
{
    path: ':id',
    component: ViewBrandComponent,
    data: {
        breadcrumb: 'View Brand',
        title: 'User'
    }
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BrandRoutingModule { }

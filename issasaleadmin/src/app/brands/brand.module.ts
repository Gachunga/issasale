import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { BrandsListComponent } from './brands-list/brands-list.component';
import { AddBrandComponent } from './add-brand/add-brand.component';
import { ViewBrandComponent } from './view-brand/view-brand.component';
import { BrandRoutingModule } from './brand-routing.module';


@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    BrandRoutingModule,
    Ng2SmartTableModule,
    NgxDropzoneModule
  ],
  declarations: [
    BrandsListComponent,
    AddBrandComponent,
    ViewBrandComponent,
  ],
  entryComponents: [
    AddBrandComponent
  ],
})
export class BrandsModule { }

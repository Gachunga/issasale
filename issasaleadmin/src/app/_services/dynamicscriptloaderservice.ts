import { Injectable } from '@angular/core';

interface Scripts {
    name: string;
    src: string;
}

export const ScriptStore: Scripts[] = [
{ name: 'dropzone', src: '../../assets/vendors/js/extensions/dropzone.min.js'},
{ name: 'datatablesmin', src: '../../assets/vendors/js/tables/datatable/datatables.min.js'},
{ name: 'datatablesbuttons', src: '../../assets/vendors/js/tables/datatable/datatables.buttons.min.js'},
{ name: 'datatablesbootstrap4', src: '../../assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js'},
{ name: 'buttonsbootstrap', src: '../../assets/vendors/js/tables/datatable/buttons.bootstrap.min.js'},
{ name: 'dataTablesselect', src: '../../assets/vendors/js/tables/datatable/dataTables.select.min.js'},
{ name: 'datatablescheckboxes', src: '../../assets/vendors/js/tables/datatable/datatables.checkboxes.min.js'},
{ name: 'datalistview', src: '../../assets/js/scripts/ui/data-list-view.min.js'},
];

declare var document: any;

@Injectable({
    providedIn: 'root'
})
export class DynamicScriptLoaderService {

    private scripts: any = {};

    constructor() {
        ScriptStore.forEach((script: any) => {
            this.scripts[script.name] = {
                loaded: false,
                src: script.src
            };
        });
    }

    load(...scripts: string[]) {
        const promises: any[] = [];
        scripts.forEach((script) => promises.push(this.loadScript(script)));
        return Promise.all(promises);
    }

    loadScript(name: string) {
        return new Promise((resolve, reject) => {
            if (!this.scripts[name].loaded) {
                // load script
                const script = document.createElement('script');
                script.type = 'text/javascript';
                script.src = this.scripts[name].src;
                console.log(script.src)
                if (script.readyState) {  // IE
                    script.onreadystatechange = () => {
                        if (script.readyState === 'loaded' || script.readyState === 'complete') {
                            script.onreadystatechange = null;
                            this.scripts[name].loaded = true;
                            resolve({ script: name, loaded: true, status: 'Loaded' });
                        }
                    };
                } else {  // Others
                    script.onload = () => {
                        this.scripts[name].loaded = true;
                        resolve({ script: name, loaded: true, status: 'Loaded' });
                    };
                }
                script.onerror = (error: any) => resolve({ script: name, loaded: false, status: 'Loaded' });
                document.getElementsByTagName('head')[0].appendChild(script);
            } else {
                resolve({ script: name, loaded: true, status: 'Already Loaded' });
            }
        });
    }

}
import { DatePipe } from '@angular/common';
import { HttpService } from 'src/app/shared/services/http.service';
import { Component, OnInit } from '@angular/core';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AddComponent } from '../add/add.component';
import { NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { DynamicScriptLoaderService } from '../../_services/dynamicscriptloaderservice';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { GlobalService } from '../../shared/services/global.service';
import {TreeNode} from 'primeng/api';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [DatePipe]
})
export class ListComponent implements OnInit {
  public formData;
  public modalRef: NgbModalRef;
  public brandsData = [];
  public form: FormGroup;
  public loading = false;
  public hasErrors = false;
  public errorMessages;
  public url = '';
  files: File[] = [];
  public categories: TreeNode[];
  public category_id = 0;
  public showsidepanel = false
  public fetching=false
  public brands = []
  items: FormArray;
  public discount_types = [];
  sizes: FormArray;
 
  onSelect(event) {
    console.log(event);
    this.files.push(...event.addedFiles);
  }
  
  onRemove(event) {
    this.files.splice(this.files.indexOf(event), 1);
  }

  constructor(config: NgbModalConfig, private _httpService: HttpService, private modalService: NgbModal, private router: Router,
    public datePipe: DatePipe, public toastrService: ToastrService, public fb: FormBuilder,  private _globalService: GlobalService) {
       // customize default values of modals used by this component tree
    config.backdrop = 'static';
    config.keyboard = false;
    }
  ngOnInit() {
    this.url = this._globalService.imageHost
    this.loadData();
    this.loadScripts();

    this.form = this.fb.group({
      title: [
        this.formData ? this.formData.title : '', Validators.compose([Validators.required])],
      code: [
        this.formData ? this.formData.code : '',],
      description: [
        this.formData ? this.formData.description : '', Validators.compose([Validators.required])],
      taxable: [
        this.formData ? this.formData.taxable : false,],
      brand_id: [
        this.formData ? this.formData.brand_id : '', Validators.compose([Validators.required])],
      price: [
        this.formData ? this.formData.price : '', Validators.compose([Validators.required])],
      previous_price: [
        this.formData ? this.formData.previous_price : '' ],
        discount_type_id: [
        this.formData ? this.formData.discount_type_id : '', ],
      discount: [
        this.formData ? this.formData.discount : '', ],
      items: this.fb.array([  ]),
      sizes: this.fb.array([ ])
    });
  }
  createItem(): FormGroup {
    return this.fb.group({
      name: '',
      color: '',
      color_input : ''
    });
  }
  createItemSize(): FormGroup {
    return this.fb.group({
      id : '',
      size: '',
    });
  }
  addItem(): void {
    this.items = this.form.get('items') as FormArray;
    this.items.push(this.createItem());
  }
  addSize(): void {
    this.sizes = this.form.get('sizes') as FormArray;
    this.sizes.push(this.createItemSize());
  }
  removeItem(index) : void {
    this.items.removeAt(this.items.value.findIndex(item => item.id === index))
  }
  getControls() {
    return (this.form.get('items') as FormArray).controls;
  }
  getSizeControls() {
    return (this.form.get('sizes') as FormArray).controls;
  }
  removeSizeItem(index) : void {
    this.sizes.removeAt(this.sizes.value.findIndex(size => size.id === index))
  }
  private loadData(): any {
    this.fetching = true
    this._httpService.get(`categories`).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.categories = result.data;
        } else {
          this.toastrService.error(result.message);
        }
      },
      error => {
      },
      complete => {
        this.fetching = false
      }
    );
    this._httpService.get(`brands`).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.brands = result.data;
        } else {
          this.toastrService.error(result.message);
        }
      },
      error => {
      },
      complete => {
      }
    );
    this._httpService.get(`discount_types`).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.discount_types = result.data;
        } else {
          this.toastrService.error(result.message);
        }
      },
      error => {
      },
      complete => {
      }
    );
  }
  public submitData(): void {
    this.loading = true;
    let formData  = new FormData();
    let result = Object.assign({}, this.form.value);
    let color_items = []
    let items = this.getControls()
    var arrayControl = this.form.get('items') as FormArray;
    for (let i in items) {
      //@ts-ignore
      let color = arrayControl.at(parseInt(i)).color
      let value = arrayControl.at(parseInt(i)).value
      let color_item = {color : color, name : value.name}
      formData.append('colors[]', JSON.stringify(color_item));
    }
    let sizes = this.getSizeControls()
    var sizearrayControl = this.form.get('sizes') as FormArray;
    for (let i in sizes) {
      //@ts-ignore
      let value = sizearrayControl.at(parseInt(i)).value
      formData.append('sizes_arr[]', JSON.stringify(value.size));
    }
    for (let o in result) {
      formData.append(o, result[o])
    }
    for (var i = 0; i < this.files.length; i++) { 
      formData.append("images[]", this.files[i]);
    }
    formData.append("category_id", (this.category_id).toString())
    
    this._httpService.post('products', formData).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.toastrService.success('Record created successfully!', 'Created Successfully!');
          this.loadData()
          this.files = []
          this.form.reset()
        } else {
          this.handleErrorsFromServer(result);
        }
      },
      error => {
        this.loading = false;
        this.errorMessages = error.error.error_messages;
      },
      complete => {
        this.loading = false;
      }

    );
  }
  public handleErrorsFromServer(response: any) {
    this.loading = false;
    this.hasErrors = true;
    this.errorMessages = [];
    console.log(response.errors)
    Object.entries(response.errors).forEach(
      ([key, value]) => // console.log(key, value)
        this.errorMessages.push(value)
    );
  }
  public onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this._httpService.delete('pims/commodity/' + event.data.id).subscribe(
        result => {
          if (result.response_code === 200) {
            event.confirm.resolve();
            this.toastrService.success(event.data.id, 'Deleted!');
          } else {
            this.toastrService.error(event.data.id, 'Failed to Delete!');
          }
        }
      );
    } else {
      event.confirm.reject();
    }
  }
  private viewRecord(data: any) {
    this.router.navigate(['roles', data.id]);
  }
  public onCustomAction(event: any): void {
    console.log(event);
    switch (event.action) {
      case 'viewRecord':
        this.viewRecord(event.data);
        break;
      case 'editRecord':
        // this.openModal(event.data);
        break;
      default:
        break;
    }
  }
  public openModal(id) {
    this.modalRef = this.modalService.open(AddComponent);
    this.modalRef.componentInstance.title = 'Add Category';
    this.modalRef.componentInstance.parent_id = id;
    
    this.modalRef.result.then((result) => {
      if (result === 'success') {
        this.loadData();
      }
    }, (reason) => {
    });
  }
  public toggleProductModal(cat_id){
    if(cat_id) this.category_id = cat_id 
    this.showsidepanel ? this.showsidepanel=false : this.showsidepanel=true
    console.log(this.showsidepanel)
  }
  
  public addProduct(): void {
    this.loading = true;
    let formData  = new FormData();
    let result = Object.assign({}, this.form.value);
      for (let o in result) {
        formData.append(o, result[o])
      }
    for (var i = 0; i < this.files.length; i++) { 
      formData.append("file[]", this.files[i]);
    }
    
    this._httpService.post('brands', formData).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.toastrService.success('Record created successfully!', 'Created Successfully!');
          this.loadData()
          this.files = []
          this.form.reset()
        } else {
          this.handleErrorsFromServer(result);
        }
      },
      error => {
        this.loading = false;
        this.errorMessages = error.error.error_messages;
      },
      complete => {
        this.loading = false;
      }

    );
  }
  private loadScripts() {
    const dynamicScripts = [
      'assets/vendors/js/extensions/dropzone.min.js',
      'assets/vendors/js/tables/datatable/datatables.min.js',
      'assets/vendors/js/tables/datatable/datatables.buttons.min.js',
      'assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js',
      'assets/vendors/js/tables/datatable/buttons.bootstrap.min.js',
      'assets/vendors/js/tables/datatable/dataTables.select.min.js',
      'assets/vendors/js/tables/datatable/datatables.checkboxes.min.js',
      'assets/js/scripts/ui/data-list-view.min.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }
} 


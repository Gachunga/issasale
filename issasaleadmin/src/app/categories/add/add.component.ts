import { HttpService } from 'src/app/shared/services/http.service';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  @Input() title;
  @Input() formData;
  @Input() parent_id;

  public loading = false;
  public hasErrors = false;
  public errorMessages;
  public form: FormGroup;
  public rolesData;
  public fetching : boolean =false
  files: File[] = [];

  constructor(
    public activeModal: NgbActiveModal,
    public fb: FormBuilder,
    private _httpService: HttpService,
    public toastrService: ToastrService) { }
  ngOnInit() {
    this.form = this.fb.group({
      title: [
        this.formData ? this.formData.title : '', Validators.compose([Validators.required])],
      caption: [
        this.formData ? this.formData.caption : '', ],
    });
  }
  onSelect(event) {
    console.log(event);
    this.files.push(...event.addedFiles);
  }
  
  onRemove(event) {
    this.files.splice(this.files.indexOf(event), 1);
  }
  public submitData(): void {
    this.loading = true;
    let formData  = new FormData();
    let result = Object.assign({}, this.form.value);
      for (let o in result) {
        formData.append(o, result[o])
      }
    for (var i = 0; i < this.files.length; i++) { 
      formData.append("file[]", this.files[i]);
    }
    formData.append('parent_id', this.parent_id)
    this._httpService.post('categories', formData).subscribe(
      result => {
        console.log(result)
        if (result.success) {
            this.toastrService.success('Record created successfully!', 'Created Successfully!');
            this.activeModal.close('success');
            this.files = []
            this.form.reset()
        } else {
          this.handleErrorsFromServer(result);
        }
      },
      error => {
        this.loading = false;
        this.errorMessages = error.error.error_messages;
      },
      complete => {
        this.loading = false;
      }

    );
  }
  public saveChanges(): void {
    this.loading = true;
    this.form.value.id = this.formData.id;
    this._httpService.post('clients/update', this.form.value).subscribe(
      result => {
        if (result.response.response_status.response_code === 200) {
          this.toastrService.success('Changes saved!', 'Saved Successfully!');
          this.activeModal.close('success');
        } else {
          this.handleErrorsFromServer(result);
        }
      },
      error => {
        this.errorMessages = error.error.error_messages;
      },
      complete => {
        this.loading = false;
      }

    );
  }
  public handleErrorsFromServer(response: any) {
    this.loading = false;
    this.hasErrors = true;
    this.errorMessages = [];
    console.log(response.errors)
    Object.entries(response.errors).forEach(
      ([key, value]) => // console.log(key, value)
        this.errorMessages.push(value)
    );
  }

  public closeModal(): void {
    this.activeModal.dismiss('Cross click');
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';


const routes: Routes = [
  {
    path: '',
    component: ListComponent,
    data: {
        title: 'Category List'
    }
},
{
    path: ':id',
    component: ViewComponent,
    data: {
        breadcrumb: 'View Category',
        title: 'Category'
    }
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { ListComponent } from './list/list.component';
import { AddComponent } from './add/add.component';
import { ViewComponent } from './view/view.component';
import { CategoryRoutingModule } from './categories-routing.module';
import { ColorPickerModule } from 'ngx-color-picker';

import {TreeModule} from 'primeng/tree';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    CategoryRoutingModule,
    Ng2SmartTableModule,
    NgxDropzoneModule,
    TreeModule,
    ColorPickerModule
  ],
  declarations: [
    ListComponent,
    AddComponent,
    ViewComponent,
  ],
  entryComponents: [
    AddComponent
  ],
})
export class CategoriesModule { }

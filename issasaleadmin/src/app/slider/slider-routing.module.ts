import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SliderListComponent } from './slider-list/slider-list.component';


const routes: Routes = [
  {
    path: '',
    component: SliderListComponent,
    data: {
        title: 'Brands List'
    }
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SliderRoutingModule { }

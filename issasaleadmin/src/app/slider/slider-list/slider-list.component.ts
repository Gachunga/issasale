import { DatePipe } from '@angular/common';
import { HttpService } from 'src/app/shared/services/http.service';
import { Component, OnInit } from '@angular/core';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { DynamicScriptLoaderService } from '../../_services/dynamicscriptloaderservice';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { GlobalService } from '../../shared/services/global.service';
import {  SwiperConfigInterface } from 'ngx-swiper-wrapper';
declare var $: any;

@Component({
  selector: 'app-slider-list',
  templateUrl: './slider-list.component.html',
  styleUrls: ['./slider-list.component.css'],
  providers: [DatePipe]
})
export class SliderListComponent implements OnInit {
  public formData;
  public modalRef: NgbModalRef;
  public slidersData = [];
  public form: FormGroup;
  public loading = false;
  public hasErrors = false;
  public errorMessages;
  public url = '';
  files: File[] = [];
  scripts_loaded = false;
  public color : any;
  public showsidepanel = false
  public showeditsidepanel = false;
  public edit = false
  public edit_slider;
  
  public config: SwiperConfigInterface = {
    a11y: true,
    direction: 'horizontal',
    slidesPerView: 1,
    keyboard: true,
    mousewheel: true,
    scrollbar: false,
    navigation: true,
    pagination: false
  };

  onSelect(event) {
    console.log(event);
    this.files.push(...event.addedFiles);
  }
  
  onRemove(event) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }
  constructor(private _httpService: HttpService, private modalService: NgbModal, private router: Router,
    public datePipe: DatePipe, public toastrService: ToastrService, public fb: FormBuilder,  private _globalService: GlobalService) {
       // customize default values of modals used by this component tree
    }
  ngOnInit() {
    this.url = this._globalService.imageHost
    this.loadData();
    this.form = this.fb.group({
      title_caption: [
        this.formData ? this.formData.name : '',],
      title: [
        this.formData ? this.formData.name : '', Validators.compose([Validators.required])],
      title_desc: [
        this.formData ? this.formData.title_desc : '',],
      level: [
        this.formData ? this.formData.level : '', Validators.compose([Validators.required])],
      btn_text: [
        this.formData ? this.formData.btn_text : '', Validators.compose([Validators.required])],
      date_from: [
        this.formData ? this.formData.date_from : '',],
      date_to: [
        this.formData ? this.formData.date_to : '',],
      discount: [
        this.formData ? this.formData.discount : '',],
    });
  }
  public toggleSliderModal(cat_id){
    this.form.reset() 
    this.showsidepanel ? this.showsidepanel=false : this.showsidepanel=true
  }
  public toggleEdit(slider){
    this.edit_slider = slider
    console.log(this.edit, this.showeditsidepanel)
    this.edit ? this.edit=false : this.edit=true
    this.showsidepanel ? this.showsidepanel=false : this.showsidepanel=true
    this.form.patchValue({title : slider.title ? slider.title : ""})
    this.form.patchValue({title_caption: slider.title_caption})
    this.form.patchValue({title_desc: slider.title_desc!="null" ? slider.title_desc : ""})
    this.form.patchValue({level : slider.level})
    this.form.patchValue({date_from : slider.date_from})
    this.form.patchValue({date_to : slider.date_to})
    this.form.patchValue({discount : slider.discount})
    this.form.patchValue({btn_text : slider.btn_text})
    this.color = slider.background_color;
  }
  private loadData(): any {
    this._httpService.get(`slider_images`).subscribe(
      result => {
        if (result.success) {
          this.slidersData = result.data;
          if(!this.scripts_loaded){
            this.loadScripts();
          }
          
        } else {
          this.toastrService.error(result.message);
        }
      },
      error => {
      },
      complete => {
      }
    );
  }
  public submitData(): void {
    this.loading = true;
    let formData  = new FormData();
    let result = Object.assign({}, this.form.value);
      for (let o in result) {
        formData.append(o, result[o])
      }
    for (var i = 0; i < this.files.length; i++) { 
      formData.append("file[]", this.files[i]);
    }
    formData.append('background_color', this.color.toString())
    
    this._httpService.post('slider_images', formData).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.toastrService.success('Record created successfully!', 'Created Successfully!');
          this.loadData()
          this.files = []
          this.form.reset()
        } else {
          this.handleErrorsFromServer(result);
        }
      },
      error => {
        this.loading = false;
        this.errorMessages = error.error.error_messages;
      },
      complete => {
        this.loading = false;
        this.showsidepanel = false
      }

    );
  }
  public saveData(slider_id){
    var date_from = ($('#data-date-from').val())
    var date_to = ($('#data-date-to').val())
    this.loading = true;
    let formData  = new FormData();
    let result = Object.assign({}, this.form.value);
      for (let o in result) {
        formData.append(o, result[o])
      }
    for (var i = 0; i < this.files.length; i++) { 
      formData.append("file[]", this.files[i]);
    }
    if(this.color){
      formData.append('background_color', this.color.toString())
    }
    formData.append("date_from", date_from);
    formData.append("date_to", date_to);
    
    this._httpService.post('slider_images/update/'+slider_id, formData).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.toastrService.success('Record updated successfully!', 'Updated Successfully!');
          this.edit=false
          this.showsidepanel=false
          this.loadData()
          this.files = []
          this.form.reset()
          this.loading = false;
        } else {
          this.handleErrorsFromServer(result);
        }
      },
      error => {
        this.loading = false;
        this.errorMessages = error.error.error_messages;
      },
      complete => {
        this.loading = false;
      }

    );
  }
  public handleErrorsFromServer(response: any) {
    this.loading = false;
    this.hasErrors = true;
    this.errorMessages = [];
    console.log(response.errors)
    Object.entries(response.errors).forEach(
      ([key, value]) => // console.log(key, value)
        this.errorMessages.push(value)
    );
  }
  public onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this._httpService.delete('pims/commodity/' + event.data.id).subscribe(
        result => {
          if (result.response_code === 200) {
            event.confirm.resolve();
            this.toastrService.success(event.data.id, 'Deleted!');
          } else {
            this.toastrService.error(event.data.id, 'Failed to Delete!');
          }
        }
      );
    } else {
      event.confirm.reject();
    }
  }
  private viewRecord(data: any) {
    this.router.navigate(['roles', data.id]);
  }
  public onCustomAction(event: any): void {
    console.log(event);
    switch (event.action) {
      case 'viewRecord':
        this.viewRecord(event.data);
        break;
      case 'editRecord':
        // this.openModal(event.data);
        break;
      default:
        break;
    }
  }
  private loadScripts() {
    const dynamicScripts = [
      'assets/vendors/js/extensions/dropzone.min.js',
      'assets/vendors/js/tables/datatable/datatables.min.js',
      'assets/vendors/js/tables/datatable/datatables.buttons.min.js',
      'assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js',
      'assets/vendors/js/tables/datatable/buttons.bootstrap.min.js',
      'assets/vendors/js/tables/datatable/dataTables.select.min.js',
      'assets/vendors/js/tables/datatable/datatables.checkboxes.min.js',
      'assets/js/scripts/pickers/dateTime/pick-a-datetime.min.js',
      'assets/js/scripts/ui/data-list-view.min.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
    this.scripts_loaded = true
  }
} 


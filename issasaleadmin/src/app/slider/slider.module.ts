import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { SliderRoutingModule } from './slider-routing.module';
import { SliderListComponent } from './slider-list/slider-list.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { ColorPickerModule } from 'ngx-color-picker';
import { SwiperModule } from 'ngx-swiper-wrapper';

@NgModule({
  declarations: [
    SliderListComponent
  ],
  imports: [
    CommonModule,
    SliderRoutingModule,
    SharedModule,
    NgxDropzoneModule,
    ColorPickerModule,
    SwiperModule
  ]
})
export class SliderModule { }

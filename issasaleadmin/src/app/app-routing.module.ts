import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './pages/errors/not-found/not-found.component';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { AuthGuard } from './_helpers';
import { ColorsComponent } from './products/colors/colors.component';
import { SizesComponent } from './products/sizes/sizes.component';

const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'permissions', loadChildren: () => import('./permissions/permissions.module').then(m => m.PermissionsModule) },
    { path: 'roles', loadChildren: () => import('./roles/roles.module').then(m => m.RolesModule), canActivate: [AuthGuard] },
    { path: 'users', loadChildren: () => import('./user/user.module').then(m => m.RolesModule), canActivate: [AuthGuard] },
    { path: 'brands', loadChildren: () => import('./brands/brand.module').then(m => m.BrandsModule), canActivate: [AuthGuard] },
    { path: 'categories', loadChildren: () => import('./categories/categories.module').then(m => m.CategoriesModule), canActivate: [AuthGuard] },
    { path: 'products', loadChildren: () => import('./products/products.module').then(m => m.ProductsModule), canActivate: [AuthGuard] },
    { path: 'sliders', loadChildren: () => import('./slider/slider.module').then(m => m.SliderModule), canActivate: [AuthGuard] },
    { path: 'deals', loadChildren: () => import('./todaysdeal/deal.module').then(m => m.TodaysDealsModule), canActivate: [AuthGuard] },


    { path: '**', component: NotFoundComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }

﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { GlobalService } from '../shared/services/global.service';

import { AuthenticationService } from '@app/_services';

@Component({ templateUrl: 'login.component.html' })
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';
    base_url :string;
    loggedin : boolean = false

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private _globalService: GlobalService
    ) { 
        // redirect to home if already logged in
        if (this.authenticationService.userValue) {
            this.loggedin = true 
            this.router.navigate(['/home']);
        }
    }
   
    ngOnInit() {
        this.base_url = this._globalService.apiHost
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        this.loading = true;
        const info = {username: this.f.username.value, password: this.f.password.value}
        this.authenticationService.login(this.base_url, this.f.username.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    if(data.Code==201){
                        this.router.navigate([this.returnUrl]);   
                    }else{
                        this.error = data.Message
                        
                    }
                    this.loading = false;
                },
                error => {
                    console.log("error",error)
                    this.error = error;
                    this.loading = false;
                });
    }
}

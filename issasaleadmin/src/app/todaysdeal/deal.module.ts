import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { TodaysDealsRoutingModule } from './deal-routing.module';
import { DealListComponent } from './deal-list/deal-list.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { ColorPickerModule } from 'ngx-color-picker';

@NgModule({
  declarations: [
    DealListComponent
  ],
  imports: [
    CommonModule,
    TodaysDealsRoutingModule,
    SharedModule,
    NgxDropzoneModule,
    ColorPickerModule
  ]
})
export class TodaysDealsModule { }

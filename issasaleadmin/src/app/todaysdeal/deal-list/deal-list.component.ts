import { DatePipe } from '@angular/common';
import { HttpService } from 'src/app/shared/services/http.service';
import { Component, OnInit } from '@angular/core';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { DynamicScriptLoaderService } from '../../_services/dynamicscriptloaderservice';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { GlobalService } from '../../shared/services/global.service';
declare var $: any;

@Component({
  selector: 'app-deals-list',
  templateUrl: './deal-list.component.html',
  styleUrls: ['./deal-list.component.css'],
  providers: [DatePipe]
})
export class DealListComponent implements OnInit {
  public formData;
  public modalRef: NgbModalRef;
  public today_dealsData = [];
  public productsData = [];
  public discountTypesData = []
  public form: FormGroup;
  public loading = false;
  public hasErrors = false;
  public errorMessages;
  public url = '';
  files: File[] = [];
  scripts_loaded = false;
  product_images: [];
  public color : any;
 
  onSelect(event) {
    console.log(event);
    this.files.push(...event.addedFiles);
  }
  
  onRemove(event) {
    this.files.splice(this.files.indexOf(event), 1);
  }
  constructor(private _httpService: HttpService, private modalService: NgbModal, private router: Router,
    public datePipe: DatePipe, public toastrService: ToastrService, public fb: FormBuilder,  private _globalService: GlobalService) {
       // customize default values of modals used by this component tree
    }
  ngOnInit() {
    this.url = this._globalService.imageHost
    this.loadData();
    this.form = this.fb.group({
      product_id: [
        this.formData ? this.formData.product_id : '', Validators.compose([Validators.required])],
      discount_type_id: [
        this.formData ? this.formData.discount_id : '', ],
      discount: [
        this.formData ? this.formData.discount : '', ],
      date: [
        this.formData ? this.formData.date : '',],
      level: [
        this.formData ? this.formData.level : '', Validators.compose([Validators.required])],
    });
  }
  setSelected(product_id){
    var selected_product = this.productsData.find(element => element.id == product_id)
    this.product_images = selected_product['product_images']
    setTimeout(() => {
      this.loadSwiper()
    }, 100);
    
  }
  private loadData(): any {
    this._httpService.get(`todays_deals`).subscribe(
      result => {
        if (result.success) {
          this.today_dealsData = result.data;
          if(!this.scripts_loaded){
            this.loadScripts();
          }
          
        } else {
          this.toastrService.error(result.message);
        }
      },
      error => {
      },
      complete => {
      }
    );
    this._httpService.get(`products`).subscribe(
      result => {
        if (result.success) {
          this.productsData = result.data;
          
        } else {
          this.toastrService.error(result.message);
        }
      },
      error => {
      },
      complete => {
      }
    );
    this._httpService.get(`discount_types`).subscribe(
      result => {
        if (result.success) {
          this.discountTypesData = result.data;
          
        } else {
          this.toastrService.error(result.message);
        }
      },
      error => {
      },
      complete => {
      }
    );
  }
  public submitData(): void {
    var date = ($('#data-date').val())
    if(!date){
      this.toastrService.error('Date is required!');
      return
    }
    
    var product_image_id = $('.swiper-slide-active').attr("id")
    this.loading = true;
    let formData  = new FormData();
    let result = Object.assign({}, this.form.value);
      for (let o in result) {
        formData.append(o, result[o])
      }
      formData.append('product_image_id', product_image_id.toString())
      formData.append('date', date.toString())
      formData.append('background_color', this.color.toString())
    
    this._httpService.post('todays_deals', formData).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.toastrService.success('Record created successfully!', 'Created Successfully!');
          this.loadData()
          this.product_images = []
          this.form.reset()
        } else {
          this.handleErrorsFromServer(result);
        }
      },
      error => {
        this.loading = false;
        this.errorMessages = error.error.error_messages;
      },
      complete => {
        this.loading = false;
      }

    );
  }
  public handleErrorsFromServer(response: any) {
    this.loading = false;
    this.hasErrors = true;
    this.errorMessages = [];
    console.log(response.errors)
    Object.entries(response.errors).forEach(
      ([key, value]) => // console.log(key, value)
        this.errorMessages.push(value)
    );
  }
  public onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this._httpService.delete('pims/commodity/' + event.data.id).subscribe(
        result => {
          if (result.response_code === 200) {
            event.confirm.resolve();
            this.toastrService.success(event.data.id, 'Deleted!');
          } else {
            this.toastrService.error(event.data.id, 'Failed to Delete!');
          }
        }
      );
    } else {
      event.confirm.reject();
    }
  }
  private viewRecord(data: any) {
    this.router.navigate(['roles', data.id]);
  }
  public onCustomAction(event: any): void {
    console.log(event);
    switch (event.action) {
      case 'viewRecord':
        this.viewRecord(event.data);
        break;
      case 'editRecord':
        // this.openModal(event.data);
        break;
      default:
        break;
    }
  }
  private loadScripts() {
    const dynamicScripts = [
      'assets/vendors/js/extensions/dropzone.min.js',
      'assets/vendors/js/tables/datatable/datatables.min.js',
      'assets/vendors/js/tables/datatable/datatables.buttons.min.js',
      'assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js',
      'assets/vendors/js/tables/datatable/buttons.bootstrap.min.js',
      'assets/vendors/js/tables/datatable/dataTables.select.min.js',
      'assets/vendors/js/tables/datatable/datatables.checkboxes.min.js',
      'assets/js/scripts/ui/data-list-view.min.js',
      'assets/js/scripts/pickers/dateTime/pick-a-datetime.min.js',
      'assets/js/scripts/extensions/swiper.min.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
    this.scripts_loaded = true
  }
  private loadSwiper() {
    const dynamicScripts = [
      'assets/js/scripts/extensions/swiper.min.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
    this.scripts_loaded = true
  }
} 


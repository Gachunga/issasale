import { DatePipe, JsonPipe } from '@angular/common';
import { HttpService } from 'src/app/shared/services/http.service';
import { Component, OnInit } from '@angular/core';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { DynamicScriptLoaderService } from '../../_services/dynamicscriptloaderservice';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { GlobalService } from '../../shared/services/global.service';

@Component({
  selector: 'app-colors',
  templateUrl: './colors.component.html',
  styleUrls: ['./colors.component.css'],
  providers: [DatePipe, JsonPipe]
})
export class ColorsComponent implements OnInit {
  public formData;
  public modalRef: NgbModalRef;
  public colorsData = [];
  public form: FormGroup;
  public loading = false;
  public hasErrors = false;
  public errorMessages;
  public url = '';
  files: File[] = [];
  scripts_loaded = false;
  items: FormArray;
  
  constructor(config: NgbModalConfig, private _httpService: HttpService, private modalService: NgbModal, private router: Router,
    public datePipe: DatePipe, public toastrService: ToastrService, public fb: FormBuilder,  private _globalService: GlobalService) {
       // customize default values of modals used by this component tree
    config.backdrop = 'static';
    config.keyboard = false;
    }
  ngOnInit() {
    this.url = this._globalService.imageHost
    this.loadData();
    this.form = this.fb.group({
      items: this.fb.array([this.createItem()]),
    });
  }
  createItem(): FormGroup {
    return this.fb.group({
      id : '',
      name: '',
      color: '',
    });
  }
  createItemUpdate(detail): FormGroup {
    return this.fb.group({
      id: detail.id,
      name: detail.name ? detail.name : "",
      color: detail.color ? detail.color : "",
    });
  }
  addItem(): void {
    this.items = this.form.get('items') as FormArray;
    this.items.push(this.createItem());
  }
  removeItem(index) : void {
    this.items.removeAt(this.items.value.findIndex(item => item.id === index))
  }
  getControls() {
    return (this.form.get('items') as FormArray).controls;
  }
  private loadData(): any {
    this._httpService.get(`colors`).subscribe(
      result => {
        if (result.success) {
          this.colorsData = result.data;
          if(!this.scripts_loaded){
            this.loadScripts();
          }
          
        } else {
          this.toastrService.error(result.message);
        }
      },
      error => {
      },
      complete => {
      }
    );
  }
  public submitData(): void {
    this.loading = true;
    let formData  = new FormData();
    let items = this.getControls()
    var arrayControl = this.form.get('items') as FormArray;
    for (let i in items) {
      //@ts-ignore
      let color = arrayControl.at(parseInt(i)).color
      let value = arrayControl.at(parseInt(i)).value
      let color_item = {color : color, name : value.name}
      formData.append('colors[]', JSON.stringify(color_item));
    }
    this._httpService.post('colors', formData).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.toastrService.success('Record created successfully!', 'Created Successfully!');
          this.loadData()
          this.files = []
          const arr = <FormArray>this.form.controls.items;
          arr.controls = [];
          this.form.reset()
        } else {
          this.handleErrorsFromServer(result);
        }
      },
      error => {
        this.loading = false;
        this.errorMessages = error.error.error_messages;
      },
      complete => {
        this.loading = false;
      }

    );
  }
  public handleErrorsFromServer(response: any) {
    this.loading = false;
    this.hasErrors = true;
    this.errorMessages = [];
    console.log(response.errors)
    Object.entries(response.errors).forEach(
      ([key, value]) => // console.log(key, value)
        this.errorMessages.push(value)
    );
  }
  public onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this._httpService.delete('pims/commodity/' + event.data.id).subscribe(
        result => {
          if (result.response_code === 200) {
            event.confirm.resolve();
            this.toastrService.success(event.data.id, 'Deleted!');
          } else {
            this.toastrService.error(event.data.id, 'Failed to Delete!');
          }
        }
      );
    } else {
      event.confirm.reject();
    }
  }
  private viewRecord(data: any) {
    this.router.navigate(['roles', data.id]);
  }
  public onCustomAction(event: any): void {
    console.log(event);
    switch (event.action) {
      case 'viewRecord':
        this.viewRecord(event.data);
        break;
      case 'editRecord':
        // this.openModal(event.data);
        break;
      default:
        break;
    }
  }
  private loadScripts() {
    const dynamicScripts = [
      'assets/vendors/js/tables/datatable/datatables.min.js',
      'assets/vendors/js/tables/datatable/datatables.buttons.min.js',
      'assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js',
      'assets/vendors/js/tables/datatable/buttons.bootstrap.min.js',
      'assets/vendors/js/tables/datatable/dataTables.select.min.js',
      'assets/vendors/js/tables/datatable/datatables.checkboxes.min.js',
      'assets/js/scripts/ui/data-list-view.min.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
    this.scripts_loaded = true
  }
} 


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuantitiesListComponent } from './quantity-list.component';

describe('QuantitiesListComponent', () => {
  let component: QuantitiesListComponent;
  let fixture: ComponentFixture<QuantitiesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuantitiesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuantitiesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

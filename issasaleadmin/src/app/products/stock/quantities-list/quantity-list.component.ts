import { DatePipe } from '@angular/common';
import { HttpService } from 'src/app/shared/services/http.service';
import { Component, OnInit } from '@angular/core';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AddQuantityComponent } from '../add-quantity/add-quantity.component';
import { NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-quantity-list',
  templateUrl: './quantity-list.component.html',
  styleUrls: ['./quantity-list.component.css'],
  providers: [DatePipe]
})
export class QuantitiesListComponent implements OnInit {
  public formData;
  public modalRef: NgbModalRef;
  public settings = {
    selectMode: 'single',  // single|multi
    hideHeader: false,
    hideSubHeader: false,
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: false,
      custom: [  { name: 'viewRecord', title: '<i class="fa fa-eye"></i>&nbsp;&nbsp;' },
      { name: 'addRecord', title: '<i class="fa fa-plus-square"></i>&nbsp;&nbsp;' },
      // { name: 'editRecord', title: '<i class="fa fa-pencil"></i>&nbsp;&nbsp;' }
    ],
      position: 'right' // left|right
    },
    delete: {
      deleteButtonContent: '&nbsp;&nbsp;<i class="fa fa-trash-o text-danger"></i>',
      confirmDelete: true
    },
    noDataMessage: 'No data found',
    columns: {
      title: {
        title: 'Product',
        type: 'string',
      },
      color: {
        title: 'Color',
        type: 'html',
        valuePrepareFunction: (color, row) => {
          return this.sanitizer.bypassSecurityTrustHtml(`<div class="badge" style="background-color: ${color}">${row.name}</div>`);
        },
      },
      size: {
        title: 'Size',
        type: 'size'
      },
      quantity: {
        title: 'Quantity',
        type: 'string',
        editable :true
      },
    },
    pager: {
      display: true,
      perPage: 10
    }
  };
  dataSet: any;
  constructor(config: NgbModalConfig, private _httpService: HttpService, private modalService: NgbModal, private router: Router,
    public datePipe: DatePipe, public toastrService: ToastrService,
    private sanitizer: DomSanitizer) {
       // customize default values of modals used by this component tree
    config.backdrop = 'static';
    config.keyboard = false;
    }
  ngOnInit() {
    this.loadData();
  }
  private loadData(): any {
    this._httpService.get(`product_quantities`).subscribe(
      result => {
        if (result.success) {
          this.dataSet = result.data;
        } else {
          this.toastrService.error(result.message);
        }
      },
      error => {
      },
      complete => {
      }
    );
  }
  public openModal(formData) {
    this.formData = formData;
    this.modalRef = this.modalService.open(AddQuantityComponent);
    if (formData) {
      this.modalRef.componentInstance.title = 'Edit Quantity Info ';
    } else {
      this.modalRef.componentInstance.title = 'Add Quantity';
    }
    this.modalRef.componentInstance.formData = this.formData;
    this.modalRef.result.then((result) => {
      if (result === 'success') {
        this.loadData();
      }
    }, (reason) => {
    });
  }
  public onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this._httpService.delete('pims/commodity/' + event.data.id).subscribe(
        result => {
          if (result.response_code === 200) {
            event.confirm.resolve();
            this.toastrService.success(event.data.id, 'Deleted!');
          } else {
            this.toastrService.error(event.data.id, 'Failed to Delete!');
          }
        }
      );
    } else {
      event.confirm.reject();
    }
  }
  private viewRecord(data: any) {
    this.router.navigate(['roles', data.id]);
  }
  public onCustomAction(event: any): void {
    console.log(event);
    switch (event.action) {
      case 'viewRecord':
        this.viewRecord(event.data);
        break;
      case 'addRecord':
        this.openModal(event.data);
        break;
      case 'editRecord':
        this.openModal(event.data);
        break;
      default:
        break;
    }
  }

}


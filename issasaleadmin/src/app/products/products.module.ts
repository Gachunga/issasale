import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { ProductsRoutingModule } from './products-routing.module';
import { ProductsListComponent } from './products-list/products-list.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { SwiperModule, SWIPER_CONFIG, SwiperConfigInterface  } from 'ngx-swiper-wrapper';
import { QuantitiesListComponent } from './stock/quantities-list/quantity-list.component';
import { AddQuantityComponent } from './stock/add-quantity/add-quantity.component';
import { ColorsComponent } from './colors/colors.component';
import { SizesComponent } from './sizes/sizes.component';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto'
};

@NgModule({
  declarations: [
    ProductsListComponent,
    QuantitiesListComponent,
    AddQuantityComponent,
    ColorsComponent,
    SizesComponent
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    NgxDropzoneModule,
    SharedModule,
    ColorPickerModule,
    SwiperModule,
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ]
})
export class ProductsModule { }

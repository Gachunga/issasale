import { DatePipe, JsonPipe } from '@angular/common';
import { HttpService } from 'src/app/shared/services/http.service';
import { Component, OnInit } from '@angular/core';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { DynamicScriptLoaderService } from '../../_services/dynamicscriptloaderservice';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { GlobalService } from '../../shared/services/global.service';

@Component({
  selector: 'app-colors',
  templateUrl: './sizes.component.html',
  styleUrls: ['./sizes.component.css'],
  providers: [DatePipe, JsonPipe]
})
export class SizesComponent implements OnInit {
  public formData;
  public modalRef: NgbModalRef;
  public sizesData = [];
  public form: FormGroup;
  public loading = false;
  public hasErrors = false;
  public errorMessages;
  public url = '';
  files: File[] = [];
  scripts_loaded = false;
  sizes: FormArray;
  
  constructor(config: NgbModalConfig, private _httpService: HttpService, private modalService: NgbModal, private router: Router,
    public datePipe: DatePipe, public toastrService: ToastrService, public fb: FormBuilder,  private _globalService: GlobalService) {
       // customize default values of modals used by this component tree
    config.backdrop = 'static';
    config.keyboard = false;
    }
  ngOnInit() {
    this.url = this._globalService.imageHost
    this.loadData();
    this.form = this.fb.group({
      sizes: this.fb.array([this.createItemSize()]),
    });
  }
  createItemSize(): FormGroup {
    return this.fb.group({
      id : '',
      size: '',
    });
  }
  addSize(): void {
    this.sizes = this.form.get('sizes') as FormArray;
    this.sizes.push(this.createItemSize());
  }
  getSizeControls() {
    return (this.form.get('sizes') as FormArray).controls;
  }
  removeSizeItem(index) : void {
    this.sizes.removeAt(this.sizes.value.findIndex(size => size.id === index))
  }
  createItemUpdate(detail): FormGroup {
    return this.fb.group({
      id: detail.id,
      name: detail.name ? detail.name : "",
      color: detail.color ? detail.color : "",
    });
  }
  getControls() {
    return (this.form.get('items') as FormArray).controls;
  }
  private loadData(): any {
    this._httpService.get(`sizes`).subscribe(
      result => {
        if (result.success) {
          console.log(result)
          this.sizesData = result.data;
          if(!this.scripts_loaded){
            this.loadScripts();
          }
          
        } else {
          this.toastrService.error(result.message);
        }
      },
      error => {
      },
      complete => {
      }
    );
  }
  public submitData(): void {
    this.loading = true;
    let formData  = new FormData();
    let sizes = this.getSizeControls()
    var sizearrayControl = this.form.get('sizes') as FormArray;
    for (let i in sizes) {
      //@ts-ignore
      let value = sizearrayControl.at(parseInt(i)).value
      if(value.size!==""){
      formData.append('sizes_arr[]', JSON.stringify(value.size));
      }
    }
    this._httpService.post('sizes', formData).subscribe(
      result => {
        if (result.success) {
          this.toastrService.success('Record created successfully!', 'Created Successfully!');
          this.loadData()
          this.files = []
          const arr = <FormArray>this.form.controls.items;
          arr.controls = [];
          this.form.reset()
        } else {
          this.handleErrorsFromServer(result);
        }
      },
      error => {
        this.loading = false;
        this.errorMessages = error.error.error_messages;
      },
      complete => {
        this.loading = false;
      }

    );
  }
  public handleErrorsFromServer(response: any) {
    this.loading = false;
    this.hasErrors = true;
    this.errorMessages = [];
    console.log(response.errors)
    Object.entries(response.errors).forEach(
      ([key, value]) => // console.log(key, value)
        this.errorMessages.push(value)
    );
  }
  public onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this._httpService.delete('pims/commodity/' + event.data.id).subscribe(
        result => {
          if (result.response_code === 200) {
            event.confirm.resolve();
            this.toastrService.success(event.data.id, 'Deleted!');
          } else {
            this.toastrService.error(event.data.id, 'Failed to Delete!');
          }
        }
      );
    } else {
      event.confirm.reject();
    }
  }
  private viewRecord(data: any) {
    this.router.navigate(['roles', data.id]);
  }
  public onCustomAction(event: any): void {
    console.log(event);
    switch (event.action) {
      case 'viewRecord':
        this.viewRecord(event.data);
        break;
      case 'editRecord':
        // this.openModal(event.data);
        break;
      default:
        break;
    }
  }
  private loadScripts() {
    const dynamicScripts = [
      'assets/vendors/js/tables/datatable/datatables.min.js',
      'assets/vendors/js/tables/datatable/datatables.buttons.min.js',
      'assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js',
      'assets/vendors/js/tables/datatable/buttons.bootstrap.min.js',
      'assets/vendors/js/tables/datatable/dataTables.select.min.js',
      'assets/vendors/js/tables/datatable/datatables.checkboxes.min.js',
      'assets/js/scripts/ui/data-list-view.min.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
    this.scripts_loaded = true
  }
} 


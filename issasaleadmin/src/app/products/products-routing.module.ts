import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ColorsComponent } from './colors/colors.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { SizesComponent } from './sizes/sizes.component';
import { QuantitiesListComponent } from './stock/quantities-list/quantity-list.component';


const routes: Routes = [
  {
    path: '',
    component: ProductsListComponent,
    data: {
        title: 'Products List'
    },
  },
  {
    path: 'quantity',
    component: QuantitiesListComponent,
    data: {
        title: 'Quantity List'
    },
  },
  { path: 'colors', component: ColorsComponent,  },
  { path: 'sizes', component: SizesComponent,  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }

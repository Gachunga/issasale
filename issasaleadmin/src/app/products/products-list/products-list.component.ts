import { DatePipe } from '@angular/common';
import { HttpService } from 'src/app/shared/services/http.service';
import { Component, OnInit } from '@angular/core';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
// import { AddBrandComponent } from '../add-brand/add-brand.component';
import { NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { DynamicScriptLoaderService } from '../../_services/dynamicscriptloaderservice';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { GlobalService } from '../../shared/services/global.service';
import {  SwiperConfigInterface } from 'ngx-swiper-wrapper';

declare var $: any;

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css'],
  providers: [DatePipe]
})
export class ProductsListComponent implements OnInit {
  public formData;
  public modalRef: NgbModalRef;
  public productsData = [];
  public form: FormGroup;
  public loading = false;
  public hasErrors = false;
  public errorMessages;
  public url = '';
  files: File[] = [];
  public categories = []
  public category_id = 0;
  public showsidepanel = false
  public showeditsidepanel = false
  public fetching=false
  public brandsData = []
  public colorsData = []
  public sizesData = []
  public brands = []
  items: FormArray;
  sizes: FormArray;
  variations = [];
  public discount_types = [];
  public edit = false
  public edit_product;
  displayed_images = []
  deletedImages : string[] = [];
  scripts_loaded = false
  
  public config: SwiperConfigInterface = {
    a11y: true,
    direction: 'horizontal',
    slidesPerView: 1,
    keyboard: true,
    mousewheel: true,
    scrollbar: false,
    navigation: true,
    pagination: false
  };

  onSelect(event) {
    console.log(event);
    this.files.push(...event.addedFiles);
  }
  
  onRemove(event) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }
  constructor(config: NgbModalConfig, private _httpService: HttpService, private modalService: NgbModal, private router: Router,
    public datePipe: DatePipe, public toastrService: ToastrService, public fb: FormBuilder,  private _globalService: GlobalService) {
       // customize default values of modals used by this component tree
    config.backdrop = 'static';
    config.keyboard = false;
    }
  deleteImage(id, index){
    console.log(index)
    if(confirm("Are you sure to delete this image")) {
      this.deletedImages.push(id)
      this.displayed_images.splice(index, 1)
    }
    console.log(this.deletedImages)
  }
  ngOnInit() {
    this.url = this._globalService.imageHost
    this.loadData();
    this.form = this.fb.group({
      title: [
        this.formData ? this.formData.title : '', Validators.compose([Validators.required])],
      code: [
        this.formData ? this.formData.code : '',],
      description: [
        this.formData ? this.formData.description : '', Validators.compose([Validators.required])],
      taxable: [
        this.formData ? this.formData.taxable : "false",],
      brand_id: [
        this.formData ? this.formData.brand_id : '', Validators.compose([Validators.required])],
      category_id: [
        this.formData ? this.formData.category_id : '', Validators.compose([Validators.required])],
      price: [
        this.formData ? this.formData.price : '', Validators.compose([Validators.required])],
      previous_price: [
        this.formData ? this.formData.previous_price : '' ],
        discount_type_id: [
        this.formData ? this.formData.discount_type_id : '', ],
      discount: [
        this.formData ? this.formData.discount : '', ],
      items: this.fb.array([]),
      sizes: this.fb.array([]),
      variations: this.fb.array([ ])
    });
  }
  createItem(): FormGroup {
    return this.fb.group({
      id : '',
      name: '',
      color: '',
    });
  }
  createItemSize(): FormGroup {
    return this.fb.group({
      id : '',
      size: '',
    });
  }
  createItemUpdate(detail): FormGroup {
    return this.fb.group({
      id: detail.id,
      name: detail.name ? detail.name : "",
      color: detail.color ? detail.color : "",
    });
  }
  addItem(): void {
    this.items = this.form.get('items') as FormArray;
    this.items.push(this.createItem());
  }
  addSize(): void {
    this.sizes = this.form.get('sizes') as FormArray;
    this.sizes.push(this.createItemSize());
  }
  removeItem(index) : void {
    this.items.removeAt(this.items.value.findIndex(item => item.id === index))
  }
  getControls() {
    return (this.form.get('items') as FormArray).controls;
  }
  getSizeControls() {
    return (this.form.get('sizes') as FormArray).controls;
  }
  removeSizeItem(index) : void {
    this.sizes.removeAt(this.sizes.value.findIndex(size => size.id === index))
  }
  checkValue(event, color, size){
    let checked = event.target.checked;
    let id = color.id+''+size.id
    var exists = this.variations.findIndex(variation => variation.id==id)
    if(exists>0){
      this.variations.splice(exists, 1)
    }else{
      this.variations.push({id:id,  color_id: color.id, size_id: size.id})
    }
  }
  public toggleProductModal(cat_id){
    this.form.reset()
    this.displayed_images=[]
    
    if(cat_id) this.category_id = cat_id
    this.edit =false 
    this.showsidepanel ? this.showsidepanel=false : this.showsidepanel=true
  }
  public toggleEdit(product){
    this.edit_product = product
    this.edit =true
    this.showsidepanel ? this.showsidepanel=false : this.showsidepanel=true
    this.form.patchValue({title : product.title ? product.title : ""})
    this.form.patchValue({brand_id: product.brand_id})
    this.form.patchValue({code: product.code})
    this.form.patchValue({description : product.description})
    this.form.patchValue({taxable : product.taxable ==1 ? "true" :"false"})
    this.form.patchValue({category_id : product.category_id})
    this.form.patchValue({price : product.product_prices[0].price})
    this.form.patchValue({previous_price : product.product_prices[0].previous_price})
    this.form.patchValue({discount_type_id : product.product_prices[0].discount_type_id})
    this.form.patchValue({discount : product.product_prices[0].discount})
    
    for(let i in product.product_colors){
      this.items = this.form.get('items') as FormArray;
      this.items.push(this.createItemUpdate(product.product_colors[i]));
      
    }
    this.displayed_images = product.product_images
  }
  private loadData(): any {
    this._httpService.get(`products`).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.productsData = result.data;
          if(!this.scripts_loaded){
            this.loadScripts();
          }
        } else {
          this.toastrService.error(result.message);
        }
      },
      error => {
      },
      complete => {
      }
    );
    this.fetching = true
    this._httpService.get(`category/list`).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.categories = result.data;
        } else {
          this.toastrService.error(result.message);
        }
      },
      error => {
      },
      complete => {
        this.fetching = false
      }
    );
    this._httpService.get(`brands`).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.brands = result.data;
        } else {
          this.toastrService.error(result.message);
        }
      },
      error => {
      },
      complete => {
      }
    );
    this._httpService.get(`discount_types`).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.discount_types = result.data;
        } else {
          this.toastrService.error(result.message);
        }
      },
      error => {
      },
      complete => {
      }
    );
    this._httpService.get(`colors`).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.colorsData = result.data;
          if(!this.scripts_loaded){
            this.loadScripts();
          }
        } else {
          this.toastrService.error(result.message);
        }
      },
      error => {
      },
      complete => {
      }
    );
    this._httpService.get(`sizes`).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.sizesData = result.data;
          if(!this.scripts_loaded){
            this.loadScripts();
          }
        } else {
          this.toastrService.error(result.message);
        }
      },
      error => {
      },
      complete => {
      }
    );
  }
  public submitData(): void {
    this.loading = true;
    let formData  = new FormData();
    let result = Object.assign({}, this.form.value);
    console.log(result)
    
    for (let o in result) {
      formData.append(o, result[o])
    }
    for (var i = 0; i < this.files.length; i++) { 
      formData.append("images[]", this.files[i]);
    }
    for (let variant of this.variations) {
      //@ts-ignore
      formData.append('variants[]', JSON.stringify(variant));
      
    }
    
    this._httpService.post('products', formData).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.toastrService.success('Record created successfully!', 'Created Successfully!');
          this.loadData()
          this.files = []
          this.form.reset()
        } else {
          this.handleErrorsFromServer(result);
        }
      },
      error => {
        this.loading = false;
        this.errorMessages = error.error.error_messages;
      },
      complete => {
        this.loading = false;
      }

    );
  }
  public handleErrorsFromServer(response: any) {
    this.loading = false;
    this.hasErrors = true;
    this.errorMessages = [];
    console.log(response.errors)
    Object.entries(response.errors).forEach(
      ([key, value]) => // console.log(key, value)
        this.errorMessages.push(value)
    );
  }
  public saveData(id): void {
    this.loading = true;
    let formData  = new FormData();
    let result = Object.assign({}, this.form.value);
    let color_items = []
    let items = this.getControls()
    var arrayControl = this.form.get('items') as FormArray;
    for (let i in items) {
      let value = arrayControl.at(parseInt(i)).value
      let touched = arrayControl.at(parseInt(i)).touched
      //@ts-ignore
      let color = value.id&&!touched ? value.color : arrayControl.at(parseInt(i)).color
      let color_item = {id : value.id, color : color, name : value.name}
      formData.append('colors[]', JSON.stringify(color_item));
    }
    
    for (let o in result) {
      formData.append(o, result[o])
    }
    for (var i = 0; i < this.files.length; i++) { 
      formData.append("images[]", this.files[i]);
    }
    for(let deleteImage of this.deletedImages){
      formData.append("deletedImages[]", deleteImage)
    }
    
    this._httpService.post('product/update/'+id, formData).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.toastrService.success('Record created successfully!', 'Created Successfully!');
          this.loadData()
          this.files = []
          this.form.reset()
          this.displayed_images= []
        } else {
          this.handleErrorsFromServer(result);
        }
      },
      error => {
        this.loading = false;
        this.errorMessages = error.error.error_messages;
      },
      complete => {
        this.loading = false;
      }

    );
  }
  public onDeleteConfirm(id): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this._httpService.delete('products' + id).subscribe(
        result => {
          if (result.response_code === 200) {
            this.toastrService.success(id, 'Deleted!');
          } else {
            this.toastrService.error(id, 'Failed to Delete!');
          }
        }
      );
    } else {
      // event.confirm.reject();
    }
  }
  private viewRecord(data: any) {
    this.router.navigate(['roles', data.id]);
  }
  public onCustomAction(event: any): void {
    console.log(event);
    switch (event.action) {
      case 'viewRecord':
        this.viewRecord(event.data);
        break;
      case 'editRecord':
        // this.openModal(event.data);
        break;
      default:
        break;
    }
  }
  private loadScripts() {
    $('#colorselector').colorselector();
    const dynamicScripts = [
      'assets/vendors/js/extensions/dropzone.min.js',
      'assets/vendors/js/tables/datatable/datatables.min.js',
      'assets/vendors/js/tables/datatable/datatables.buttons.min.js',
      'assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js',
      'assets/vendors/js/tables/datatable/buttons.bootstrap.min.js',
      'assets/vendors/js/tables/datatable/dataTables.select.min.js',
      'assets/vendors/js/tables/datatable/datatables.checkboxes.min.js',
      'assets/js/scripts/ui/data-list-view.min.js',
    ];
    
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
    this.scripts_loaded = true
  }
} 


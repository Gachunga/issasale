﻿export class User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    authdata?: string;
    token ?: string
    uuid : string
    avatar_location : string
    full_name : string
}
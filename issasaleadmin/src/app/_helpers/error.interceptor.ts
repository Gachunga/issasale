import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import { AuthenticationService } from '@app/_services';

@Injectable({
    providedIn: 'root'
    })
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService, private toastrService : ToastrService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            console.log(err)
            if (err.status === 401) {
                // auto logout if 401 response returned from api
                this.authenticationService.logout();
            }

            const error = err.error.message || err.statusText;
            this.toastrService.error(error);
            Object.entries(err.error.errors).forEach(
                ([key, value]) => // console.log(key, value)
                this.toastrService.error(value[0].toString())
              );
            return throwError(error);
        }))
    }
}
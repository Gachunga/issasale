import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersListComponent } from './users-list/users-list.component';
import { ViewUserComponent } from './view-user/view-user.component';


const routes: Routes = [
  {
    path: '',
    component: UsersListComponent,
    data: {
        title: 'User List'
    }
},
{
    path: ':id',
    component: ViewUserComponent,
    data: {
        breadcrumb: 'View User',
        title: 'User'
    }
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }

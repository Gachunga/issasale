import { HttpService } from 'src/app/shared/services/http.service';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  @Input() title;
  @Input() formData;
  public loading = false;
  public hasErrors = false;
  public errorMessages;
  public form: FormGroup;
  public rolesData;
  public fetching : boolean =false
  constructor(
    public activeModal: NgbActiveModal,
    public fb: FormBuilder,
    private _httpService: HttpService,
    public toastrService: ToastrService) { }
  ngOnInit() {
    this.loadRoles();
    this.form = this.fb.group({
      first_name: [
        this.formData ? this.formData.first_name : '', Validators.compose([Validators.required])],
      last_name: [
        this.formData ? this.formData.last_name : '', Validators.compose([Validators.required])],
      email: [
        this.formData ? this.formData.email : '', Validators.compose([Validators.required])],
      password: [
        this.formData ? this.formData.password : '', Validators.compose([Validators.required])],
      password_confirmation: [
        this.formData ? this.formData.password_confirmation : '', Validators.compose([Validators.required])],
      roles: new FormArray([], this.minSelectedCheckboxes(1)),
    });
  }
  get rolesFromArray() {
    return this.form.controls.roles as FormArray;
  }
  private addCheckboxes() {
    this.rolesData.forEach(() => this.rolesFromArray.push(new FormControl(false)));
  }
  public submitData(): void {
    this.loading = true;
    const selectedRoleIds = this.form.value.roles
      .map((checked, i) => checked ? this.rolesData[i].id : null)
      .filter(v => v !== null);
    this.form.value.roles = selectedRoleIds
    console.log(JSON.stringify(this.form.value))
    this._httpService.post('users', this.form.value).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.toastrService.success('Record created successfully!', 'Created Successfully!');
          this.activeModal.close('success');
        } else {
          this.handleErrorsFromServer(result);
        }
      },
      error => {
        this.errorMessages = error.error.error_messages;
      },
      complete => {
        this.loading = false;
      }

    );
  }
  public saveChanges(): void {
    this.loading = true;
    this.form.value.id = this.formData.id;
    this._httpService.post('clients/update', this.form.value).subscribe(
      result => {
        if (result.response.response_status.response_code === 200) {
          this.toastrService.success('Changes saved!', 'Saved Successfully!');
          this.activeModal.close('success');
        } else {
          this.handleErrorsFromServer(result);
        }
      },
      error => {
        this.errorMessages = error.error.error_messages;
      },
      complete => {
        this.loading = false;
      }

    );
  }
  public handleErrorsFromServer(response: any) {
    this.loading = false;
    this.hasErrors = true;
    this.errorMessages = [];
    console.log(response.errors)
    Object.entries(response.errors).forEach(
      ([key, value]) => // console.log(key, value)
        this.errorMessages.push(value)
    );
  }

  public closeModal(): void {
    this.activeModal.dismiss('Cross click');
  }
 private loadRoles() {
  this.fetching=true
  this._httpService.get('roles').subscribe(
    result => {
      if (result.success) {
        this.rolesData = result.data;
        this.addCheckboxes();
      } else {
      }
    },
    error => {
    },
    complete => {
      this.fetching=false
    }
  );
 }
 public minSelectedCheckboxes(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      // get a list of checkbox values (boolean)
      .map(control => control.value)
      // total up the number of checked checkboxes
      .reduce((prev, next) => next ? prev + next : prev, 0);

    // if the total is not greater than the minimum, return the error message
    return totalSelected >= min ? null : { required: true };
  };

  return validator;
}
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { UsersListComponent } from './users-list/users-list.component';
import { AddUserComponent } from './add-user/add-user.component';
import { ViewUserComponent } from './view-user/view-user.component';
import { UserRoutingModule } from './user-routing.module';


@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    UserRoutingModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    UsersListComponent,
    AddUserComponent,
    ViewUserComponent,
  ],
  entryComponents: [
    AddUserComponent
  ],
})
export class RolesModule { }

import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@app/_services';
import {Observable} from 'rxjs'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {
  isLoggedIn$: Observable<boolean>;

  constructor(
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit(): void {
      this.isLoggedIn$ =  this.authenticationService.isLoggedIn
      console.log(this.isLoggedIn$)
  }

}

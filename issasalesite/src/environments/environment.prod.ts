export const environment = {
    production: true,
    apiUrl: 'http://localhost:4000',
    apiHost : 'https://inappex.com/issasale/issasaleb/public/api/v1/',
    imageHost : 'https://inappex.com/issasale/issasaleb/public/storage/',
};

$('#quickView').on('shown.bs.modal', function (e) {
    quickViewSlider = new Swiper('#quickViewSlider', {
        mode: 'horizontal',
        loop: true,
        on: {
            slideChangeTransitionStart: highlightThumb
        }
    });
})

$('#quickView').on('hidden.bs.modal', function (e) {
    quickViewSlider.destroy();
})


function highlightThumb() {
    var sliderId = this.$el.attr('id');
    var thumbs = $('.swiper-thumbs[data-swiper="#' + sliderId + '"]')
    // if thumbs for this slider exist
    if (thumbs.length > 0) {
        thumbs.find(".swiper-thumb-item.active").removeClass('active');
        thumbs.find(".swiper-thumb-item").eq(this.realIndex).addClass('active');
    }
}

$('.swiper-thumb-item').on('click', function (e) {
    e.preventDefault();
    var swiperId = $(this).parents('.swiper-thumbs').data('swiper');
    $(swiperId)[0].swiper.slideToLoop($(this).index())
});
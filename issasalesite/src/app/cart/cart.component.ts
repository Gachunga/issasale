import { Component, OnInit } from '@angular/core';
import { CartService } from '../_services/cart.service'
import { ToastrService } from 'ngx-toastr';
import {Router} from '@angular/router';
import { GlobalService } from '../shared/services/global.service';
import { HttpService } from 'src/app/shared/services/http.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  public cartItems = []
  public url = ''
  constructor(
    public cartService: CartService,
    private _globalService: GlobalService,
    private _httpService : HttpService,
    private toastrService : ToastrService
  ) { }

  ngOnInit(): void {
    this.url = this._globalService.imageHost
    this.cartItems = this.cartService.items
  }
  handleAddToCartProduct (product) {
    console.log(product)
    this.cartService.addToCart(product)
    this.toastrService.success('1 product has been added to cart', 'cart', {
      positionClass: 'toast-bottom-right' 
   });
  }
  handleRemoveFromCartProduct (index) {
    this.cartService.removeItem(index)
    this.toastrService.success('1 product has been removed to cart', 'cart', {
      positionClass: 'toast-bottom-right' 
   });
  }
  handleReduceItemCartProduct (index) {
    this.cartService.reduceItemQuantity(index)
    this.toastrService.success('1 product has been removed from cart', 'cart', {
      positionClass: 'toast-bottom-right' 
   });
  }

}

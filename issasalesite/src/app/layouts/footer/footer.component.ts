import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@app/_services';
import {Observable} from 'rxjs'

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.less']
})
export class FooterComponent implements OnInit {
  isLoggedIn$: Observable<boolean>;

  constructor(
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit(): void {
      this.isLoggedIn$ =  this.authenticationService.isLoggedIn
  }

}

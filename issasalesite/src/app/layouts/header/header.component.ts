import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@app/_services';
import {Observable} from 'rxjs'
import { Router } from '@angular/router';
import { CartService } from '@app/_services/cart.service';
import { GlobalService } from '../../shared/services/global.service';
import { HttpService } from 'src/app/shared/services/http.service';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLoggedIn$: Observable<boolean>;
  cartItems = [];
  public url = "";
  public className = "header";
  public categoriesData;

  constructor(
    private _httpService : HttpService,
    private authenticationService: AuthenticationService,
    public cartService: CartService,
    private router: Router,
    private _globalService: GlobalService
  ) { 
    router.events.subscribe((url:any) => {
      console.log(router.url==="/");  // to print only path eg:"/login"
      if(router.url=="/"||router.url=="/home"){
        this.className = "header header-absolute"
      }else{
        this.className = "header"
      }
    });
  }

  ngOnInit(): void {
      this.isLoggedIn$ =  this.authenticationService.isLoggedIn
      this.cartItems = this.cartService.getItems()
      this.url = this._globalService.imageHost  
      this.cartItems = this.cartService.getItems()
      this.loadData();
  }
  private loadData(): any {
    this._httpService.get(`category/listall`).subscribe(
        result => {
          console.log(result)
          if (result.success) {
            this.categoriesData = result.data[0];
            console.log(this.categoriesData)
            setTimeout(() => {
              
            }, 200);
          } else {
            
          }
        },
        error => {
        },
        complete => {
        }
      );
    }
  setDetailProduct(product){
    this.cartService.setDetailProduct(product)
    this.router.navigateByUrl('/detail');
    $('#sidebarCart').modal('hide');
  }
  hideCart(){
    $('#sidebarCart').modal('hide');
  }

}

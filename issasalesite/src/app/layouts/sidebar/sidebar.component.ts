import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@app/_services';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.less']
})
export class SidebarComponent implements OnInit {
  isLoggedIn$: Observable<boolean>;

  constructor(
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit(): void {
      this.isLoggedIn$ =  this.authenticationService.isLoggedIn
  }

}

import { Component, OnInit } from '@angular/core';
import { CartService } from '../_services/cart.service'
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import {Router} from '@angular/router';
import { GlobalService } from '../shared/services/global.service';
import { HttpService } from 'src/app/shared/services/http.service';
import { NgxSpinnerService } from "ngx-spinner";
import { AuthenticationService } from '@app/_services';

declare var $: any;
@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  public cartItems = []
  public formData;
  paymentMethodsData=[]
  paymentmethod = ''
  public url = ''
  public form: FormGroup;
  loading = false
  public hasErrors = false;
  public errorMessages;
  constructor(
    public cartService: CartService,
    private _globalService: GlobalService,
    private _httpService : HttpService,
    private toastrService : ToastrService,
    public fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private authenticationService: AuthenticationService,
    private router: Router
  ) {
    if (this.authenticationService.userValue) {
      console.log(this.authenticationService.userValue) 
      // this.router.navigate(['/home']);
    }else{
      $('#loginModal').modal('toggle')
    }
   }

  ngOnInit(): void {
    this.url = this._globalService.imageHost
    this.cartItems = this.cartService.items
    this.loadData()
    this.form = this.fb.group({
      name: [
        this.formData ? this.formData.name : '', Validators.compose([Validators.required])],
      email: [
        this.formData ? this.formData.email : '', Validators.compose([Validators.required])],
      location: [
        this.formData ? this.formData.location : '', Validators.compose([Validators.required])],
      city: [
        this.formData ? this.formData.city : '', Validators.compose([Validators.required])],
      phone: [
        this.formData ? this.formData.phone : '', Validators.compose([Validators.required])],
    });
  }
  setPaymentMethod(method){
    this.paymentmethod = method
  }
  private loadData(): any {
    this.spinner.show();
    this._httpService.get(`payment/list`).subscribe(
        result => {
          console.log(result)
          if (result.success) {
            this.paymentMethodsData = result.data;
            this.paymentmethod = result.data[0].id
            this.spinner.hide()
          } else {
            
          }
        },
        error => {
        },
        complete => {
        }
      );
  }
  handleAddToCartProduct (product) {
    this.cartService.addToCart(product)
    this.toastrService.success('1 product has been added to cart', 'cart', {
      positionClass: 'toast-bottom-right' 
   });
  }
  handleRemoveFromCartProduct (index) {
    this.cartService.removeItem(index)
    this.toastrService.success('1 product has been removed to cart', 'cart', {
      positionClass: 'toast-bottom-right' 
   });
  }
  handleReduceItemCartProduct (index) {
    this.cartService.reduceItemQuantity(index)
    this.toastrService.success('1 product has been removed from cart', 'cart', {
      positionClass: 'toast-bottom-right' 
   });
  }
  public submitData(): void {
    // this.loading = true;
    let formData  = new FormData();
    let result = Object.assign({}, this.form.value);
    for (let o in result) {
      formData.append(o, result[o])
    }
    
    let cartITEMS = []
    this.cartService.items.map((item) =>{
      cartITEMS.push({
        color_id : item.color_id,
        size_id : item.size_id,
        quantity : item.quantity,
        id : item.productId,
      })
    })
    formData.append("cartItems", JSON.stringify(cartITEMS))
    formData.append("payment_method_id", this.paymentmethod)

    this._httpService.post('invoice/save', formData).subscribe(
      result => {
        console.log(result)
        if (result.success) {
          this.toastrService.success('Record created successfully!', 'Created Successfully!');
          this.form.reset()
          this.cartService.clearCart()
          this.router.navigate(['/confirmed', result]);
        } else {
          this.handleErrorsFromServer(result);
        }
      },
      error => {
        this.loading = false;
        this.errorMessages = error.error.error_messages;
      },
      complete => {
        this.loading = false;
      }

    );
  }
  public handleErrorsFromServer(response: any) {
    this.loading = false;
    this.hasErrors = true;
    this.errorMessages = [];

    Object.entries(response.errors).forEach(
      ([key, value]) => // console.log(key, value)
        this.errorMessages.push(value)
    );
  }
}

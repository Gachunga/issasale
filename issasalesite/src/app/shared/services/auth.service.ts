import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from "@auth0/angular-jwt";

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    private loggedIn = false;
    public redirectURL = '';
    
    constructor(
        private _router: Router,
        private http: HttpClient,
        public helper: JwtHelperService
    ) { }
    private getHeaders(): HttpHeaders {
        return new HttpHeaders({
            'Content-Type': 'application/json'
        });
    }
    public login(username, password) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json; charset=UTF-8');

        return this.http.post(
            //    this._globalService.apiHost + '/login',
            '',
            //   JSON.stringify({
            //    'LoginForm':
            //  {
            //    'username': username,
            //   'password': password
            //    },
            //  }),
            {
                "password": "string",
                "username": "string"
            },
            { headers: this.getHeaders() }
        ).pipe(
            map(data => {
                console.log(data);
                if (data['success'] === true) {
                    localStorage.setItem('access_token', data['data']['access_token']);
                    this.loggedIn = true;
                } else {
                    localStorage.removeItem('access_token');
                    this.loggedIn = false;
                }
                return data;
            }));
    }


    public logout(): void {
        localStorage.removeItem('access_token');
        localStorage.removeItem('user');
        this.loggedIn = false;
        this._router.navigate(['/website/home']);
    }
    public getToken(): any {
        return localStorage.getItem('access_token');
    }
    public unauthorizedAccess(error: any): void {
        this.logout();
        
    }

    public isLoggedIn(): boolean {
     console.log(this.getJWTValue())
        return !this.helper.isTokenExpired(this.getToken());
    }

    public getJWTValue(): any {
        return this.helper.decodeToken(this.getToken());
    }

    private handleError(error: Response | any) {

        let errorMessage: any = {};
        // Connection error
        if (error.status === 0) {
            errorMessage = {
                success: false,
                status: 0,
                data: 'Sorry, there was a connection error occurred. Please try again.',
            };
        } else {
            errorMessage = error.json();
        }
        return Observable.throw(errorMessage);
    }
}

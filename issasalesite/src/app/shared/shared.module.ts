
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { LabelBooleanComponent } from './components/label-boolean/label-boolean.component';
import { LabelCompletedComponent } from './components/label-completed/label-completed.component';
import { LabelActiveComponent } from './components/label-active/label-active.component';
// import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
// import { FlexmonsterPivotModule } from 'ng-flexmonster';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';


@NgModule({
  declarations: [LabelBooleanComponent, LabelCompletedComponent, LabelActiveComponent],
  entryComponents: [LabelBooleanComponent, LabelCompletedComponent, LabelActiveComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgbModule,
    Ng2SmartTableModule,
    PerfectScrollbarModule,
  //   SweetAlert2Module.forRoot({
  //     buttonsStyling: false,
  //     customClass: 'modal-content',
  //     confirmButtonClass: 'btn btn-primary',
  //     cancelButtonClass: 'btn'
  // }),
  // AgmCoreModule.forRoot({
  //   // job apiKey: 'AIzaSyAwQJXjzQ82D6nQsjwHHYZ1T6tDlRJe220'
  //   apiKey: 'AIzaSyCeXaOKfJXQZuh-3wZmMmYSt5NruUJPVgU'
  //  }),
  //  FlexmonsterPivotModule,
   
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    // PipesModule,
    NgbModule,
    Ng2SmartTableModule,
    // MultiselectDropdownModule,
    LabelBooleanComponent,
    LabelCompletedComponent,
    LabelActiveComponent,
    // GoogleMapsComponent,
    // AgmCoreModule,
    // FlexmonsterPivotModule,
    PerfectScrollbarModule
  ]
})
export class SharedModule {
  constructor() {
  }
}

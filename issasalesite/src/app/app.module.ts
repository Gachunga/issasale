﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// used to create fake backend
import { fakeBackendProvider } from './_helpers';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { BasicAuthInterceptor, ErrorInterceptor } from './_helpers';
import { HomeComponent } from './home';
import { LoginComponent } from './login';;
import { HeaderComponent } from './layouts/header/header.component'
;
import { SidebarComponent } from './layouts/sidebar/sidebar.component'
;
import { FooterComponent } from './layouts/footer/footer.component'
;
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './shared/shared.module';
import { CountdownModule } from 'ngx-countdown';
import { ProductItemComponent } from './home/product-item/product-item.component';;
import { ProductDetailComponent } from './product-detail/product-detail.component'
;
import { QuickviewComponent } from './home/quickview/quickview.component'
;
import { CartComponent } from './cart/cart.component'
;
import { CheckoutComponent } from './checkout/checkout.component'
;
import { CategoryComponent } from './category/category.component'
;
import { CategoriesComponent } from './categories/categories.component'
;
import { CheckoutConfirmedComponent } from './checkout-confirmed/checkout-confirmed.component'
import { NgxSpinnerModule } from "ngx-spinner";
import {NgPipesModule} from 'ngx-pipes';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        AppRoutingModule,
        ToastrModule.forRoot(),       
        NgbModule,
        SharedModule,
        BrowserAnimationsModule,
        CountdownModule,
        NgxSpinnerModule,
        NgPipesModule 
    ],
    exports : [
        FormsModule
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        ProductItemComponent,
        LoginComponent,
        HeaderComponent,
        SidebarComponent,
        FooterComponent,
        ProductDetailComponent,
        QuickviewComponent,
        CartComponent,
        CheckoutComponent,
        CategoryComponent ,
        CategoriesComponent ,
        CheckoutConfirmedComponent 
    ],
        
    providers: [
        // { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
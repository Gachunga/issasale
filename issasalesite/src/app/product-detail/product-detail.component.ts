import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { CartService } from '../_services/cart.service'
import { ToastrService } from 'ngx-toastr';
import {Router} from '@angular/router';
import { GlobalService } from '../shared/services/global.service';
import { HttpService } from 'src/app/shared/services/http.service';

declare var $: any;

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  public scripts_loaded = false
  public product=null
  public url = ''
  public quantity = 0;
  public categoryProducts = []
  public quickviewproduct = null
  changeDetectorRef: ChangeDetectorRef
  constructor(
    public cartService: CartService,
    private toastrService : ToastrService,
    private router: Router,
    private _globalService: GlobalService,
    private _httpService : HttpService,
    changeDetectorRef: ChangeDetectorRef
  ) { 
    this.changeDetectorRef = changeDetectorRef;
    this.categoryProducts = []
  }
  
  ngOnInit(): void {
    this.url = this._globalService.imageHost
    this.product = this.cartService.detailProduct
    this.quantity = this.product.quantity
    this.loadScripts()
    this.loadData()
  }
  setQuickViewProduct(product){
    this.quickviewproduct=product
    $('#quickView').modal();
    setTimeout(() => {
      $('.selectpicker').selectpicker('refresh');
      this.loadScript()
    }, 100);
    // this.loadScript()
  }
  private loadData(): any {
    this._httpService.get(`product/maylike/${this.product.category.id}`).subscribe(
      result => {
        if (result.success) {
          this.categoryProducts = result.data;
          console.log(this.categoryProducts)
          this.loadScripts()
        } else {
          
        }
      },
      error => {
      },
      complete => {
      }
    );
  }
  handleAddToCart() {
    if(this.quantity==0)return
    this.cartService.addToCart(this.product, this.quantity)
    this.toastrService.success(this.quantity +' product' + (this.quantity>1 ? 's' : '')+ ' ha' + (this.quantity>1 ? 's' : 've')+ ' been added to cart');
  }
  handleAddToCartProduct (product) {
    this.cartService.addToCart(product)
    this.toastrService.success('1 product has been added to cart', 'cart', {
      positionClass: 'toast-bottom-right' 
   });
  }
  setDetailProduct(product){
    this.cartService.setDetailProduct(product)
    this.product = this.cartService.detailProduct
    this.loadScript()
    $("html, body").animate({ scrollTop: 10 }, "slow");
    return false;
  }
  private loadScripts() {
    const dynamicScripts = [
        "assets/js/photoswipe-init.ee6f4f5e.js",
        "assets/js/sliders-init.1db6fb07.js",
        "assets/js/theme.fe2c17cd.js"
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
    this.scripts_loaded = true
  }
  private loadScript() {
    const dynamicScripts = [
      "assets/js/sliders-init.1db6fb07.js",
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
    this.scripts_loaded = true
  }
}

import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http.service';
import { CartService } from '@app/_services/cart.service';
import { GlobalService } from '../shared/services/global.service';
import { timeout } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  public scripts_loaded = false;
  slidersData: [];
  public categoriesData;
  allProducts = []
  productsData = [];
  categoryProducts = [];
  productDisplayed = [];
  todaysDeals = []
  brandsData =[]
  sizesData =[]
  colorsData =[]
  cartItems = [];
  priceFrom = 100
  priceTo = 50000
  colorsChecked= [];
  brandsChecked= [];
  sizesChecked= [];
  header = "";
  headerdesc = ""
  url
  quickviewproduct
  page = 1;
  pageSize = 12
  category_ids = []
  type;
  type_id;

  constructor(
    private _httpService : HttpService,
    private _globalService: GlobalService, 
    public cartService: CartService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.loadData()
    this.loadScripts()
    this.loadScript()
    this.url = this._globalService.imageHost
    this.activatedRoute.params.subscribe(params => {
      this.type= params['type'];
      this.type_id = params['type_id'];
      });
  }
  displayNo(e, no){
    e.preventDefault()
    this.pageSize = no
  }
  getCategoryIds(categoryi){
    categoryi.children.map((category) => {
        this.category_ids.push(category.cat_id)
        if(category.children.length){
          this.getCategoryIds(category.children)
        }
    })
  }
  getCategory(categories, cat_id){
    let category_selected = null
    console.log(categories)
    if(categories.children){
    categories.children.map((category) => {
      if(category.cat_id == parseInt(cat_id)){
        console.log(category.cat_id)
        category_selected = category
      }else if(category.children.length){
        this.getCategory(category.children, cat_id)
      } 
    })
    }
    return category_selected
  }
  filter(e, cat_id, category){
    if(e){
      e.preventDefault()
    }
    this.category_ids = [cat_id]
    this.getCategoryIds(category)
    this.productsData = this.allProducts.filter((product) => this.category_ids.includes(product.category_id.toString()))
    this.categoryProducts = this.productsData
    this.header = category.label
    this.headerdesc = category.caption
  }
  filterNone(e){
    e.preventDefault()
    this.productsData = this.allProducts
    this.categoryProducts = this.allProducts
  }
  filterAll(){
    console.log("filtering")
    let products_filtered = []
    this.categoryProducts.map(product => {
      //filter price
      if(this.filterPrice(product)){
      //filter brand color size
      if(this.sizesChecked.length&&this.colorsChecked.length&&this.brandsChecked.length){
        if(this.filterSize(product)&&this.filterColors(product)&&this.filterBrand(product)){
          products_filtered.push(product)
        }
      }
      //sizes colors
      if(this.sizesChecked.length&&this.colorsChecked.length&&!this.brandsChecked.length){
        if(this.filterSize(product)&&this.filterColors(product)){
          products_filtered.push(product)
        }
      }
      //brand colors
      if(!this.sizesChecked.length&&this.colorsChecked.length&&this.brandsChecked.length){
        if(this.filterBrand(product)&&this.filterColors(product)){
          products_filtered.push(product)
        }
      }
      //brand size
      if(this.sizesChecked.length&&!this.colorsChecked.length&&this.brandsChecked.length){
        if(this.filterBrand(product)&&this.filterSize(product)){
          products_filtered.push(product)
        }
      }
      //sizes
      if(this.sizesChecked.length&&!this.colorsChecked.length&&!this.brandsChecked.length){
        if(this.filterSize(product)){
          products_filtered.push(product)
        }
      }
      //colors
      if(!this.sizesChecked.length&&this.colorsChecked.length&&!this.brandsChecked.length){
        if(this.filterColors(product)){
          products_filtered.push(product)
        }
      }
      //brands
      if(!this.sizesChecked.length&&!this.colorsChecked.length&&this.brandsChecked.length){
        if(this.filterBrand(product)){
          products_filtered.push(product)
        }
      }
      if(!this.sizesChecked.length&&!this.colorsChecked.length&&!this.brandsChecked.length){
          products_filtered.push(product)
      }
        
      }
    })
    this.productsData = products_filtered
  }
  filterPrice(product){
    let discounted_price = (product.product_prices[0].price/100)*(100-product.product_prices[0].discount)
    return (discounted_price>this.priceFrom&&discounted_price<this.priceTo)
  }
  filterBrand(product){
    return this.brandsChecked.includes(product.brand_id)
  }
  filterBrands(brand_id){
    this.productsData = this.allProducts.filter((product) => product.brand_id==brand_id)
    this.categoryProducts = this.productsData
    let brand = this.brandsData.find(brand => brand.id==brand_id)
    this.header = brand.name
    this.headerdesc = brand.description
  }
  filterSize(product){
    let exists = false
    product.product_colors.map((product_color) => {
      if(this.sizesChecked.includes(product_color.size_id)){
        exists = true
      }
    })
    return exists;
  }
  filterColors(product){
    let exists = false
    product.product_colors.map((product_color) => {
      if(this.colorsChecked.includes(product_color.color_id)){
        exists = true
      }
    })
    return exists;
  }
  setQuickViewProduct(product){
    this.quickviewproduct=product
    $('#quickView').modal();
    setTimeout(() => {
      $('.selectpicker').selectpicker('refresh');
      // this.loadScript()
    }, 100);
    // this.loadScript()
  }
  toggle (i) {
    $('#subcategories_'+i).toggleClass("hide show")
  }
  onCheckChange(id, checked){
    if(checked){
      this.colorsChecked.push(id)
    }else{
      let index=this.colorsChecked.findIndex(color => color==id)
      this.colorsChecked.splice(index, 1)
    }
    this.filterAll()
  }
  isItemChecked(id){
    return this.colorsChecked.findIndex(color => color==id)>=0
  }
  onCheckBrandChange(id, checked){
    if(checked){
      this.brandsChecked.push(id)
    }else{
      let index=this.brandsChecked.findIndex(brand_id => brand_id==id)
      this.brandsChecked.splice(index, 1)
    }
    this.filterAll()
  }
  isItemBrandChecked(id){
    return this.brandsChecked.findIndex(brand_id => brand_id==id)>=0
  }
  onCheckSizeChange(id, checked){
    if(checked){
      this.sizesChecked.push(id)
    }else{
      let index=this.sizesChecked.findIndex(size_id => size_id==id)
      this.sizesChecked.splice(index, 1)
    }
    this.filterAll()
  }
  isItemSizeChecked(id){
    return this.sizesChecked.findIndex(size_id => size_id==id)>=0
  }
  private loadData(): any {
    this._httpService.get(`category/listall`).subscribe(
        result => {
          console.log(result)
          if (result.success) {
            this.categoriesData = result.data[0];
            if(!this.type){
              this.header = this.categoriesData.label
              this.headerdesc = this.categoriesData.caption
            }
            this.loadProducts()
          } else {
            
          }
        },
        error => {
        },
        complete => {
        }
      );
      
      this._httpService.get(`brand/list`).subscribe(
        result => {
          if (result.success) {
            this.brandsData = result.data;
            
          } else {
            
          }
        },
        error => {
        },
        complete => {
        }
      );
      this._httpService.get(`size/list`).subscribe(
        result => {
          if (result.success) {
            this.sizesData = result.data;
            
          } else {
            
          }
        },
        error => {
        },
        complete => {
        }
      );
      this._httpService.get(`color/list`).subscribe(
        result => {
          if (result.success) {
            this.colorsData = result.data;
            
          } else {
            
          }
        },
        error => {
        },
        complete => {
        }
      );
  }
  private loadProducts() {
    this._httpService.get(`product/list`).subscribe(
      result => {
        if (result.success) {
          this.productsData = result.data;
          this.allProducts = result.data
          this.categoryProducts = result.data
          console.log(this.categoriesData)
          if(this.type=="category"){
            let category = this.getCategory(this.categoriesData, this.type_id)
            console.log(category)
            if(category){
              this.filter(null, this.type_id, category)
            }
          }else if(this.type=="brand"){
            this.filterBrands(this.type_id)
          }
        } else {
          
        }
      },
      error => {
      },
      complete => {
      }
    );
  }
  private loadScripts() {
    const dynamicScripts = [
        "assets/js/sliders-init.1db6fb07.js",
        "assets/js/theme.fe2c17cd.js"
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
    this.scripts_loaded = true
  }
  private loadScript() {
    const dynamicScripts = [
        "assets/js/quickviewslider.js",
        "assets/js/noui-slider.js",
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
    
    this.scripts_loaded = true
  }
}

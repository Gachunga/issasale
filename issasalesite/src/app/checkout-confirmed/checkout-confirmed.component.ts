import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkout-confirmed',
  templateUrl: './checkout-confirmed.component.html',
  styleUrls: ['./checkout-confirmed.component.css']
})
export class CheckoutConfirmedComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { GlobalService } from '../shared/services/global.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";

import { AuthenticationService } from '@app/_services';
declare var $: any;
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html', 
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    registerForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';
    base_url :string;
    loggedin : boolean = false
    public hasErrors = false;
    public errorMessages;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private _globalService: GlobalService,
        private toastrService : ToastrService,
        private spinner: NgxSpinnerService,
    ) { 
        // redirect to home if already logged in
        if (this.authenticationService.userValue) {
            this.loggedin = true 
            // this.router.navigate(['/home']);
        }
    }
   
    ngOnInit() {
        this.base_url = this._globalService.apiHost
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
        });
        this.registerForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
            email: ['', Validators.required]
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        this.spinner.show();
        this.loading = true;
        const info = {username: this.f.username.value, password: this.f.password.value}
        this.authenticationService.login(this.base_url, this.f.username.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    if(data.Code==201){
                        this.toastrService.success(data.Message, 'Success', {
                            positionClass: 'toast-bottom-right' 
                         });
                         this.registerForm.reset()
                         $('#loginModal').modal('toggle')    
                    }else{
                        this.error = data.Message
                        this.handleErrorsFromServer(data); 
                    }
                    this.loading = false;
                    this.spinner.hide();
                },
                error => {
                    console.log("error",error)
                    this.error = error;
                    this.loading = false;
                    this.toastrService.error(this.error, 'Success', {
                        positionClass: 'toast-bottom-right' 
                     });
                     this.spinner.hide();
                });
    }
    register() {
        this.submitted = true;
        // stop here if form is invalid
        console.log(this.registerForm.value)
        if (this.registerForm.invalid) {
            return;
        }
        this.loading = true;
        this.authenticationService.register(this.base_url, this.registerForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    if(data.Code==201){
                        // this.router.navigate([this.returnUrl]);
                        this.toastrService.success(data.Message, 'Error', {
                            positionClass: 'toast-bottom-right' 
                         });
                         this.registerForm.reset()
                         $('#loginModal').modal('toggle')   
                    }else{
                        this.error = data.Message
                        this.handleErrorsFromServer(data);
                    }
                    this.loading = false;
                },
                error => {
                    console.log("error",error)
                    this.error = error;
                    this.loading = false;
                    this.toastrService.error(this.error, 'Success', {
                        positionClass: 'toast-bottom-right' 
                     });
                });
    }
    public handleErrorsFromServer(response: any) {
        this.loading = false;
        this.hasErrors = true;
        this.errorMessages = [];
        console.log(response.errors)
        Object.entries(response.errors).forEach(
          ([key, value]) => {// console.log(key, value)
            this.errorMessages.push(value)
            this.toastrService.error(value.toString(), 'Error', {
                positionClass: 'toast-bottom-right' 
             });
            }
        );
      }
}

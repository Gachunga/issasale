import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CartService {
    items = [];
    itemsTotal = 0;
    cartTotal = 0;
    items_count = 0;
    detailProduct = []
    constructor(
    ) {
      this.setItems()
    }
  
    addToCart(product, quantity = 1) {      
      let item = this.items.find((p) => p.productId === product.id);
      if (item === undefined) {
        item = product
        item.productId = product.id;
        item.quantity  = quantity
        this.items.push(item);
      }else{
        item.quantity += quantity;
      }
      this.calculateCart();
      this.save()
    }
    removeItem(index){
      this.items.splice(index, 1)
      this.calculateCart()
      this.save()
    }
    reduceItemQuantity(index){
      let item = this.items[index]
      if(item.quantity==1){
        this.removeItem(index)
      }else{
       item.quantity = item.quantity-1
      }
      this.calculateCart()
      this.save()
    }
    private calculateCart(): void {
      this.itemsTotal = this.items
                            .map((item) =>  {
                              var subtotal = item.quantity * ((item.product_prices[0].price/100)*(100-item.product_prices[0].discount))
                              item.subtotal = subtotal
                              return subtotal
                              }
                            )
                            .reduce((previous, current) => previous + current, 0);
    }
    getItems() {
      return this.items;
    }
  
    clearCart() {
      this.items = [];
      this.itemsTotal = 0;
      this.cartTotal = 0;
      this.items_count = 0;
      this.save()
    }
    setDetailProduct(product){
      let item = this.items.find((p) => p.productId === product.id);
      if(item){

      }else{
        product.quantity  = 1
      }
      this.detailProduct = item ? item : product
      this.save()
    }
    save(){
      localStorage.setItem('cart', JSON.stringify({
        items : this.items,
        itemsTotal : this.itemsTotal,
        items_count : this.items_count,
        detailProduct : this.detailProduct
      }))
    }
    setItems(){
      let cart = JSON.parse(localStorage.getItem('cart'))
      if(cart){
        this.items = cart.items
        this.itemsTotal = cart.itemsTotal,
        this.items_count = cart.items_count,
        this.detailProduct = cart.detailProduct
      }
    } 
  }
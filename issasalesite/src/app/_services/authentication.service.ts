﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@environments/environment';
import { User } from '@app/_models';

declare var $: any;

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private userSubject: BehaviorSubject<User>;
    public user: Observable<User>;
    private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor(
        private router: Router,
        private http: HttpClient,
    ) {
        // localStorage.removeItem('user')
        this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
        localStorage.getItem('user') && this.loggedIn.next(true);
        this.user = this.userSubject.asObservable();
    }

    public get userValue(): User {
        return this.userSubject.value;
    }
    get isLoggedIn() {
        return this.loggedIn.asObservable();
    }
    private getHeaders(): HttpHeaders {
        return new HttpHeaders({
            'Content-Type': 'application/json'
        });
    }
    public getToken(): any {
        return localStorage.getItem('access_token');
    }
    login(url: string, email: string, password : string) {
        return this.http.post<any>(`${url}login`, { email, password })
            .pipe(map(user => {
                // store user details and basic auth credentials in local storage to keep user logged in between page refreshes
                // user.authdata = window.btoa(username + ':' + password);
                console.log(user)
                if(user.Code==201){
                localStorage.setItem('user', JSON.stringify(user));
                user.token = user.token
                this.userSubject.next(user);
                this.loggedIn.next(true);
                }
                return user;    
            }));
    }
    register(url: string, formdata) {
        return this.http.post<any>(`${url}auth/register`, formdata)
            .pipe(map(user => {
                // store user details and basic auth credentials in local storage to keep user logged in between page refreshes
                // user.authdata = window.btoa(username + ':' + password);
                console.log(user)
                if(user.Code==201){
                localStorage.setItem('user', JSON.stringify(user));
                user.token = user.token
                this.userSubject.next(user);
                this.loggedIn.next(true);
                }else{

                }
                return user;    
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('user');
        this.userSubject.next(null);
        this.loggedIn.next(false);
        $('#loginModal').modal('toggle') 
    }
}
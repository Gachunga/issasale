﻿import { Component } from '@angular/core';
import { first } from 'rxjs/operators';
import { HttpService } from 'src/app/shared/services/http.service';

import { User } from '@app/_models';
import { UserService } from '@app/_services';
import { CartService } from '@app/_services/cart.service';
import { GlobalService } from '../shared/services/global.service';
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';

declare var $: any;
var end = new Date();
end.setHours(23,59,59,999);
@Component({ 
  templateUrl: 'home.component.html',
  styleUrls: ['./home.component.css'], 
})
export class HomeComponent {
    loading = false;
    completed = false;
    slidersData: [];
    categoriesData : [];
    productsData : [];
    todaysDeals :[]
    brandsData :[]
    cartItems = [];
    public url = '';
    public quickviewproduct = null;
    public scripts_loaded = false;
    public configdd = {
      stopTime : end,
      format :'dd'
    }
    public confighrs = {
      stopTime : end,
      format :'HH'
    }
    public configmin = {
      stopTime : end,
      format :'mm'
    }
    public configsec = {
      stopTime : end,
      format :'ss'
    }
    constructor(
      private userService: UserService, private _httpService : HttpService, private _globalService: GlobalService, public cartService: CartService,
      private spinner: NgxSpinnerService,
      private router : Router ) { }

    ngOnInit() {
        this.url = this._globalService.imageHost
        this.loading = true;
        this.loadData()
        this.cartItems = this.cartService.getItems()
    }
    public goTocategory(brand){
      this.router.navigate(['/category/brand/id', brand.id]);
    }
    public goToDeal(deal){
      this.router.navigate(['/detail/id', deal.id]);
    }
    get getSetQuickViewProduct() {
      return this.setQuickViewProduct.bind(this);
    }
    setQuickViewProduct(product){
      this.quickviewproduct=product
      $('#quickView').modal();
      setTimeout(() => {
        $('.selectpicker').selectpicker('refresh');
        this.loadScript()
      }, 100);
      // this.loadScript()
    }
    private loadData(): any {
      this.spinner.show()
        this._httpService.get(`slider_image/list`).subscribe(
          result => {
            if (result.success) {
              this.slidersData = result.data;
              // this.loadScripts()
            } else {
              
            }
          },
          error => {
          },
          complete => {
            this.spinner.hide()
          }
        );
        this.spinner.show()
        this._httpService.get(`category/listmain`).subscribe(
            result => {
              if (result.success) {
                this.categoriesData = result.data;
                
              } else {
                
              }
            },
            error => {
            },
            complete => {
              this.spinner.hide()
            }
          );
          this.spinner.show()
          this._httpService.get(`product/listmain`).subscribe(
            result => {
              if (result.success) {
                this.productsData = result.data;
                
              } else {
                
              }
            },
            error => {
            },
            complete => {
              this.spinner.hide()
            }
          );
          this.spinner.show()
          this._httpService.get(`todays_deal/list`).subscribe(
            result => {
              console.log(result)
              if (result.success) {
                this.todaysDeals = result.data;
                setTimeout(() => {
                  this.loadScripts()
                  this.loadScript()
                }, 100);
                
              } else {
                
              }
            },
            error => {
            },
            complete => {
              this.spinner.hide()
            }
          );
          this.spinner.show()
          this._httpService.get(`brand/list`).subscribe(
            result => {
              if (result.success) {
                this.brandsData = result.data;
                
              } else {
                
              }
            },
            error => {
            },
            complete => {
              this.spinner.hide()
            }
          );
      }
      private loadScripts() {
        const dynamicScripts = [
            "assets/js/sliders-init.1db6fb07.js",
            "assets/js/theme.fe2c17cd.js"
        ];
        for (let i = 0; i < dynamicScripts.length; i++) {
          const node = document.createElement('script');
          node.src = dynamicScripts[i];
          node.type = 'text/javascript';
          node.async = false;
          node.charset = 'utf-8';
          document.getElementsByTagName('head')[0].appendChild(node);
        }
        this.scripts_loaded = true
      }
      private loadScript() {
        const dynamicScripts = [
            "assets/js/quickviewslider.js",
        ];
        for (let i = 0; i < dynamicScripts.length; i++) {
          const node = document.createElement('script');
          node.src = dynamicScripts[i];
          node.type = 'text/javascript';
          node.async = false;
          node.charset = 'utf-8';
          document.getElementsByTagName('head')[0].appendChild(node);
        }
        this.scripts_loaded = true
      }
}
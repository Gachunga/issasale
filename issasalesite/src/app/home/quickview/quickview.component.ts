import { Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { CartService } from '../../_services/cart.service'
import { ToastrService } from 'ngx-toastr';
import {Router} from '@angular/router';
import { GlobalService } from '../../shared/services/global.service';
import { HttpService } from 'src/app/shared/services/http.service';

@Component({
  selector: 'app-quickview',
  templateUrl: './quickview.component.html',
  styleUrls: ['./quickview.component.css']
})
export class QuickviewComponent implements OnChanges, OnInit   {
  @Input() quickviewproduct
  @Input() url
  public quantity = 1
  public colorsChecked = []
  public sizeselected
  public colorselected
  public colors = []
  public sizes= []

  constructor(
    private cartService : CartService,
    private toastrService : ToastrService,
    private router: Router,
    private _globalService: GlobalService,
    private _httpService : HttpService
  ) { }

  ngOnInit(): void {
    this.loadScript()
  }
  ngOnChanges(changes: SimpleChanges) {
    this.sizes = []
    this.colors = []
    const currentItem: SimpleChange = changes.quickviewproduct;
    if(currentItem.currentValue){
      let product = currentItem.currentValue
      if(!product){
        return
      }
      if(!product.product_colors.length){
        return
      }
      this.sizeselected = product.product_colors[0].size_id
      this.colorselected = product.product_colors[0].color_id
      product.product_colors.map((product_color) => {
        if(this.sizes.findIndex(size => size.id==product_color.size_id)<0){
          this.sizes.push({id: product_color.size.id, size: product_color.size.size})
        }
      })
      product.product_colors.map((product_color) => {
        if(this.colors.findIndex(color => color.id==product_color.color_id)<0){
          this.colors.push({id: product_color.color.id, color: product_color.color.color})
        }
      })
    }
  }
  onCheckChange(id, checked){
    
    if(checked){
      this.colorselected=id
    }else{
      // this.color = ''
    }
  }
  isItemChecked(id){
    return this.colorsChecked.findIndex(color => color==id)>=0
  }
  handleAddToCart() {
    if(this.quantity==0)return
    this.quickviewproduct.size_id = this.sizeselected
    this.quickviewproduct.color_id = this.colorselected
    console.log(this.quickviewproduct)
    this.cartService.addToCart(this.quickviewproduct, this.quantity)
    this.toastrService.success(this.quantity +' product' + (this.quantity>1 ? 's' : '')+ ' ha' + (this.quantity>1 ? 's' : 've')+ ' been added to cart', 'cart', {
      positionClass: 'toast-bottom-right' 
   });
  }
  private loadScript() {
    const dynamicScripts = [
        "assets/js/quickviewslider.js",
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }
}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { CartService } from '../../_services/cart.service';
import { ToastrService } from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit {

  @Input() product;
  @Input() url;
  @Output("setQuickViewProduct") setQuickViewProduct: EventEmitter<any> = new EventEmitter();
  childSetQuickViewProduct (){
    this.setQuickViewProduct.next(this.product);
  }
  constructor(
    private cartService: CartService,
    private toastrService : ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
  }
  setDetailProduct(){
    this.cartService.setDetailProduct(this.product)
    this.router.navigateByUrl('/detail');
  }
  handleAddToCart() {
    this.cartService.addToCart(this.product)
    this.toastrService.success('1 product has been added to cart', 'cart', {
      positionClass: 'toast-bottom-right' 
    });
  }
}

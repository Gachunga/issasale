var snapSlider = document.getElementById('slider-snap');
      
      noUiSlider.create(snapSlider, {
      	start: [ 100, 50000 ],
      	snap: false,
      	connect: true,
          step: 1,
      	range: {
      		'min': 0,
      		'max': 300000
      	}
      });
      var snapValues = [
      	document.getElementById('slider-snap-value-lower'),
      	document.getElementById('slider-snap-value-upper')
      ];
      var inputValues = [
      	document.getElementById('slider-snap-input-lower'),
      	document.getElementById('slider-snap-input-upper')
      ];
      snapSlider.noUiSlider.on('update', function( values, handle ) {
      	snapValues[handle].innerHTML = values[handle];
      });        
      
      snapSlider.noUiSlider.on('change', function( values, handle ) {
          inputValues[handle].value = values[handle];
      }); 
             
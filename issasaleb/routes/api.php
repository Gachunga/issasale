<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

// Route::options('{any?}', function (){
//     return response('',200);
// })->where('any', '.*');

Route::group(['prefix' => 'v1', 'namespace' => 'API\V1', 'middleware' => ['json.response']], function () {
        // public routes
        Route::post('/login', 'AuthController@login')->name('login.api');
        Route::post('/logincode', 'AuthController@loginCode')->name('login.code');
        Route::post('/loginanonymous', 'AuthController@loginAnonymous')->name('login.anonymous');
        Route::post('auth/register', 'AuthController@register')->name('register.api');
        Route::get('slider_image/list', 'SliderImageAPIController@index');
        Route::get('category/listmain', 'CategoryAPIController@listMain');
        Route::get('product/listmain', 'ProductAPIController@listMain');
        Route::get('product/list', 'ProductAPIController@list');
        Route::get('todays_deal/list', 'TodaysDealAPIController@list');
        Route::get('brand/list', 'BrandAPIController@list');
        Route::get('product/maylike/{id}', 'ProductAPIController@mayAlsoLike');
        Route::get('category/listall', 'CategoryAPIController@index');
        Route::get('size/list', 'SizeAPIController@index');
        Route::get('color/list', 'ColorAPIController@index');
        Route::get('payment/list', 'PaymentMethodAPIController@index');
        Route::post('invoice/save', 'InvoiceAPIController@store');
    });
    // private routes
    Route::group(['prefix' => 'v1', 'namespace' => 'API\V1', 'middleware' => ['auth:api', 'json.response'], 'as' => 'api.'], function () {
        Route::resource('roles', 'Auth\Role\RoleAPIController');
        Route::resource('permissions', 'Auth\PermissionAPIController');
        Route::resource('users', 'Auth\User\UserAPIController');
        Route::resource('brands', 'BrandAPIController');
        Route::resource('categories', 'CategoryAPIController');
        Route::get('category/list', 'CategoryAPIController@list');
        Route::resource('products', 'ProductAPIController');
        Route::post('product/update/{id}', 'ProductAPIController@update');
        Route::resource('discount_types', 'DiscountTypeAPIController');
        Route::resource('product_quantities', 'ProductQuantityAPIController');
        Route::resource('stock_changes', 'StockChangeAPIController');
        Route::resource('product_sizes', 'ProductSizeAPIController');
        Route::resource('slider_images', 'SliderImageAPIController');
        Route::resource('todays_deals', 'TodaysDealAPIController');
        Route::resource('colors', 'ColorAPIController');
        Route::resource('sizes', 'SizeAPIController');
        Route::resource('invoices', 'InvoiceAPIController');
        Route::resource('invoice_details', 'InvoiceDetailAPIController');
        Route::resource('payment_methods', 'PaymentMethodAPIController');
        Route::post('slider_images/update/{id}', 'SliderImageAPIController@update');
    });


Route::resource('product_stock_breakdowns', 'ProductStockBreakdownAPIController');


Route::resource('colors', 'ColorAPIController');

Route::resource('sizes', 'SizeAPIController');


Route::resource('payment_methods', 'PaymentMethodAPIController');

Route::resource('statuses', 'StatusAPIController');
<?php
Breadcrumbs::macro('resource', function ($name, $title) {
    // Home > Blog
    Breadcrumbs::for("$name.index", function ($trail) use ($name, $title) {
        // $trail->parent('admin.dashboard');
        $trail->push($title, route("$name.index"));
    });

    // Home > Blog > New
    Breadcrumbs::for("$name.create", function ($trail) use ($name) {
        $trail->parent("$name.index");
        $trail->push('New', route("$name.create"));
    });

    // Home > Blog > Post 123
    Breadcrumbs::for("$name.show", function ($trail, $model) use ($name, $title) {
        $trail->parent("$name.index");
        $trail->push($title, route("$name.show", $model));
    });

    // Home > Blog > Post 123 > Edit
    Breadcrumbs::for("$name.edit", function ($trail, $model) use ($name, $title) {
        $trail->parent("$name.show", $model);
        $trail->push('Edit', route("$name.edit", $model));
    });
});

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});
Breadcrumbs::resource('admin.companies', 'Companies');
Breadcrumbs::resource('admin.companyBuses', 'Company Buses');
Breadcrumbs::resource('admin.locations', 'Location');
Breadcrumbs::resource('admin.routes', 'Routes');
Breadcrumbs::resource('admin.busRoutes', 'Bus Routes');
Breadcrumbs::resource('admin.bookings', 'Bookings');

require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';

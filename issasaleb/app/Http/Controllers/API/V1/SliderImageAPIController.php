<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\CreateSliderImageAPIRequest;
use App\Http\Requests\API\V1\UpdateSliderImageAPIRequest;
use App\Models\SliderImage;
use App\Repositories\Backend\SliderImageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SliderImageController
 * @package App\Http\Controllers\API\V1
 */

class SliderImageAPIController extends AppBaseController
{
    /** @var  SliderImageRepository */
    private $sliderImageRepository;

    public function __construct(SliderImageRepository $sliderImageRepo)
    {
        $this->sliderImageRepository = $sliderImageRepo;
    }

    /**
     * Display a listing of the SliderImage.
     * GET|HEAD /sliderImages
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $sliderImages = $this->sliderImageRepository->orderBy('level', 'ASC')->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($sliderImages->toArray(), 'Slider Images retrieved successfully');
    }

    /**
     * Store a newly created SliderImage in storage.
     * POST /sliderImages
     *
     * @param CreateSliderImageAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSliderImageAPIRequest $request)
    {
        $input = $request->all();
        $files = $request->file('file');
        $sliderImage = $this->sliderImageRepository->create($input, $files);

        return $this->sendResponse($sliderImage->toArray(), 'Slider Image saved successfully');
    }

    /**
     * Display the specified SliderImage.
     * GET|HEAD /sliderImages/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SliderImage $sliderImage */
        $sliderImage = $this->sliderImageRepository->find($id);

        if (empty($sliderImage)) {
            return $this->sendError('Slider Image not found');
        }

        return $this->sendResponse($sliderImage->toArray(), 'Slider Image retrieved successfully');
    }

    /**
     * Update the specified SliderImage in storage.
     * PUT/PATCH /sliderImages/{id}
     *
     * @param int $id
     * @param UpdateSliderImageAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSliderImageAPIRequest $request)
    {
        $input = $request->all();

        /** @var SliderImage $sliderImage */
        $sliderImage = $this->sliderImageRepository->find($id);

        if (empty($sliderImage)) {
            return $this->sendError('Slider Image not found');
        }
        $files = $request->file('file');
        $sliderImage = $this->sliderImageRepository->update($input, $id, $files);

        return $this->sendResponse($sliderImage->toArray(), 'SliderImage updated successfully');
    }

    /**
     * Remove the specified SliderImage from storage.
     * DELETE /sliderImages/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SliderImage $sliderImage */
        $sliderImage = $this->sliderImageRepository->find($id);

        if (empty($sliderImage)) {
            return $this->sendError('Slider Image not found');
        }

        $sliderImage->delete();

        return $this->sendSuccess('Slider Image deleted successfully');
    }
}

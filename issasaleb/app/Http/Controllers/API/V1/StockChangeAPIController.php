<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\CreateStockChangeAPIRequest;
use App\Http\Requests\API\V1\UpdateStockChangeAPIRequest;
use App\Models\StockChange;
use App\Repositories\Backend\StockChangeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class StockChangeController
 * @package App\Http\Controllers\API\V1
 */

class StockChangeAPIController extends AppBaseController
{
    /** @var  StockChangeRepository */
    private $stockChangeRepository;

    public function __construct(StockChangeRepository $stockChangeRepo)
    {
        $this->stockChangeRepository = $stockChangeRepo;
    }

    /**
     * Display a listing of the StockChange.
     * GET|HEAD /stockChanges
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $stockChanges = $this->stockChangeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($stockChanges->toArray(), 'Stock Changes retrieved successfully');
    }

    /**
     * Store a newly created StockChange in storage.
     * POST /stockChanges
     *
     * @param CreateStockChangeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStockChangeAPIRequest $request)
    {
        $input = $request->all();

        $stockChange = $this->stockChangeRepository->create($input);

        return $this->sendResponse($stockChange->toArray(), 'Stock Change saved successfully');
    }

    /**
     * Display the specified StockChange.
     * GET|HEAD /stockChanges/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var StockChange $stockChange */
        $stockChange = $this->stockChangeRepository->find($id);

        if (empty($stockChange)) {
            return $this->sendError('Stock Change not found');
        }

        return $this->sendResponse($stockChange->toArray(), 'Stock Change retrieved successfully');
    }

    /**
     * Update the specified StockChange in storage.
     * PUT/PATCH /stockChanges/{id}
     *
     * @param int $id
     * @param UpdateStockChangeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStockChangeAPIRequest $request)
    {
        $input = $request->all();

        /** @var StockChange $stockChange */
        $stockChange = $this->stockChangeRepository->find($id);

        if (empty($stockChange)) {
            return $this->sendError('Stock Change not found');
        }

        $stockChange = $this->stockChangeRepository->update($input, $id);

        return $this->sendResponse($stockChange->toArray(), 'StockChange updated successfully');
    }

    /**
     * Remove the specified StockChange from storage.
     * DELETE /stockChanges/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var StockChange $stockChange */
        $stockChange = $this->stockChangeRepository->find($id);

        if (empty($stockChange)) {
            return $this->sendError('Stock Change not found');
        }

        $stockChange->delete();

        return $this->sendSuccess('Stock Change deleted successfully');
    }
}

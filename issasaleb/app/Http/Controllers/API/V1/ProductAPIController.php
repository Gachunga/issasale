<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\CreateProductAPIRequest;
use App\Http\Requests\API\V1\UpdateProductAPIRequest;
use App\Models\Product;
use App\Repositories\Backend\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Log;
use Response;

/**
 * Class ProductController
 * @package App\Http\Controllers\API\V1
 */

class ProductAPIController extends AppBaseController
{
    /** @var  ProductRepository */
    private $productRepository;

    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepository = $productRepo;
    }

    /**
     * Display a listing of the Product.
     * GET|HEAD /products
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $products = Product::with('product_images', 'product_colors','product_colors.color', 'product_colors.size', 'product_prices', 'category')->orderBy('created_at', 'desc')->get();

        return $this->sendResponse($products->toArray(), 'Products retrieved successfully');
    }
    public function list(Request $request)
    {
        $products = Product::with('product_images', 'product_colors','product_colors.color', 'product_colors.size', 'product_prices', 'category', 'brand')->orderBy('created_at', 'desc')->get();

        return $this->sendResponse($products->toArray(), 'Products retrieved successfully');
    }
    public function listMain(Request $request)
    {
        $products = $this->productRepository->limit(20)->with('product_images', 'product_prices','product_colors', 'category', 'product_sizes','product_colors.color', 'product_colors.size')->get();

        return $this->sendResponse($products, 'Products retrieved successfully');
    }
    public function mayAlsoLike($id)
    {
        $products = $this->productRepository->limit(10)->with('product_images', 'product_prices','product_colors', 'category', 'product_sizes')->where('category_id', $id)->get();

        return $this->sendResponse($products, 'Products retrieved successfully');
    }

    /**
     * Store a newly created Product in storage.
     * POST /products
     *
     * @param CreateProductAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateProductAPIRequest $request)
    {
        $input = $request->all();
        $files = $request->file('images');
        $product = $this->productRepository->create($input, $files);

        return $this->sendResponse($product->toArray(), 'Product saved successfully');
    }

    /**
     * Display the specified Product.
     * GET|HEAD /products/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Product $product */
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            return $this->sendError('Product not found');
        }

        return $this->sendResponse($product->toArray(), 'Product retrieved successfully');
    }

    /**
     * Update the specified Product in storage.
     * PUT/PATCH /products/{id}
     *
     * @param int $id
     * @param UpdateProductAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductAPIRequest $request)
    {
        $input = $request->all();
        $files = $request->file('images');
        // Log::debug($input);
        /** @var Product $product */
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            return $this->sendError('Product not found');
        }

        $product = $this->productRepository->update($input, $id, $files);

        return $this->sendResponse($product->toArray(), 'Product updated successfully');
    }

    /**
     * Remove the specified Product from storage.
     * DELETE /products/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Product $product */
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            return $this->sendError('Product not found');
        }

        $product->delete();

        return $this->sendSuccess('Product deleted successfully');
    }
}

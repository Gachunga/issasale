<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\CreateProductStockBreakdownAPIRequest;
use App\Http\Requests\API\V1\UpdateProductStockBreakdownAPIRequest;
use App\Models\ProductStockBreakdown;
use App\Repositories\Backend\ProductStockBreakdownRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ProductStockBreakdownController
 * @package App\Http\Controllers\API\V1
 */

class ProductStockBreakdownAPIController extends AppBaseController
{
    /** @var  ProductStockBreakdownRepository */
    private $productStockBreakdownRepository;

    public function __construct(ProductStockBreakdownRepository $productStockBreakdownRepo)
    {
        $this->productStockBreakdownRepository = $productStockBreakdownRepo;
    }

    /**
     * Display a listing of the ProductStockBreakdown.
     * GET|HEAD /productStockBreakdowns
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $productStockBreakdowns = $this->productStockBreakdownRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($productStockBreakdowns->toArray(), 'Product Stock Breakdowns retrieved successfully');
    }

    /**
     * Store a newly created ProductStockBreakdown in storage.
     * POST /productStockBreakdowns
     *
     * @param CreateProductStockBreakdownAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateProductStockBreakdownAPIRequest $request)
    {
        $input = $request->all();

        $productStockBreakdown = $this->productStockBreakdownRepository->create($input);

        return $this->sendResponse($productStockBreakdown->toArray(), 'Product Stock Breakdown saved successfully');
    }

    /**
     * Display the specified ProductStockBreakdown.
     * GET|HEAD /productStockBreakdowns/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ProductStockBreakdown $productStockBreakdown */
        $productStockBreakdown = $this->productStockBreakdownRepository->find($id);

        if (empty($productStockBreakdown)) {
            return $this->sendError('Product Stock Breakdown not found');
        }

        return $this->sendResponse($productStockBreakdown->toArray(), 'Product Stock Breakdown retrieved successfully');
    }

    /**
     * Update the specified ProductStockBreakdown in storage.
     * PUT/PATCH /productStockBreakdowns/{id}
     *
     * @param int $id
     * @param UpdateProductStockBreakdownAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductStockBreakdownAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProductStockBreakdown $productStockBreakdown */
        $productStockBreakdown = $this->productStockBreakdownRepository->find($id);

        if (empty($productStockBreakdown)) {
            return $this->sendError('Product Stock Breakdown not found');
        }

        $productStockBreakdown = $this->productStockBreakdownRepository->update($input, $id);

        return $this->sendResponse($productStockBreakdown->toArray(), 'ProductStockBreakdown updated successfully');
    }

    /**
     * Remove the specified ProductStockBreakdown from storage.
     * DELETE /productStockBreakdowns/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ProductStockBreakdown $productStockBreakdown */
        $productStockBreakdown = $this->productStockBreakdownRepository->find($id);

        if (empty($productStockBreakdown)) {
            return $this->sendError('Product Stock Breakdown not found');
        }

        $productStockBreakdown->delete();

        return $this->sendSuccess('Product Stock Breakdown deleted successfully');
    }
}

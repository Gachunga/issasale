<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\CreateTodaysDealAPIRequest;
use App\Http\Requests\API\V1\UpdateTodaysDealAPIRequest;
use App\Models\TodaysDeal;
use App\Repositories\Backend\TodaysDealRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Carbon\Carbon;
use Response;

/**
 * Class TodaysDealController
 * @package App\Http\Controllers\API\V1
 */

class TodaysDealAPIController extends AppBaseController
{
    /** @var  TodaysDealRepository */
    private $todaysDealRepository;

    public function __construct(TodaysDealRepository $todaysDealRepo)
    {
        $this->todaysDealRepository = $todaysDealRepo;
    }

    /**
     * Display a listing of the TodaysDeal.
     * GET|HEAD /todaysDeals
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $todaysDeals = $this->todaysDealRepository->with('product.product_prices', 'product_image', 'discount_type')->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($todaysDeals->toArray(), 'Todays Deals retrieved successfully');
    }
    public function list(Request $request)
    {
        $todaysDeals = TodaysDeal::whereDate('date', Carbon::today())->with('product.product_prices', 'product_image', 'discount_type')->get();

        return $this->sendResponse($todaysDeals->toArray(), 'Todays Deals retrieved successfully');
    }

    /**
     * Store a newly created TodaysDeal in storage.
     * POST /todaysDeals
     *
     * @param CreateTodaysDealAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTodaysDealAPIRequest $request)
    {
        $input = $request->all();

        $todaysDeal = $this->todaysDealRepository->create($input);

        return $this->sendResponse($todaysDeal->toArray(), 'Todays Deal saved successfully');
    }

    /**
     * Display the specified TodaysDeal.
     * GET|HEAD /todaysDeals/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TodaysDeal $todaysDeal */
        $todaysDeal = $this->todaysDealRepository->find($id);

        if (empty($todaysDeal)) {
            return $this->sendError('Todays Deal not found');
        }

        return $this->sendResponse($todaysDeal->toArray(), 'Todays Deal retrieved successfully');
    }

    /**
     * Update the specified TodaysDeal in storage.
     * PUT/PATCH /todaysDeals/{id}
     *
     * @param int $id
     * @param UpdateTodaysDealAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTodaysDealAPIRequest $request)
    {
        $input = $request->all();

        /** @var TodaysDeal $todaysDeal */
        $todaysDeal = $this->todaysDealRepository->find($id);

        if (empty($todaysDeal)) {
            return $this->sendError('Todays Deal not found');
        }

        $todaysDeal = $this->todaysDealRepository->update($input, $id);

        return $this->sendResponse($todaysDeal->toArray(), 'TodaysDeal updated successfully');
    }

    /**
     * Remove the specified TodaysDeal from storage.
     * DELETE /todaysDeals/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TodaysDeal $todaysDeal */
        $todaysDeal = $this->todaysDealRepository->find($id);

        if (empty($todaysDeal)) {
            return $this->sendError('Todays Deal not found');
        }

        $todaysDeal->delete();

        return $this->sendSuccess('Todays Deal deleted successfully');
    }
}

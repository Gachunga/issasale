<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\CreateProductQuantityAPIRequest;
use App\Http\Requests\API\V1\UpdateProductQuantityAPIRequest;
use App\Models\ProductQuantity;
use App\Repositories\Backend\ProductQuantityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\ProductColor;
use Illuminate\Support\Facades\DB;
use Log;
use Response;

/**
 * Class ProductQuantityController
 * @package App\Http\Controllers\API\V1
 */

class ProductQuantityAPIController extends AppBaseController
{
    /** @var  ProductQuantityRepository */
    private $productQuantityRepository;

    public function __construct(ProductQuantityRepository $productQuantityRepo)
    {
        $this->productQuantityRepository = $productQuantityRepo;
    }

    /**
     * Display a listing of the ProductQuantity.
     * GET|HEAD /productQuantities
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $productQuantities = DB::table('product_colors as pc')
                ->leftJoin('colors as c', 'pc.color_id', '=', 'c.id')
                ->leftJoin('sizes as s', 'pc.size_id', '=', 's.id')
                ->leftJoin('products as ps', 'pc.product_id', '=', 'ps.id')
                ->select('*', 'pc.id as id')
                ->get();

        return $this->sendResponse($productQuantities->toArray(), 'Product Quantities retrieved successfully');
    }

    /**
     * Store a newly created ProductQuantity in storage.
     * POST /productQuantities
     *
     * @param CreateProductQuantityAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateProductQuantityAPIRequest $request)
    {
        $input = $request->all();
        Log::debug($input);
        $productQuantity = $this->productQuantityRepository->create($input);

        return $this->sendResponse($productQuantity->toArray(), 'Product Quantity saved successfully');
    }

    /**
     * Display the specified ProductQuantity.
     * GET|HEAD /productQuantities/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ProductQuantity $productQuantity */
        $productQuantity = $this->productQuantityRepository->find($id);

        if (empty($productQuantity)) {
            return $this->sendError('Product Quantity not found');
        }

        return $this->sendResponse($productQuantity->toArray(), 'Product Quantity retrieved successfully');
    }

    /**
     * Update the specified ProductQuantity in storage.
     * PUT/PATCH /productQuantities/{id}
     *
     * @param int $id
     * @param UpdateProductQuantityAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductQuantityAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProductQuantity $productQuantity */
        $productQuantity = $this->productQuantityRepository->find($id);

        if (empty($productQuantity)) {
            return $this->sendError('Product Quantity not found');
        }

        $productQuantity = $this->productQuantityRepository->update($input, $id);

        return $this->sendResponse($productQuantity->toArray(), 'ProductQuantity updated successfully');
    }

    /**
     * Remove the specified ProductQuantity from storage.
     * DELETE /productQuantities/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ProductQuantity $productQuantity */
        $productQuantity = $this->productQuantityRepository->find($id);

        if (empty($productQuantity)) {
            return $this->sendError('Product Quantity not found');
        }

        $productQuantity->delete();

        return $this->sendSuccess('Product Quantity deleted successfully');
    }
}

<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\V1\CreateDiscountTypeAPIRequest;
use App\Http\Requests\API\V1\UpdateDiscountTypeAPIRequest;
use App\Models\DiscountType;
use App\Repositories\Backend\DiscountTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DiscountTypeController
 * @package App\Http\Controllers\API\V1
 */

class DiscountTypeAPIController extends AppBaseController
{
    /** @var  DiscountTypeRepository */
    private $discountTypeRepository;

    public function __construct(DiscountTypeRepository $discountTypeRepo)
    {
        $this->discountTypeRepository = $discountTypeRepo;
    }

    /**
     * Display a listing of the DiscountType.
     * GET|HEAD /discountTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $discountTypes = $this->discountTypeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($discountTypes->toArray(), 'Discount Types retrieved successfully');
    }

    /**
     * Store a newly created DiscountType in storage.
     * POST /discountTypes
     *
     * @param CreateDiscountTypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDiscountTypeAPIRequest $request)
    {
        $input = $request->all();

        $discountType = $this->discountTypeRepository->create($input);

        return $this->sendResponse($discountType->toArray(), 'Discount Type saved successfully');
    }

    /**
     * Display the specified DiscountType.
     * GET|HEAD /discountTypes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DiscountType $discountType */
        $discountType = $this->discountTypeRepository->find($id);

        if (empty($discountType)) {
            return $this->sendError('Discount Type not found');
        }

        return $this->sendResponse($discountType->toArray(), 'Discount Type retrieved successfully');
    }

    /**
     * Update the specified DiscountType in storage.
     * PUT/PATCH /discountTypes/{id}
     *
     * @param int $id
     * @param UpdateDiscountTypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDiscountTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var DiscountType $discountType */
        $discountType = $this->discountTypeRepository->find($id);

        if (empty($discountType)) {
            return $this->sendError('Discount Type not found');
        }

        $discountType = $this->discountTypeRepository->update($input, $id);

        return $this->sendResponse($discountType->toArray(), 'DiscountType updated successfully');
    }

    /**
     * Remove the specified DiscountType from storage.
     * DELETE /discountTypes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DiscountType $discountType */
        $discountType = $this->discountTypeRepository->find($id);

        if (empty($discountType)) {
            return $this->sendError('Discount Type not found');
        }

        $discountType->delete();

        return $this->sendSuccess('Discount Type deleted successfully');
    }
}

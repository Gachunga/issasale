<?php

namespace App\Models;

use App\Models\Auth\User;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SeatReservation
 * @package App\Models
 * @version April 12, 2020, 9:52 am EAT
 *
 * @property \App\Models\User reservedBy
 * @property \App\Models\Seat seat
 * @property integer seat_id
 * @property string|\Carbon\Carbon reserved_at
 * @property integer reserved_by
 */
class SeatReservation extends Model
{
    use SoftDeletes;

    public $table = 'seat_reservations';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'seat_id',
        'reserved_at',
        'reserve_date',
        'reserved_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'seat_id' => 'integer',
        'reserved_at' => 'datetime',
        'reserved_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function reservedBy()
    {
        return $this->belongsTo(User::class, 'reserved_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function seat()
    {
        return $this->belongsTo(\App\Models\Seat::class, 'seat_id');
    }
}

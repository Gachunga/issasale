<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductPrice
 * @package App\Models
 * @version September 1, 2020, 11:17 am UTC
 *
 * @property integer $product_id
 * @property number $previous_price
 * @property number $price
 * @property integer $discount_type_id
 * @property number $discount
 * @property boolean $active
 */
class ProductPrice extends Model
{
    use SoftDeletes;

    public $table = 'product_prices';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'product_id',
        'previous_price',
        'price',
        'discount_type_id',
        'discount',
        'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'previous_price' => 'float',
        'price' => 'float',
        'discount_type_id' => 'integer',
        'discount' => 'float',
        'active' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'product_id' => 'required|integer',
        'previous_price' => 'nullable|numeric',
        'price' => 'required|numeric',
        'discount_type_id' => 'nullable|integer',
        'discount' => 'required|numeric',
        'active' => 'boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    
}

<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Ticket
 * @package App\Models
 * @version April 3, 2020, 4:51 pm UTC
 *
 * @property \App\Models\Booking booking
 * @property \App\Models\Passenger passenger
 * @property \App\Models\Seat seat
 * @property string ticket_ref
 * @property integer booking_id
 * @property integer passenger_id
 * @property integer seat_id
 * @property number amount
 */
class Ticket extends Model
{
    use SoftDeletes;

    public $table = 'tickets';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'ticket_ref',
        'booking_id',
        'passenger_id',
        'seat_id',
        'amount',
        'is_return',
        'ticket_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'ticket_ref' => 'string',
        'booking_id' => 'integer',
        'passenger_id' => 'integer',
        'seat_id' => 'integer',
        'amount' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'booking_id' => 'required',
        'passenger_id' => 'required',
        'seat_id' => 'required',
        'amount' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function booking()
    {
        return $this->belongsTo(\App\Models\Booking::class, 'booking_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function passenger()
    {
        return $this->belongsTo(\App\Models\Passenger::class, 'passenger_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function seat()
    {
        return $this->belongsTo(\App\Models\Seat::class, 'seat_id');
    }
}

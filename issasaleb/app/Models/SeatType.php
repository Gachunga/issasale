<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SeatType
 * @package App\Models
 * @version April 2, 2020, 10:14 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection routeSeatPrices
 * @property \Illuminate\Database\Eloquent\Collection seats
 * @property string name
 * @property string desc
 * @property boolean active
 */
class SeatType extends Model
{
    use SoftDeletes;

    public $table = 'seat_types';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'desc',
        'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'desc' => 'string',
        'active' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'active' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function routeSeatPrices()
    {
        return $this->hasMany(\App\Models\RouteSeatPrice::class, 'seat_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function seats()
    {
        return $this->hasMany(\App\Models\Seat::class, 'seat_type_id');
    }
}

<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Invoice
 * @package App\Models
 * @version September 28, 2020, 2:43 am UTC
 *
 * @property string $order_id
 * @property string $name
 * @property string $email
 * @property string $location
 * @property string $city
 * @property string $phone
 * @property string $status
 * @property boolean $is_order
 * @property boolean $is_paid
 * @property integer $payment_method_id
 */
class Invoice extends Model
{
    use SoftDeletes;

    public $table = 'invoices';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'order_id',
        'name',
        'email',
        'location',
        'city',
        'phone',
        'status',
        'status_id',
        'is_order',
        'is_paid',
        'payment_method_id',
        'user_id',
        'total',
        'sub_total',
        'shipping',
        'tax',
        'discount'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_id' => 'string',
        'name' => 'string',
        'email' => 'string',
        'location' => 'string',
        'city' => 'string',
        'phone' => 'string',
        'status' => 'string',
        'is_order' => 'boolean',
        'is_paid' => 'boolean',
        'payment_method_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:191',
        'email' => 'nullable|string|max:191',
        'location' => 'nullable|string|max:191',
        'city' => 'nullable|string|max:191',
        'phone' => 'nullable|string|max:191',
        'payment_method_id' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    
}

<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class InvoiceDetail
 * @package App\Models
 * @version September 28, 2020, 2:43 am UTC
 *
 * @property integer $invoice_id
 * @property integer $product_id
 * @property integer $size_id
 * @property integer $color_id
 * @property integer $quantity
 * @property number $discount
 * @property number $price
 * @property number $discounted_price
 * @property number $sub_total
 */
class InvoiceDetail extends Model
{
    use SoftDeletes;

    public $table = 'invoice_details';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'invoice_id',
        'product_id',
        'size_id',
        'color_id',
        'quantity',
        'discount',
        'price',
        'discounted_price',
        'sub_total',
        'product_color_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'invoice_id' => 'integer',
        'product_id' => 'integer',
        'size_id' => 'integer',
        'color_id' => 'integer',
        'quantity' => 'integer',
        'discount' => 'float',
        'price' => 'float',
        'discounted_price' => 'float',
        'sub_total' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'invoice_id' => 'required',
        'product_id' => 'required',
        'size_id' => 'required',
        'color_id' => 'required',
        'quantity' => 'required|integer',
        'discount' => 'nullable|numeric',
        'price' => 'nullable|numeric',
        'discounted_price' => 'nullable|numeric',
        'sub_total' => 'required|numeric',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    
}

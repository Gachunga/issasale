<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RouteSeatPrice
 * @package App\Models
 * @version April 2, 2020, 10:44 am UTC
 *
 * @property \App\Models\BusRoute busRoute
 * @property \App\Models\SeatType seatType
 * @property integer bus_route_id
 * @property integer seat_type_id
 * @property number price
 */
class RouteSeatPrice extends Model
{
    use SoftDeletes;

    public $table = 'route_seat_prices';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'bus_route_id',
        'seat_type_id',
        'price'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'bus_route_id' => 'integer',
        'seat_type_id' => 'integer',
        'price' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'bus_route_id' => 'required',
        'seat_type_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function busRoute()
    {
        return $this->belongsTo(\App\Models\BusRoute::class, 'bus_route_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function seatType()
    {
        return $this->belongsTo(\App\Models\SeatType::class, 'seat_type_id');
    }
}

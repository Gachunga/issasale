<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Brand
 * @package App\Models
 * @version August 30, 2020, 1:18 pm UTC
 *
 * @property string $name
 * @property string $description
 * @property string $brand_logo_url
 * @property integer $ranking
 */
class Brand extends Model
{
    use SoftDeletes;

    public $table = 'brands';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'description',
        'brand_logo_url',
        'ranking'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'ranking' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:50|unique:brands',
        'description' => 'nullable|string|max:100',
        'ranking' => 'nullable|integer|unique:brands',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    
}

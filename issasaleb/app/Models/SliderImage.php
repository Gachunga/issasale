<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SliderImage
 * @package App\Models
 * @version September 10, 2020, 1:07 pm UTC
 *
 * @property string $title_caption
 * @property string $title
 * @property string $title_desc
 * @property string $img_url
 */
class SliderImage extends Model
{
    use SoftDeletes;

    public $table = 'slider_images';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title_caption',
        'title',
        'title_desc',
        'img_url',
        'level',
        'background_color',
        'btn_text',
        'active',
        'discount',
        'price',
        'date_from',
        'date_to',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title_caption' => 'string',
        'title' => 'string',
        'title_desc' => 'string',
        'img_url' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title_caption' => 'nullable|string|max:191',
        'title' => 'required|string|max:191',
        'level' => 'integer|unique:slider_images',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public static $update_rules = [
        'title_caption' => 'nullable|string|max:191',
        'title' => 'required|string|max:191',
    ];
    
}

<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BusRouteDay
 * @package App\Models
 * @version April 11, 2020, 7:51 am UTC
 *
 * @property \App\Models\BusRoute busRoute
 * @property \App\Models\Day day
 * @property integer bus_route_id
 * @property integer day_id
 */
class BusRouteDay extends Model
{
    use SoftDeletes;

    public $table = 'bus_route_days';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'bus_route_id',
        'day_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'bus_route_id' => 'integer',
        'day_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'bus_route_id' => 'required',
        'day_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function busRoute()
    {
        return $this->belongsTo(\App\Models\BusRoute::class, 'bus_route_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function day()
    {
        return $this->belongsTo(\App\Models\Day::class, 'day_id');
    }
}

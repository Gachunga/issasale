<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductColor
 * @package App\Models
 * @version September 1, 2020, 9:22 am UTC
 *
 * @property integer $product_id
 * @property string $color
 * @property string $name
 * @property boolean $available
 */
class ProductColor extends Model
{
    use SoftDeletes;

    public $table = 'product_colors';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'product_id',
        'color',
        'color_id',
        'size_id',
        'name',
        'available'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'color' => 'string',
        'name' => 'string',
        'available' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'product_id' => 'required|integer',
        'color' => 'required|string|max:191',
        'name' => 'nullable|string|max:191',
        'available' => 'required|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function color()
    {
        return $this->hasOne(\App\Models\Color::class, 'id', 'color_id');
    }
    public function size()
    {
        return $this->hasOne(\App\Models\Size::class, 'id', 'size_id');
    }
    
}

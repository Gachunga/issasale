<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class StockChange
 * @package App\Models
 * @version September 2, 2020, 11:58 am UTC
 *
 * @property integer $product_id
 * @property string $action
 * @property integer $initial
 * @property integer $quantity
 * @property integer $final
 * @property integer $user_id
 */
class StockChange extends Model
{
    use SoftDeletes;

    public $table = 'stock_changes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'product_id',
        'action',
        'initial',
        'quantity',
        'final',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'action' => 'string',
        'initial' => 'integer',
        'quantity' => 'integer',
        'final' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'product_id' => 'required|integer',
        'action' => 'required|string|max:191',
        'initial' => 'required|integer',
        'quantity' => 'required|integer',
        'final' => 'required|integer',
        'user_id' => 'required|integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    
}

<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Seat
 * @package App\Models
 * @version April 2, 2020, 1:04 am UTC
 *
 * @property \App\Models\CompanyBus bus
 * @property integer bus_id
 * @property integer row_no
 * @property integer col_no
 * @property string seat_no
 * @property number seat_price
 * @property boolean is_business
 * @property boolean has_seat
 */
class Seat extends Model
{
    use SoftDeletes;

    public $table = 'seats';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'bus_id',
        'row_no',
        'col_no',
        'seat_no',
        'seat_price',
        'is_business',
        'has_seat'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'bus_id' => 'integer',
        'row_no' => 'integer',
        'col_no' => 'integer',
        'seat_no' => 'string',
        'seat_price' => 'float',
        'is_business' => 'boolean',
        'has_seat' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'bus_id' => 'required',
        'row_no' => 'required',
        'col_no' => 'required',
        'seat_no' => 'required',
        'is_business' => 'required',
        'has_seat' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function bus()
    {
        return $this->belongsTo(\App\Models\CompanyBus::class, 'bus_id');
    }
}

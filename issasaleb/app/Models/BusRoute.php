<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BusRoute
 * @package App\Models
 * @version March 29, 2020, 1:12 pm UTC
 *
 * @property \App\Models\CompanyBus bus
 * @property \App\Models\Route route
 * @property integer bus_id
 * @property integer route_id
 * @property string|\Carbon\Carbon departs
 * @property string|\Carbon\Carbon arrival
 * @property boolean recurring
 * @property number cost
 * @property integer currency
 * @property string desc
 */
class BusRoute extends Model
{
    use SoftDeletes;

    public $table = 'bus_routes';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'bus_id',
        'route_id',
        'departs',
        'arrival',
        'recurring',
        'cost',
        'currency',
        'desc',
        'payment_phone'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'bus_id' => 'integer',
        'route_id' => 'integer',
        'departs' => 'time',
        'arrival' => 'datetime',
        'recurring' => 'boolean',
        'cost' => 'float',
        'currency' => 'integer',
        'desc' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'bus_id' => 'required',
        'route_id' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function bus()
    {
        return $this->belongsTo(\App\Models\CompanyBus::class, 'bus_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function route()
    {
        return $this->belongsTo(\App\Models\Route::class, 'route_id');
    }
    public function seatPrices()
    {
        return $this->hasMany(\App\Models\RouteSeatPrice::class, 'bus_route_id');
    }
    public function bus_route_days()
    {
        return $this->hasMany(\App\Models\BusRouteDay::class, 'bus_route_id');
    }
}

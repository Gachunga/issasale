<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TodaysDeal
 * @package App\Models
 * @version September 12, 2020, 8:21 pm UTC
 *
 * @property integer $product_id
 * @property integer $product_image_id
 * @property integer $discount_type_id
 * @property number $discount
 * @property string $date
 * @property string $background_color
 */
class TodaysDeal extends Model
{
    use SoftDeletes;

    public $table = 'today_deals';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'product_id',
        'product_image_id',
        'discount_type_id',
        'discount',
        'date',
        'background_color',
        'level'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'product_image_id' => 'integer',
        'discount_type_id' => 'integer',
        'discount' => 'float',
        'date' => 'date',
        'background_color' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'product_id' => 'required',
        'product_image_id' => 'required',
        'discount_type_id' => 'nullable',
        'discount' => 'nullable|numeric',
        'date' => 'required',
        'background_color' => 'nullable|string|max:191',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function product()
    {
        return $this->hasOne(\App\Models\Product::class, 'id', 'product_id');
    }
    public function product_image()
    {
        return $this->hasOne(\App\Models\ProductImage::class, 'id', 'product_image_id');
    }
    public function discount_type()
    {
        return $this->hasOne(\App\Models\DiscountType::class, 'id', 'discount_type_id');
    }
}

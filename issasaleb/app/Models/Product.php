<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 * @package App\Models
 * @version August 31, 2020, 11:29 am UTC
 *
 * @property string $title
 * @property string $code
 * @property string $description
 * @property boolean $taxable
 * @property integer $category_id
 * @property integer $brand_id
 */
class Product extends Model
{
    use SoftDeletes;

    public $table = 'products';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'code',
        'description',
        'taxable',
        'category_id',
        'brand_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'code' => 'string',
        'description' => 'text',
        'category_id' => 'integer',
        'brand_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|string|max:191',
        'code' => 'nullable|string|max:191',
        'description' => 'nullable|string',
        'taxable' => 'required',
        'category_id' => 'nullable|integer',
        'brand_id' => 'nullable|integer',
    ];
    public function category()
    {
        return $this->hasOne(\App\Models\Category::class, 'id', 'category_id');
    }
    public function brand()
    {
        return $this->hasOne(\App\Models\Brand::class, 'id', 'brand_id');
    }
    public function product_images()
    {
        return $this->hasMany(\App\Models\ProductImage::class, 'product_id');
    }
    public function product_colors()
    {
        return $this->hasMany(\App\Models\ProductColor::class, 'product_id');
    }
    public function product_prices()
    {
        return $this->hasMany(\App\Models\ProductPrice::class, 'product_id');
    }
    public function product_sizes()
    {
        return $this->hasMany(\App\Models\ProductSize::class, 'product_id');
    }
    
}

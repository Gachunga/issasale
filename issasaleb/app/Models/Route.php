<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Route
 * @package App\Models
 * @version March 29, 2020, 1:02 pm UTC
 *
 * @property \App\Models\Company company
 * @property \App\Models\Location fromLocation
 * @property \App\Models\Location toLocation
 * @property integer from_location_id
 * @property integer to_location_id
 * @property string desc
 * @property integer company_id
 */
class Route extends Model
{
    use SoftDeletes;

    public $table = 'routes';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'from_location_id',
        'to_location_id',
        'desc',
        'company_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'from_location_id' => 'integer',
        'to_location_id' => 'integer',
        'desc' => 'string',
        'company_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'from_location_id' => 'required',
        'to_location_id' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function fromLocation()
    {
        return $this->belongsTo(\App\Models\Location::class, 'from_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function toLocation()
    {
        return $this->belongsTo(\App\Models\Location::class, 'to_location_id');
    }
}

<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Booking
 * @package App\Models
 * @version April 3, 2020, 4:50 pm UTC
 *
 * @property \App\Models\BusRoute busRoute
 * @property \App\Models\PaymentMethod paymentMethod
 * @property \Illuminate\Database\Eloquent\Collection tickets
 * @property integer bus_route_id
 * @property integer payment_method_id
 * @property number total_amount
 * @property boolean paid
 */
class Booking extends Model
{
    use SoftDeletes;

    public $table = 'bookings';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'bus_route_id',
        'payment_method_id',
        'total_amount',
        'paid',
        'payment_phone',
        'has_return',
        'booked_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'bus_route_id' => 'integer',
        'payment_method_id' => 'integer',
        'total_amount' => 'float',
        'paid' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function busRoute()
    {
        return $this->belongsTo(\App\Models\BusRoute::class, 'bus_route_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function paymentMethod()
    {
        return $this->belongsTo(\App\Models\PaymentMethod::class, 'payment_method_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function tickets()
    {
        return $this->hasMany(\App\Models\Ticket::class, 'booking_id');
    }
}

<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductStockBreakdown
 * @package App\Models
 * @version September 4, 2020, 12:47 am UTC
 *
 * @property integer $product_quantity_id
 * @property integer $product_size_id
 * @property integer $product_color_id
 * @property integer $quantity
 */
class ProductStockBreakdown extends Model
{
    use SoftDeletes;

    public $table = 'product_stock_breakdown';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'product_quantity_id',
        'product_size_id',
        'product_color_id',
        'quantity'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_quantity_id' => 'integer',
        'product_size_id' => 'integer',
        'product_color_id' => 'integer',
        'quantity' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'product_quantity_id' => 'required',
        'product_size_id' => 'required',
        'product_color_id' => 'required',
        'quantity' => 'nullable|integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    
}

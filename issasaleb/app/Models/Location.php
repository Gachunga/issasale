<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Location
 * @package App\Models
 * @version March 29, 2020, 1:01 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection routes
 * @property \Illuminate\Database\Eloquent\Collection route1s
 * @property string name
 * @property string desc
 */
class Location extends Model
{
    use SoftDeletes;

    public $table = 'locations';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'desc',
        'pick_up_location'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'desc' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function routes()
    {
        return $this->hasMany(\App\Models\Route::class, 'from_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function route1s()
    {
        return $this->hasMany(\App\Models\Route::class, 'to_location_id');
    }
}

<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CompanyBus
 * @package App\Models
 * @version March 29, 2020, 10:54 am UTC
 *
 * @property \App\Models\Company company
 * @property string name
 * @property string desc
 * @property integer type
 * @property integer company_id
 */
class CompanyBus extends Model
{
    use SoftDeletes;

    public $table = 'company_buses';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'desc',
        'type',
        'company_id',
        'rows_no',
        'cols_no',
        'bus_img_url'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'desc' => 'string',
        'type' => 'integer',
        'company_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'company_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id');
    }
    public function seats()
    {
        return $this->hasMany(\App\Models\Seat::class, 'bus_id');
    }
    public function seatsCount()
    {
    return $this->seats()
        ->selectRaw('bus_id, count(*) as aggregate')
        ->where('has_seat', true)
        ->groupBy('bus_id');
    }
}

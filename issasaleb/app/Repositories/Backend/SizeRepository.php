<?php

namespace App\Repositories\Backend;

use App\Models\Size;
use App\Repositories\BaseRepository;
use DB;

/**
 * Class SizeRepository
 * @package App\Repositories\Backend
 * @version September 24, 2020, 3:04 pm UTC
*/

class SizeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'size'
    ];
    public function __construct(Size $model)
    {
        $this->model = $model;
    }
    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function create(array $data) : Size
    {
        // Make sure it doesn't already exist
        
        return DB::transaction(function () use ($data) {
            $sizes = $data['sizes_arr'];
            if(!empty($sizes)&&$sizes!=null){
                foreach($sizes as $sizeobj){
                    $size = json_decode($sizeobj);
                    if ($this->CompanyExists($size)) {
                        throw new \Exception('A Size already exists with the name '.e($data['name']));
                    }
                    $Size = Size::create([
                        "size" => $size,
                    ]);
                }
            }
            
            if ($Size) {
                return $Size;
            }

            throw new \Exception('An error occured attempting to create Size');
        });
    }
    protected function CompanyExists($name) : bool
    {
        return $this->model
            ->where('size', strtolower($name))
            ->count() > 0;
    }
    public function find($id) : Size
    {
        return $this->model->find($id);
    }
    public function delete($id)
    {
        $model = $this->model->find($id);
        return $model->delete();
    }
}
<?php

namespace App\Repositories\Backend\Auth;

use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use DB;
use Spatie\Permission\Models\Permission;

/**
 * Class PermissionRepository.
 */
class PermissionRepository extends BaseRepository
{
    /**
     * PermissionRepository constructor.
     *
     * @param  Permission  $model
     */
    public function __construct(Permission $model)
    {
        $this->model = $model;
    }

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function create(array $data) : Permission
    {
        // Make sure it doesn't already exist
        if ($this->PermissionExists($data['name'])) {
            throw new GeneralException('A Permission already exists with the name '.e($data['name']));
        }

        return DB::transaction(function () use ($data) {
            $Client = $this->model::create($data);

            if ($Client) {
                return $Client;
            }

            throw new GeneralException('An error occured attempting to create Client');
        });
    }
    protected function PermissionExists($name) : bool
    {
        return $this->model
            ->where('name', strtolower($name))
            ->count() > 0;
    }
    public function find($id) : Permission
    {
        return $this->model->find($id);
    }
    public function delete($id)
    {
        $model = $this->model->find($id);
        return $model->delete();
    }
}
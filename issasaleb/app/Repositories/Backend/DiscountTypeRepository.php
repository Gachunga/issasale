<?php

namespace App\Repositories\Backend;

use App\Models\DiscountType;
use App\Repositories\BaseRepository;
use DB;

/**
 * Class DiscountTypeRepository
 * @package App\Repositories\Backend
 * @version September 1, 2020, 9:01 am UTC
*/

class DiscountTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function __construct(DiscountType $model)
    {
        $this->model = $model;
    }

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function create(array $data) : DiscountType
    {
        // Make sure it doesn't already exist
        if ($this->ProductExists($data['code'])) {
            throw new \Exception('A DiscountType already exists with the code '.e($data['code']));
        }

        return DB::transaction(function () use ($data) {
            $DiscountType = $this->model::create($data);

            if ($DiscountType) {
                return $DiscountType;
            }

            throw new \Exception('An error occured attempting to create DiscountType');
        });
    }
    protected function ProductExists($code) : bool
    {
        return $this->model
            ->where('code', strtolower($code))
            ->count() > 0;
    }
    public function find($id) : DiscountType
    {
        return $this->model->find($id);
    }
    public function delete($id)
    {
        $model = $this->model->find($id);
        return $model->delete();
    }
}


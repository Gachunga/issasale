<?php

namespace App\Repositories\Backend;

use App\Models\TodaysDeal;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use DB;
use Exception;
use Log;

/**
 * Class TodaysDealRepository
 * @package App\Repositories\Backend
 * @version September 12, 2020, 8:21 pm UTC
*/

class TodaysDealRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'product_image_id',
        'discount_type_id',
        'discount',
        'date',
        'background_color'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function __construct(TodaysDeal $model)
    {
        $this->model = $model;
    }

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function create(array $data) : TodaysDeal
    {
        Log::debug($data);
        // Make sure it doesn't already exist
        // if ($this->CompanyExists($data['product_id'])) {
        //     throw new Exception('A TodaysDeal already exists of the product ');
        // }
        $data['date'] = Carbon::parse($data['date']);
        
        return DB::transaction(function () use ($data) {
            $TodaysDeal = $this->model::create($data);

            if ($TodaysDeal) {
                return $TodaysDeal;
            }

            throw new Exception('An error occured attempting to create TodaysDeal');
        });
    }
    protected function CompanyExists($data) : bool
    {
        return $this->model
            ->where('product_id', strtolower($data['product_id']))
            // ->where('product_id', strtolower($data['product_id']))
            ->count() > 0;
    }
    public function find($id) : TodaysDeal
    {
        return $this->model->find($id);
    }
    public function delete($id)
    {
        $model = $this->model->find($id);
        return $model->delete();
    }
}

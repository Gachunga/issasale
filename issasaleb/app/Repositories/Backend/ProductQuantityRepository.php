<?php

namespace App\Repositories\Backend;

use App\Models\ProductColor;
use App\Models\ProductQuantity;
use App\Repositories\BaseRepository;
use DB;
use Log;

/**
 * Class ProductQuantityRepository
 * @package App\Repositories\Backend
 * @version September 2, 2020, 11:57 am UTC
*/

class ProductQuantityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'quantity'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function __construct(ProductQuantity $model)
    {
        $this->model = $model;
    }

    /**
     * Configure the Model
     **/
    public function create(array $data) : ProductQuantity
    {
        // Make sure it doesn't already exist
        return DB::transaction(function () use ($data) {
            $ProductQuantity = $this->model::create([
                'product_color_id' => $data['id'],
                'product_id' => $data['product_id'],
                'quantity' => $data['quantity']
            ]);
            $ProductQuantityColor = ProductColor::find($data['id']);
            Log::debug($ProductQuantityColor);
            $ProductQuantityColor->quantity = $ProductQuantityColor->quantity+$data['quantity'];
            $ProductQuantityColor->save();
            if ($ProductQuantity) {
                return $ProductQuantity;
            }

            throw new \Exception('An error occured attempting to create ProductQuantity');
        });
    }
    protected function ProductQuantityExists($code) : bool
    {
        return $this->model
            ->where('code', strtolower($code))
            ->count() > 0;
    }
    public function find($id) : ProductQuantity
    {
        return $this->model->find($id);
    }
    public function delete($id)
    {
        $model = $this->model->find($id);
        return $model->delete();
    }
}

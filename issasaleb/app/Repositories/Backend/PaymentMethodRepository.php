<?php

namespace App\Repositories\Backend;

use App\Models\PaymentMethod;
use App\Repositories\BaseRepository;
use DB;

/**
 * Class PaymentMethodRepository
 * @package App\Repositories\Backend
 * @version September 28, 2020, 2:55 am UTC
*/

class PaymentMethodRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'is_active'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function __construct(PaymentMethod $model)
    {
        $this->model = $model;
    }

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function create(array $data, $files) : PaymentMethod
    {
        // Make sure it doesn't already exist
        if ($this->CompanyExists($data['name'])) {
            throw new \Exception('A PaymentMethod already exists with the name '.e($data['name']));
        }
    
        return DB::transaction(function () use ($data) {
            $PaymentMethod = $this->model::create($data);
            
            if ($PaymentMethod) {
                return $PaymentMethod;
            }

            throw new \Exception('An error occured attempting to create PaymentMethod');
        });
    }
    protected function CompanyExists($name) : bool
    {
        return $this->model
            ->where('name', strtolower($name))
            ->count() > 0;
    }
    public function find($id) : PaymentMethod
    {
        return $this->model->find($id);
    }
    public function delete($id)
    {
        $model = $this->model->find($id);
        return $model->delete();
    }
}

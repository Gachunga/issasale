<?php

namespace App\Repositories\Backend;

use App\Models\Color;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductImage;
use App\Models\ProductPrice;
use App\Models\ProductQuantity;
use App\Models\ProductSize;
use App\Models\Size;
use App\Repositories\BaseRepository;
use DB;
use Log;
use Storage;

/**
 * Class ProductRepository
 * @package App\Repositories\Backend
 * @version August 31, 2020, 11:29 am UTC
*/

class ProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'code',
        'description',
        'taxable',
        'category_id',
        'brand_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function create(array $data, $files) : Product
    {
        // Make sure it doesn't already exist
        // if ($this->ProductExists($data['code'])) {
        //     throw new \Exception('A Product already exists with the code '.e($data['code']));
        // }
        $data['taxable'] = $data['taxable'] === 'true'? true: false;
        
        return DB::transaction(function () use ($data, $files) {
            $Product = $this->model::create($data);
            
            if ($Product) {
                $i=0;
                foreach($files as $file){
                    $path = Storage::putFile('products', $file);
                    $ProductImage = ProductImage::create([
                        "image_url" => $path,
                        "product_id" => $Product->id,
                        "main" => $i==0 ? true : false
                    ]);
                    $i++;
                }
                $variants = $data['variants'];
                Log::debug($variants);
                if(!empty($variants)&&$variants!=null){
                    foreach($variants as $variant){
                        $variantval = json_decode($variant);
                        $ProductColor = ProductColor::create([
                            "color_id" => $variantval->color_id,
                            "size_id" => $variantval->size_id,
                            "product_id" => $Product->id
                        ]);
                    }
                }
                
                // $ProductQuantity = new ProductQuantity();
                // $ProductQuantity->product_id = $Product->id;
                // $ProductQuantity->quantity = 0;
                // $ProductQuantity->save();

                $ProductPrice = new ProductPrice();
                $ProductPrice->product_id = $Product->id;
                $ProductPrice->price = isset($data['price']) ? doubleval($data['price']) : null;
                $ProductPrice->previous_price = isset($data['previous_price']) ? doubleval($data['previous_price']) : null;
                $ProductPrice->discount_type_id = isset($data['discount_type_id'])? $data['discount_type_id'] : null;
                $ProductPrice->discount = isset($data['discount']) ? doubleval($data['discount']) : null;
                $ProductPrice->active = true;
                $ProductPrice->save();
                
                return $Product;
            }

            throw new \Exception('An error occured attempting to create Product');
        });
    }
    public function update(array $data,$id, $files) : Product
    {
        // Make sure it doesn't already exist
        $data['taxable'] = $data['taxable'] === 'true'? true: false;
        
        return DB::transaction(function () use ($data, $files, $id) {
            $Product = $this->find($id)->save($data);
            
            if ($Product) {
                $i=0;
                if(isset($files)&&$files!=null){
                foreach($files as $file){
                    $path = Storage::putFile('products', $file);
                    $ProductImage = ProductImage::create([
                        "image_url" => $path,
                        "product_id" => $id,
                    ]);                    
                    
                }
                }
                if(isset($data['deletedImages'])&&$data['deletedImages']!=null){
                    foreach($data['deletedImages'] as $deletedImage){
                        $ProductImage = ProductImage::destroy($deletedImage);  
                    }
                }
                $colors = $data['colors'];
                if(!empty($colors)){
                    foreach($colors as $color){
                        $color = json_decode($color);
                        if(isset($color->id)){
                            $ProductColor =ProductColor::find($color->id);
                            
                            if($ProductColor!=null){
                                $ProductColor->name = $color->name;
                                $ProductColor->color =  $color->color;
                                $ProductColor->save();
                            }
                        }else{
                        $ProductColor = ProductColor::create([
                            "name" => $color->name,
                            "color" => $color->color,
                            "product_id" => $id
                        ]);
                      }
                    }
                }
                
                $ProductPrice = ProductPrice::where("product_id", $id)->first();
                $ProductPrice->product_id = $id;
                $ProductPrice->price = isset($data['price']) ? doubleval($data['price']) : null;
                $ProductPrice->previous_price = isset($data['previous_price']) ? doubleval($data['previous_price']) : null;
                $ProductPrice->discount_type_id = isset($data['discount_type_id'])? $data['discount_type_id'] : null;
                $ProductPrice->discount = isset($data['discount']) ? doubleval($data['discount']) : null;
                $ProductPrice->active = true;
                $ProductPrice->save();
                $Product = $this->find($id);
                
                return $Product;
            }

            throw new \Exception('An error occured attempting to create Product');
        });
    }
    protected function ProductExists($code) : bool
    {
        Log::debug($this->model
        ->where('code', strtolower($code))
        ->count());
        return $this->model
            ->where('code', strtolower($code))
            ->count() > 0;
    }
    public function find($id) : Product
    {
        return $this->model->find($id);
    }
    public function delete($id)
    {
        $model = $this->model->find($id);
        return $model->delete();
    }
}


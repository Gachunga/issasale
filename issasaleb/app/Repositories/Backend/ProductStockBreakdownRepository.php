<?php

namespace App\Repositories\Backend;

use App\Models\ProductStockBreakdown;
use App\Repositories\BaseRepository;

/**
 * Class ProductStockBreakdownRepository
 * @package App\Repositories\Backend
 * @version September 4, 2020, 12:47 am UTC
*/

class ProductStockBreakdownRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_quantity_id',
        'product_size_id',
        'product_color_id',
        'quantity'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductStockBreakdown::class;
    }
}

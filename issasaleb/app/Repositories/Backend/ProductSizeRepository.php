<?php

namespace App\Repositories\Backend;

use App\Models\ProductSize;
use App\Repositories\BaseRepository;

/**
 * Class ProductSizeRepository
 * @package App\Repositories\Backend
 * @version September 3, 2020, 11:31 pm UTC
*/

class ProductSizeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'size',
        'quantity'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductSize::class;
    }
}

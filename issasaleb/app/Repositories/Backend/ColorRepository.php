<?php

namespace App\Repositories\Backend;

use App\Models\Color;
use App\Repositories\BaseRepository;
use DB;
use Log;

/**
 * Class ColorRepository
 * @package App\Repositories\Backend
 * @version September 24, 2020, 2:59 pm UTC
*/

class ColorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'color',
        'name'
    ];

    public function __construct(Color $model)
    {
        $this->model = $model;
    }
    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function create(array $data) : Color
    {
        // Make sure it doesn't already exist
          
        
        return DB::transaction(function () use ($data) {
            $colors = $data['colors'];
            Log::debug($colors);
            if(!empty($colors)&&$colors!=null){
                foreach($colors as $colorobj){
                    $color = json_decode($colorobj);
                    if ($this->CompanyExists($color->name)) {
                        throw new \Exception('A Color already exists with the name '.e($data['name']));
                    }
                    $Color = $this->model::create([
                        "name" => $color->name,
                        "color" => $color->color,
                    ]);
                }
            }
            
            if ($Color) {
                return $Color;
            }

            throw new \Exception('An error occured attempting to create Color');
        });
    }
    protected function CompanyExists($name) : bool
    {
        return $this->model
            ->where('name', strtolower($name))
            ->count() > 0;
    }
    public function find($id) : Color
    {
        return $this->model->find($id);
    }
    public function delete($id)
    {
        $model = $this->model->find($id);
        return $model->delete();
    }
}

<?php

namespace App\Repositories\Backend;

use App\Models\InvoiceDetail;
use App\Repositories\BaseRepository;

/**
 * Class InvoiceDetailRepository
 * @package App\Repositories\Backend
 * @version September 28, 2020, 2:43 am UTC
*/

class InvoiceDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'invoice_id',
        'product_id',
        'size_id',
        'color_id',
        'quantity',
        'discount',
        'price',
        'discounted_price',
        'sub_total'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InvoiceDetail::class;
    }
}

<?php

namespace App\Repositories\Backend;

use App\Models\StockChange;
use App\Repositories\BaseRepository;

/**
 * Class StockChangeRepository
 * @package App\Repositories\Backend
 * @version September 2, 2020, 11:58 am UTC
*/

class StockChangeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'action',
        'initial',
        'quantity',
        'final',
        'user_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StockChange::class;
    }
}

<?php

namespace App\Repositories\Backend;

use App\Models\Category;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use Storage;

/**
 * Class CategoryRepository
 * @package App\Repositories\Backend
 * @version August 31, 2020, 6:45 am UTC
*/

class CategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'parent_id'
    ];

    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    public function create(array $data, $files) : Category
    {
        // Make sure it doesn't already exist
        if ($this->SaleStructureExists($data['title'])) {
            throw new \Exception('A Category already exists with the name '.e($data['title']));
        }
            if($files[0]!=null){
            $path = Storage::putFile('categories', $files[0]);
                $data['img_url'] = $path;
            }

        return DB::transaction(function () use ($data) {
            $Category = $this->model::create($data);

            if ($Category) {
                return $Category;
            }

            throw new \Exception('An error occured attempting to create Category');
        });
        }
        protected function SaleStructureExists($name) : bool
        {
            return $this->model
                ->where('title', strtolower($name))
                ->count() > 0;
        }
        public function update(array $data, $id) : Category
        {
            return DB::transaction(function () use ($data, $id) {
                $saleStructure = Category::whereId($id)->update($data);
                if ($saleStructure) {
                    $saleStructure = Category::whereId($id)->first();
                    return $saleStructure;
                }

                throw new \Exception('An error occured attempting to update Category');
            });
        }
        public function list(){
            $structure_id = 1;
            $root_categories = Category::where('id', '=' , $structure_id)->get()->toArray();
            $array_categories = $this->list_categories($root_categories);
            return $array_categories;
        }
        public function list_categories(Array $categories)
        {
        $data = [];

        foreach($categories as $category)
        {
            $childrenarr = Category::where('parent_id', '=' , $category['id'])->get()->toArray();
            $data[] = [
            'cat_id' => $category['id'],
            'parent_id' => $category['parent_id'],
            'name' => $category['title'],
            'label' => $category['title'],
            'data' => $category['title'],
            'caption' => $category['caption'],
            "expandedIcon"=> "pi pi-folder-open",
            "collapsedIcon"=> "pi pi-folder",
            'children' => $childrenarr!=[] ? $this->list_categories($childrenarr) : [],
            ];
        }

        return $data;
        }
        public function find($id) : Category
        {
            return $this->model->find($id);
        }
}


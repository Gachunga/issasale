<?php

namespace App\Repositories\Backend;

use App\Exceptions\GeneralException;
use App\Models\Client;
use App\Repositories\BaseRepository;
use DB;

/**
 * Class ClientRepository
 * @package App\Repositories\Backend
 * @version August 20, 2020, 8:32 am UTC
*/

class ClientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    public function __construct(Client $model)
    {
        $this->model = $model;
    }

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function create(array $data) : Client
    {
        // Make sure it doesn't already exist
        if ($this->CompanyExists($data['name'])) {
            throw new GeneralException('A Client already exists with the name '.e($data['name']));
        }

        return DB::transaction(function () use ($data) {
            $Client = $this->model::create($data);

            if ($Client) {
                return $Client;
            }

            throw new GeneralException('An error occured attempting to create Client');
        });
    }
    protected function CompanyExists($name) : bool
    {
        return $this->model
            ->where('name', strtolower($name))
            ->count() > 0;
    }
    public function find($id) : Client
    {
        return $this->model->find($id);
    }
    public function delete($id)
    {
        $model = $this->model->find($id);
        return $model->delete();
    }
}

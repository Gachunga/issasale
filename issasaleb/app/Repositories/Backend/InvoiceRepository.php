<?php

namespace App\Repositories\Backend;

use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\Product;
use App\Models\ProductColor;
use App\Repositories\BaseRepository;
use Auth;
use DB;
use Log;

/**
 * Class InvoiceRepository
 * @package App\Repositories\Backend
 * @version September 28, 2020, 2:43 am UTC
*/

class InvoiceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'name',
        'email',
        'location',
        'city',
        'phone',
        'status',
        'is_order',
        'is_paid',
        'payment_method_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function __construct(Invoice $model)
    {
        $this->model = $model;
    }

    /**
     * Configure the Model
     **/
    public function create(array $data) : Invoice
    {
        $user_id = Auth::guard('api')->user()->id;
        // Make sure it doesn't already exists
        return DB::transaction(function () use ($data, $user_id) {
            $today = date("Ymd");
            $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
            $unique = $today . $rand;
            $data['order_id'] = $unique;
            $data['is_order'] = 1;
            $data['is_paid'] = 0;
            $data['status_id'] = 1;
            $data['user_id'] = $user_id;

            $Invoice = $this->model::create($data);
            
            if ($Invoice) {
                $sub_total = 0;
                $discount = 0;
                $shipping = 0;
                $tax = 0;
                $total = 0;

                $cartItems = json_decode($data['cartItems']);
                foreach($cartItems as $cartItem){
                    $Product = Product::where('id', $cartItem->id)->with('product_prices')->first();
                    $ProductColorId = ProductColor::where('size_id', $cartItem->size_id)->where('color_id', $cartItem->color_id)->where('product_id', $cartItem->id)->first();

                    if(!empty($Product)){
                        $price = $Product->product_prices[0]->price;
                        $discountprc = $Product->product_prices[0]->discount;
                        $discountedPrice = ($price*(100-$discountprc))/100;
                        $discount = ($price*($discountprc))/100;
                        $subtotal = $discountedPrice* $cartItem->quantity;
                        $sub_total = $sub_total+$subtotal;
                        $discount = $sub_total+$discount;

                        InvoiceDetail::create([
                            'invoice_id' => $Invoice->id,
                            'product_id' => $cartItem->id,
                            'product_color_id' => $ProductColorId->id,
                            'quantity' => $cartItem->quantity,
                            'discount' => $discountprc,
                            'price' => $price,
                            'discounted_price' => $discountedPrice,
                            'sub_total' => $subtotal,
                        ]);
                        $Invoice->total = $total;
                        $Invoice->sub_total = $sub_total;
                        $Invoice->tax = $tax;
                        $Invoice->discount = $discount;
                        $Invoice->shipping = $shipping;
                        $Invoice->save();
                    }
                }
                return $Invoice;
            }

            throw new \Exception('An error occured attempting to create Invoice');
        });
    }
    protected function CompanyExists($name) : bool
    {
        return $this->model
            ->where('name', strtolower($name))
            ->count() > 0;
    }
    public function find($id) : Invoice
    {
        return $this->model->find($id);
    }
    public function delete($id)
    {
        $model = $this->model->find($id);
        return $model->delete();
    }
}
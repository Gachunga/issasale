<?php

namespace App\Repositories\Backend;

use App\Exceptions\GeneralException;
use App\Models\Brand;
use App\Repositories\BaseRepository;
use DB;
use Log;
use Storage;

/**
 * Class BrandRepository
 * @package App\Repositories\Backend
 * @version August 30, 2020, 1:18 pm UTC
*/

class BrandRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'brand_logo_url',
        'ranking'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function __construct(Brand $model)
    {
        $this->model = $model;
    }

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function create(array $data, $files) : Brand
    {
        // Make sure it doesn't already exist
        if ($this->CompanyExists($data['name'])) {
            throw new \Exception('A Brand already exists with the name '.e($data['name']));
        }
        if($files[0]!=null){
        $path = Storage::putFile('logos', $files[0]);
        
        }else{
            throw new \Exception('Image is required');
        }   
        
        return DB::transaction(function () use ($data, $path) {
            $data['brand_logo_url'] = $path;
            $Brand = $this->model::create($data);
            
            if ($Brand) {
                return $Brand;
            }

            throw new \Exception('An error occured attempting to create Brand');
        });
    }
    protected function CompanyExists($name) : bool
    {
        return $this->model
            ->where('name', strtolower($name))
            ->count() > 0;
    }
    public function find($id) : Brand
    {
        return $this->model->find($id);
    }
    public function delete($id)
    {
        $model = $this->model->find($id);
        return $model->delete();
    }
}

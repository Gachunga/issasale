<?php

namespace App\Repositories\Backend;

use App\Models\SliderImage;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use DB;
use Exception;
use Log;
use Storage;

/**
 * Class SliderImageRepository
 * @package App\Repositories\Backend
 * @version September 10, 2020, 1:07 pm UTC
*/

class SliderImageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title_caption',
        'title',
        'title_desc',
        'img_url'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function __construct(SliderImage $model)
    {
        $this->model = $model;
    }

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function create(array $data, $files) : SliderImage
    {
        // Make sure it doesn't already exist
        if($files[0]!=null){
        $path = Storage::putFile('sliders', $files[0]);
        
        }else{
            throw new \Exception('Image is required');
        }
        if(isset($data['date_from'])){
            $data['date_from'] = Carbon::parse($data['date_from']);
        }
        if(isset($data['date_to'])){
            $data['date_to'] = Carbon::parse($data['date_to']);
        }   
        
        return DB::transaction(function () use ($data, $path) {
            $data['img_url'] = $path;
            $SliderImage = $this->model::create($data);
            
            if ($SliderImage) {
                return $SliderImage;
            }

            throw new \Exception('An error occured attempting to create SliderImage');
        });
    }
    public function update(array $data, $id, $files) : SliderImage
    {
        Log::debug($data);
        // Make sure it doesn't already exist
        $path = null;
        if($files[0]!=null){
        $path = Storage::putFile('sliders', $files[0]);
        }
        if(isset($data['date_from'])){
            $data['date_from'] = Carbon::parse($data['date_from']);
        }
        if(isset($data['date_to'])){
            $data['date_to'] = Carbon::parse($data['date_to']);
        }   
        return DB::transaction(function () use ($data, $path, $id) {
            if(isset($path)){
                $data['img_url'] = $path;
            }
            
            $SliderImage = $this->model::find($id);
            $SliderImage->level = $data['level'];
            $SliderImage->update($data);
            
            if ($SliderImage) {
                return $SliderImage;
            }

            throw new \Exception('An error occured attempting to create SliderImage');
        });
    }
    protected function ImageExists($name) : bool
    {
        return $this->model
            ->where('title', strtolower($name))
            ->count() > 0;
    }
    public function find($id) : SliderImage
    {
        return $this->model->find($id);
    }
    public function delete($id)
    {
        $model = $this->model->find($id);
        return $model->delete();
    }
}
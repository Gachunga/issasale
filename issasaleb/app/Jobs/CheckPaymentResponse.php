<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use App\Repositories\Backend\SeatReservationRepository;
use Carbon\Carbon;

use App\Models\SeatReservation;
use App\Models\Auth\User;

class CheckPaymentResponse implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $seat_reservation;
    protected $ride;
    /** @var  RideRepository */
    private $rideRepository;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(SeatReservation $seat_reservation)
    {
        $this->seat_reservation = $seat_reservation;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SeatReservationRepository $seat_reservationRepository)
    {
        Log::debug('SeatReservationRepository handle START');
        Log::debug('SeatReservationRepository handle econf');
        Log::debug($this->seat_reservation);
        $reservation = SeatReservation::where('id', $this->seat_reservation->id)->first();
        $paid = $reservation->paid;
        if(!$paid){
            $reservation->delete();
        }
        Log::debug('SeatReservationRepository handle STOP');
    }
}

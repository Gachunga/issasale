<?php namespace Tests\Repositories;

use App\Models\TodaysDeal;
use App\Repositories\Backend\TodaysDealRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TodaysDealRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TodaysDealRepository
     */
    protected $todaysDealRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->todaysDealRepo = \App::make(TodaysDealRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_todays_deal()
    {
        $todaysDeal = factory(TodaysDeal::class)->make()->toArray();

        $createdTodaysDeal = $this->todaysDealRepo->create($todaysDeal);

        $createdTodaysDeal = $createdTodaysDeal->toArray();
        $this->assertArrayHasKey('id', $createdTodaysDeal);
        $this->assertNotNull($createdTodaysDeal['id'], 'Created TodaysDeal must have id specified');
        $this->assertNotNull(TodaysDeal::find($createdTodaysDeal['id']), 'TodaysDeal with given id must be in DB');
        $this->assertModelData($todaysDeal, $createdTodaysDeal);
    }

    /**
     * @test read
     */
    public function test_read_todays_deal()
    {
        $todaysDeal = factory(TodaysDeal::class)->create();

        $dbTodaysDeal = $this->todaysDealRepo->find($todaysDeal->id);

        $dbTodaysDeal = $dbTodaysDeal->toArray();
        $this->assertModelData($todaysDeal->toArray(), $dbTodaysDeal);
    }

    /**
     * @test update
     */
    public function test_update_todays_deal()
    {
        $todaysDeal = factory(TodaysDeal::class)->create();
        $fakeTodaysDeal = factory(TodaysDeal::class)->make()->toArray();

        $updatedTodaysDeal = $this->todaysDealRepo->update($fakeTodaysDeal, $todaysDeal->id);

        $this->assertModelData($fakeTodaysDeal, $updatedTodaysDeal->toArray());
        $dbTodaysDeal = $this->todaysDealRepo->find($todaysDeal->id);
        $this->assertModelData($fakeTodaysDeal, $dbTodaysDeal->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_todays_deal()
    {
        $todaysDeal = factory(TodaysDeal::class)->create();

        $resp = $this->todaysDealRepo->delete($todaysDeal->id);

        $this->assertTrue($resp);
        $this->assertNull(TodaysDeal::find($todaysDeal->id), 'TodaysDeal should not exist in DB');
    }
}

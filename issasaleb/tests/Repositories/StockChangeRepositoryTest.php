<?php namespace Tests\Repositories;

use App\Models\StockChange;
use App\Repositories\Backend\StockChangeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class StockChangeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var StockChangeRepository
     */
    protected $stockChangeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->stockChangeRepo = \App::make(StockChangeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_stock_change()
    {
        $stockChange = factory(StockChange::class)->make()->toArray();

        $createdStockChange = $this->stockChangeRepo->create($stockChange);

        $createdStockChange = $createdStockChange->toArray();
        $this->assertArrayHasKey('id', $createdStockChange);
        $this->assertNotNull($createdStockChange['id'], 'Created StockChange must have id specified');
        $this->assertNotNull(StockChange::find($createdStockChange['id']), 'StockChange with given id must be in DB');
        $this->assertModelData($stockChange, $createdStockChange);
    }

    /**
     * @test read
     */
    public function test_read_stock_change()
    {
        $stockChange = factory(StockChange::class)->create();

        $dbStockChange = $this->stockChangeRepo->find($stockChange->id);

        $dbStockChange = $dbStockChange->toArray();
        $this->assertModelData($stockChange->toArray(), $dbStockChange);
    }

    /**
     * @test update
     */
    public function test_update_stock_change()
    {
        $stockChange = factory(StockChange::class)->create();
        $fakeStockChange = factory(StockChange::class)->make()->toArray();

        $updatedStockChange = $this->stockChangeRepo->update($fakeStockChange, $stockChange->id);

        $this->assertModelData($fakeStockChange, $updatedStockChange->toArray());
        $dbStockChange = $this->stockChangeRepo->find($stockChange->id);
        $this->assertModelData($fakeStockChange, $dbStockChange->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_stock_change()
    {
        $stockChange = factory(StockChange::class)->create();

        $resp = $this->stockChangeRepo->delete($stockChange->id);

        $this->assertTrue($resp);
        $this->assertNull(StockChange::find($stockChange->id), 'StockChange should not exist in DB');
    }
}

<?php namespace Tests\Repositories;

use App\Models\DiscountType;
use App\Repositories\Backend\DiscountTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DiscountTypeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DiscountTypeRepository
     */
    protected $discountTypeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->discountTypeRepo = \App::make(DiscountTypeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_discount_type()
    {
        $discountType = factory(DiscountType::class)->make()->toArray();

        $createdDiscountType = $this->discountTypeRepo->create($discountType);

        $createdDiscountType = $createdDiscountType->toArray();
        $this->assertArrayHasKey('id', $createdDiscountType);
        $this->assertNotNull($createdDiscountType['id'], 'Created DiscountType must have id specified');
        $this->assertNotNull(DiscountType::find($createdDiscountType['id']), 'DiscountType with given id must be in DB');
        $this->assertModelData($discountType, $createdDiscountType);
    }

    /**
     * @test read
     */
    public function test_read_discount_type()
    {
        $discountType = factory(DiscountType::class)->create();

        $dbDiscountType = $this->discountTypeRepo->find($discountType->id);

        $dbDiscountType = $dbDiscountType->toArray();
        $this->assertModelData($discountType->toArray(), $dbDiscountType);
    }

    /**
     * @test update
     */
    public function test_update_discount_type()
    {
        $discountType = factory(DiscountType::class)->create();
        $fakeDiscountType = factory(DiscountType::class)->make()->toArray();

        $updatedDiscountType = $this->discountTypeRepo->update($fakeDiscountType, $discountType->id);

        $this->assertModelData($fakeDiscountType, $updatedDiscountType->toArray());
        $dbDiscountType = $this->discountTypeRepo->find($discountType->id);
        $this->assertModelData($fakeDiscountType, $dbDiscountType->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_discount_type()
    {
        $discountType = factory(DiscountType::class)->create();

        $resp = $this->discountTypeRepo->delete($discountType->id);

        $this->assertTrue($resp);
        $this->assertNull(DiscountType::find($discountType->id), 'DiscountType should not exist in DB');
    }
}

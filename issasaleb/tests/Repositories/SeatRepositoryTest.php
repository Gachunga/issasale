<?php namespace Tests\Repositories;

use App\Models\Seat;
use App\Repositories\Backend\SeatRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SeatRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SeatRepository
     */
    protected $seatRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->seatRepo = \App::make(SeatRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_seat()
    {
        $seat = factory(Seat::class)->make()->toArray();

        $createdSeat = $this->seatRepo->create($seat);

        $createdSeat = $createdSeat->toArray();
        $this->assertArrayHasKey('id', $createdSeat);
        $this->assertNotNull($createdSeat['id'], 'Created Seat must have id specified');
        $this->assertNotNull(Seat::find($createdSeat['id']), 'Seat with given id must be in DB');
        $this->assertModelData($seat, $createdSeat);
    }

    /**
     * @test read
     */
    public function test_read_seat()
    {
        $seat = factory(Seat::class)->create();

        $dbSeat = $this->seatRepo->find($seat->id);

        $dbSeat = $dbSeat->toArray();
        $this->assertModelData($seat->toArray(), $dbSeat);
    }

    /**
     * @test update
     */
    public function test_update_seat()
    {
        $seat = factory(Seat::class)->create();
        $fakeSeat = factory(Seat::class)->make()->toArray();

        $updatedSeat = $this->seatRepo->update($fakeSeat, $seat->id);

        $this->assertModelData($fakeSeat, $updatedSeat->toArray());
        $dbSeat = $this->seatRepo->find($seat->id);
        $this->assertModelData($fakeSeat, $dbSeat->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_seat()
    {
        $seat = factory(Seat::class)->create();

        $resp = $this->seatRepo->delete($seat->id);

        $this->assertTrue($resp);
        $this->assertNull(Seat::find($seat->id), 'Seat should not exist in DB');
    }
}

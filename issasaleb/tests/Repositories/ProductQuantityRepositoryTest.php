<?php namespace Tests\Repositories;

use App\Models\ProductQuantity;
use App\Repositories\Backend\ProductQuantityRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProductQuantityRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProductQuantityRepository
     */
    protected $productQuantityRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->productQuantityRepo = \App::make(ProductQuantityRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_product_quantity()
    {
        $productQuantity = factory(ProductQuantity::class)->make()->toArray();

        $createdProductQuantity = $this->productQuantityRepo->create($productQuantity);

        $createdProductQuantity = $createdProductQuantity->toArray();
        $this->assertArrayHasKey('id', $createdProductQuantity);
        $this->assertNotNull($createdProductQuantity['id'], 'Created ProductQuantity must have id specified');
        $this->assertNotNull(ProductQuantity::find($createdProductQuantity['id']), 'ProductQuantity with given id must be in DB');
        $this->assertModelData($productQuantity, $createdProductQuantity);
    }

    /**
     * @test read
     */
    public function test_read_product_quantity()
    {
        $productQuantity = factory(ProductQuantity::class)->create();

        $dbProductQuantity = $this->productQuantityRepo->find($productQuantity->id);

        $dbProductQuantity = $dbProductQuantity->toArray();
        $this->assertModelData($productQuantity->toArray(), $dbProductQuantity);
    }

    /**
     * @test update
     */
    public function test_update_product_quantity()
    {
        $productQuantity = factory(ProductQuantity::class)->create();
        $fakeProductQuantity = factory(ProductQuantity::class)->make()->toArray();

        $updatedProductQuantity = $this->productQuantityRepo->update($fakeProductQuantity, $productQuantity->id);

        $this->assertModelData($fakeProductQuantity, $updatedProductQuantity->toArray());
        $dbProductQuantity = $this->productQuantityRepo->find($productQuantity->id);
        $this->assertModelData($fakeProductQuantity, $dbProductQuantity->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_product_quantity()
    {
        $productQuantity = factory(ProductQuantity::class)->create();

        $resp = $this->productQuantityRepo->delete($productQuantity->id);

        $this->assertTrue($resp);
        $this->assertNull(ProductQuantity::find($productQuantity->id), 'ProductQuantity should not exist in DB');
    }
}

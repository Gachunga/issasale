<?php namespace Tests\Repositories;

use App\Models\CompanyBuses;
use App\Repositories\Backend\CompanyBusesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CompanyBusesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CompanyBusesRepository
     */
    protected $companyBusesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->companyBusesRepo = \App::make(CompanyBusesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_company_buses()
    {
        $companyBuses = factory(CompanyBuses::class)->make()->toArray();

        $createdCompanyBuses = $this->companyBusesRepo->create($companyBuses);

        $createdCompanyBuses = $createdCompanyBuses->toArray();
        $this->assertArrayHasKey('id', $createdCompanyBuses);
        $this->assertNotNull($createdCompanyBuses['id'], 'Created CompanyBuses must have id specified');
        $this->assertNotNull(CompanyBuses::find($createdCompanyBuses['id']), 'CompanyBuses with given id must be in DB');
        $this->assertModelData($companyBuses, $createdCompanyBuses);
    }

    /**
     * @test read
     */
    public function test_read_company_buses()
    {
        $companyBuses = factory(CompanyBuses::class)->create();

        $dbCompanyBuses = $this->companyBusesRepo->find($companyBuses->id);

        $dbCompanyBuses = $dbCompanyBuses->toArray();
        $this->assertModelData($companyBuses->toArray(), $dbCompanyBuses);
    }

    /**
     * @test update
     */
    public function test_update_company_buses()
    {
        $companyBuses = factory(CompanyBuses::class)->create();
        $fakeCompanyBuses = factory(CompanyBuses::class)->make()->toArray();

        $updatedCompanyBuses = $this->companyBusesRepo->update($fakeCompanyBuses, $companyBuses->id);

        $this->assertModelData($fakeCompanyBuses, $updatedCompanyBuses->toArray());
        $dbCompanyBuses = $this->companyBusesRepo->find($companyBuses->id);
        $this->assertModelData($fakeCompanyBuses, $dbCompanyBuses->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_company_buses()
    {
        $companyBuses = factory(CompanyBuses::class)->create();

        $resp = $this->companyBusesRepo->delete($companyBuses->id);

        $this->assertTrue($resp);
        $this->assertNull(CompanyBuses::find($companyBuses->id), 'CompanyBuses should not exist in DB');
    }
}

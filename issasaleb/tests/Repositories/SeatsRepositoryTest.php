<?php namespace Tests\Repositories;

use App\Models\Seats;
use App\Repositories\Backend\SeatRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SeatsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SeatsRepository
     */
    protected $seatsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->seatsRepo = \App::make(SeatRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_seats()
    {
        $seats = factory(Seats::class)->make()->toArray();

        $createdSeats = $this->seatsRepo->create($seats);

        $createdSeats = $createdSeats->toArray();
        $this->assertArrayHasKey('id', $createdSeats);
        $this->assertNotNull($createdSeats['id'], 'Created Seats must have id specified');
        $this->assertNotNull(Seats::find($createdSeats['id']), 'Seats with given id must be in DB');
        $this->assertModelData($seats, $createdSeats);
    }

    /**
     * @test read
     */
    public function test_read_seats()
    {
        $seats = factory(Seats::class)->create();

        $dbSeats = $this->seatsRepo->find($seats->id);

        $dbSeats = $dbSeats->toArray();
        $this->assertModelData($seats->toArray(), $dbSeats);
    }

    /**
     * @test update
     */
    public function test_update_seats()
    {
        $seats = factory(Seats::class)->create();
        $fakeSeats = factory(Seats::class)->make()->toArray();

        $updatedSeats = $this->seatsRepo->update($fakeSeats, $seats->id);

        $this->assertModelData($fakeSeats, $updatedSeats->toArray());
        $dbSeats = $this->seatsRepo->find($seats->id);
        $this->assertModelData($fakeSeats, $dbSeats->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_seats()
    {
        $seats = factory(Seats::class)->create();

        $resp = $this->seatsRepo->delete($seats->id);

        $this->assertTrue($resp);
        $this->assertNull(Seats::find($seats->id), 'Seats should not exist in DB');
    }
}

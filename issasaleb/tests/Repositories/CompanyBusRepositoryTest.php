<?php namespace Tests\Repositories;

use App\Models\CompanyBus;
use App\Repositories\Backend\CompanyBusRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CompanyBusRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CompanyBusRepository
     */
    protected $companyBusRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->companyBusRepo = \App::make(CompanyBusRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_company_bus()
    {
        $companyBus = factory(CompanyBus::class)->make()->toArray();

        $createdCompanyBus = $this->companyBusRepo->create($companyBus);

        $createdCompanyBus = $createdCompanyBus->toArray();
        $this->assertArrayHasKey('id', $createdCompanyBus);
        $this->assertNotNull($createdCompanyBus['id'], 'Created CompanyBus must have id specified');
        $this->assertNotNull(CompanyBus::find($createdCompanyBus['id']), 'CompanyBus with given id must be in DB');
        $this->assertModelData($companyBus, $createdCompanyBus);
    }

    /**
     * @test read
     */
    public function test_read_company_bus()
    {
        $companyBus = factory(CompanyBus::class)->create();

        $dbCompanyBus = $this->companyBusRepo->find($companyBus->id);

        $dbCompanyBus = $dbCompanyBus->toArray();
        $this->assertModelData($companyBus->toArray(), $dbCompanyBus);
    }

    /**
     * @test update
     */
    public function test_update_company_bus()
    {
        $companyBus = factory(CompanyBus::class)->create();
        $fakeCompanyBus = factory(CompanyBus::class)->make()->toArray();

        $updatedCompanyBus = $this->companyBusRepo->update($fakeCompanyBus, $companyBus->id);

        $this->assertModelData($fakeCompanyBus, $updatedCompanyBus->toArray());
        $dbCompanyBus = $this->companyBusRepo->find($companyBus->id);
        $this->assertModelData($fakeCompanyBus, $dbCompanyBus->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_company_bus()
    {
        $companyBus = factory(CompanyBus::class)->create();

        $resp = $this->companyBusRepo->delete($companyBus->id);

        $this->assertTrue($resp);
        $this->assertNull(CompanyBus::find($companyBus->id), 'CompanyBus should not exist in DB');
    }
}

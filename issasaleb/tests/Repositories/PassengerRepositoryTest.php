<?php namespace Tests\Repositories;

use App\Models\Passenger;
use App\Repositories\Backend\PassengerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PassengerRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PassengerRepository
     */
    protected $passengerRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->passengerRepo = \App::make(PassengerRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_passenger()
    {
        $passenger = factory(Passenger::class)->make()->toArray();

        $createdPassenger = $this->passengerRepo->create($passenger);

        $createdPassenger = $createdPassenger->toArray();
        $this->assertArrayHasKey('id', $createdPassenger);
        $this->assertNotNull($createdPassenger['id'], 'Created Passenger must have id specified');
        $this->assertNotNull(Passenger::find($createdPassenger['id']), 'Passenger with given id must be in DB');
        $this->assertModelData($passenger, $createdPassenger);
    }

    /**
     * @test read
     */
    public function test_read_passenger()
    {
        $passenger = factory(Passenger::class)->create();

        $dbPassenger = $this->passengerRepo->find($passenger->id);

        $dbPassenger = $dbPassenger->toArray();
        $this->assertModelData($passenger->toArray(), $dbPassenger);
    }

    /**
     * @test update
     */
    public function test_update_passenger()
    {
        $passenger = factory(Passenger::class)->create();
        $fakePassenger = factory(Passenger::class)->make()->toArray();

        $updatedPassenger = $this->passengerRepo->update($fakePassenger, $passenger->id);

        $this->assertModelData($fakePassenger, $updatedPassenger->toArray());
        $dbPassenger = $this->passengerRepo->find($passenger->id);
        $this->assertModelData($fakePassenger, $dbPassenger->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_passenger()
    {
        $passenger = factory(Passenger::class)->create();

        $resp = $this->passengerRepo->delete($passenger->id);

        $this->assertTrue($resp);
        $this->assertNull(Passenger::find($passenger->id), 'Passenger should not exist in DB');
    }
}

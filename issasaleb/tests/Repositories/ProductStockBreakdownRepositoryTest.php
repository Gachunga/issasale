<?php namespace Tests\Repositories;

use App\Models\ProductStockBreakdown;
use App\Repositories\Backend\ProductStockBreakdownRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProductStockBreakdownRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProductStockBreakdownRepository
     */
    protected $productStockBreakdownRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->productStockBreakdownRepo = \App::make(ProductStockBreakdownRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_product_stock_breakdown()
    {
        $productStockBreakdown = factory(ProductStockBreakdown::class)->make()->toArray();

        $createdProductStockBreakdown = $this->productStockBreakdownRepo->create($productStockBreakdown);

        $createdProductStockBreakdown = $createdProductStockBreakdown->toArray();
        $this->assertArrayHasKey('id', $createdProductStockBreakdown);
        $this->assertNotNull($createdProductStockBreakdown['id'], 'Created ProductStockBreakdown must have id specified');
        $this->assertNotNull(ProductStockBreakdown::find($createdProductStockBreakdown['id']), 'ProductStockBreakdown with given id must be in DB');
        $this->assertModelData($productStockBreakdown, $createdProductStockBreakdown);
    }

    /**
     * @test read
     */
    public function test_read_product_stock_breakdown()
    {
        $productStockBreakdown = factory(ProductStockBreakdown::class)->create();

        $dbProductStockBreakdown = $this->productStockBreakdownRepo->find($productStockBreakdown->id);

        $dbProductStockBreakdown = $dbProductStockBreakdown->toArray();
        $this->assertModelData($productStockBreakdown->toArray(), $dbProductStockBreakdown);
    }

    /**
     * @test update
     */
    public function test_update_product_stock_breakdown()
    {
        $productStockBreakdown = factory(ProductStockBreakdown::class)->create();
        $fakeProductStockBreakdown = factory(ProductStockBreakdown::class)->make()->toArray();

        $updatedProductStockBreakdown = $this->productStockBreakdownRepo->update($fakeProductStockBreakdown, $productStockBreakdown->id);

        $this->assertModelData($fakeProductStockBreakdown, $updatedProductStockBreakdown->toArray());
        $dbProductStockBreakdown = $this->productStockBreakdownRepo->find($productStockBreakdown->id);
        $this->assertModelData($fakeProductStockBreakdown, $dbProductStockBreakdown->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_product_stock_breakdown()
    {
        $productStockBreakdown = factory(ProductStockBreakdown::class)->create();

        $resp = $this->productStockBreakdownRepo->delete($productStockBreakdown->id);

        $this->assertTrue($resp);
        $this->assertNull(ProductStockBreakdown::find($productStockBreakdown->id), 'ProductStockBreakdown should not exist in DB');
    }
}

<?php namespace Tests\Repositories;

use App\Models\ProductPrice;
use App\Repositories\Backend\ProductPriceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProductPriceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProductPriceRepository
     */
    protected $productPriceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->productPriceRepo = \App::make(ProductPriceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_product_price()
    {
        $productPrice = factory(ProductPrice::class)->make()->toArray();

        $createdProductPrice = $this->productPriceRepo->create($productPrice);

        $createdProductPrice = $createdProductPrice->toArray();
        $this->assertArrayHasKey('id', $createdProductPrice);
        $this->assertNotNull($createdProductPrice['id'], 'Created ProductPrice must have id specified');
        $this->assertNotNull(ProductPrice::find($createdProductPrice['id']), 'ProductPrice with given id must be in DB');
        $this->assertModelData($productPrice, $createdProductPrice);
    }

    /**
     * @test read
     */
    public function test_read_product_price()
    {
        $productPrice = factory(ProductPrice::class)->create();

        $dbProductPrice = $this->productPriceRepo->find($productPrice->id);

        $dbProductPrice = $dbProductPrice->toArray();
        $this->assertModelData($productPrice->toArray(), $dbProductPrice);
    }

    /**
     * @test update
     */
    public function test_update_product_price()
    {
        $productPrice = factory(ProductPrice::class)->create();
        $fakeProductPrice = factory(ProductPrice::class)->make()->toArray();

        $updatedProductPrice = $this->productPriceRepo->update($fakeProductPrice, $productPrice->id);

        $this->assertModelData($fakeProductPrice, $updatedProductPrice->toArray());
        $dbProductPrice = $this->productPriceRepo->find($productPrice->id);
        $this->assertModelData($fakeProductPrice, $dbProductPrice->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_product_price()
    {
        $productPrice = factory(ProductPrice::class)->create();

        $resp = $this->productPriceRepo->delete($productPrice->id);

        $this->assertTrue($resp);
        $this->assertNull(ProductPrice::find($productPrice->id), 'ProductPrice should not exist in DB');
    }
}

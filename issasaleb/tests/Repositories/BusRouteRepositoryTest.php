<?php namespace Tests\Repositories;

use App\Models\BusRoute;
use App\Repositories\Backend\BusRouteRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class BusRouteRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var BusRouteRepository
     */
    protected $busRouteRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->busRouteRepo = \App::make(BusRouteRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_bus_route()
    {
        $busRoute = factory(BusRoute::class)->make()->toArray();

        $createdBusRoute = $this->busRouteRepo->create($busRoute);

        $createdBusRoute = $createdBusRoute->toArray();
        $this->assertArrayHasKey('id', $createdBusRoute);
        $this->assertNotNull($createdBusRoute['id'], 'Created BusRoute must have id specified');
        $this->assertNotNull(BusRoute::find($createdBusRoute['id']), 'BusRoute with given id must be in DB');
        $this->assertModelData($busRoute, $createdBusRoute);
    }

    /**
     * @test read
     */
    public function test_read_bus_route()
    {
        $busRoute = factory(BusRoute::class)->create();

        $dbBusRoute = $this->busRouteRepo->find($busRoute->id);

        $dbBusRoute = $dbBusRoute->toArray();
        $this->assertModelData($busRoute->toArray(), $dbBusRoute);
    }

    /**
     * @test update
     */
    public function test_update_bus_route()
    {
        $busRoute = factory(BusRoute::class)->create();
        $fakeBusRoute = factory(BusRoute::class)->make()->toArray();

        $updatedBusRoute = $this->busRouteRepo->update($fakeBusRoute, $busRoute->id);

        $this->assertModelData($fakeBusRoute, $updatedBusRoute->toArray());
        $dbBusRoute = $this->busRouteRepo->find($busRoute->id);
        $this->assertModelData($fakeBusRoute, $dbBusRoute->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_bus_route()
    {
        $busRoute = factory(BusRoute::class)->create();

        $resp = $this->busRouteRepo->delete($busRoute->id);

        $this->assertTrue($resp);
        $this->assertNull(BusRoute::find($busRoute->id), 'BusRoute should not exist in DB');
    }
}

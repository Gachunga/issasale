<?php namespace Tests\Repositories;

use App\Models\SeatReservation;
use App\Repositories\Backend\SeatReservationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SeatReservationRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SeatReservationRepository
     */
    protected $seatReservationRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->seatReservationRepo = \App::make(SeatReservationRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_seat_reservation()
    {
        $seatReservation = factory(SeatReservation::class)->make()->toArray();

        $createdSeatReservation = $this->seatReservationRepo->create($seatReservation);

        $createdSeatReservation = $createdSeatReservation->toArray();
        $this->assertArrayHasKey('id', $createdSeatReservation);
        $this->assertNotNull($createdSeatReservation['id'], 'Created SeatReservation must have id specified');
        $this->assertNotNull(SeatReservation::find($createdSeatReservation['id']), 'SeatReservation with given id must be in DB');
        $this->assertModelData($seatReservation, $createdSeatReservation);
    }

    /**
     * @test read
     */
    public function test_read_seat_reservation()
    {
        $seatReservation = factory(SeatReservation::class)->create();

        $dbSeatReservation = $this->seatReservationRepo->find($seatReservation->id);

        $dbSeatReservation = $dbSeatReservation->toArray();
        $this->assertModelData($seatReservation->toArray(), $dbSeatReservation);
    }

    /**
     * @test update
     */
    public function test_update_seat_reservation()
    {
        $seatReservation = factory(SeatReservation::class)->create();
        $fakeSeatReservation = factory(SeatReservation::class)->make()->toArray();

        $updatedSeatReservation = $this->seatReservationRepo->update($fakeSeatReservation, $seatReservation->id);

        $this->assertModelData($fakeSeatReservation, $updatedSeatReservation->toArray());
        $dbSeatReservation = $this->seatReservationRepo->find($seatReservation->id);
        $this->assertModelData($fakeSeatReservation, $dbSeatReservation->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_seat_reservation()
    {
        $seatReservation = factory(SeatReservation::class)->create();

        $resp = $this->seatReservationRepo->delete($seatReservation->id);

        $this->assertTrue($resp);
        $this->assertNull(SeatReservation::find($seatReservation->id), 'SeatReservation should not exist in DB');
    }
}

<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Seat;

class SeatApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_seat()
    {
        $seat = factory(Seat::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/seats', $seat
        );

        $this->assertApiResponse($seat);
    }

    /**
     * @test
     */
    public function test_read_seat()
    {
        $seat = factory(Seat::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/seats/'.$seat->id
        );

        $this->assertApiResponse($seat->toArray());
    }

    /**
     * @test
     */
    public function test_update_seat()
    {
        $seat = factory(Seat::class)->create();
        $editedSeat = factory(Seat::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/seats/'.$seat->id,
            $editedSeat
        );

        $this->assertApiResponse($editedSeat);
    }

    /**
     * @test
     */
    public function test_delete_seat()
    {
        $seat = factory(Seat::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/seats/'.$seat->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/seats/'.$seat->id
        );

        $this->response->assertStatus(404);
    }
}

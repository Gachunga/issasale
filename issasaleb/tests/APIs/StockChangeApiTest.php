<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\StockChange;

class StockChangeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_stock_change()
    {
        $stockChange = factory(StockChange::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/stock_changes', $stockChange
        );

        $this->assertApiResponse($stockChange);
    }

    /**
     * @test
     */
    public function test_read_stock_change()
    {
        $stockChange = factory(StockChange::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/stock_changes/'.$stockChange->id
        );

        $this->assertApiResponse($stockChange->toArray());
    }

    /**
     * @test
     */
    public function test_update_stock_change()
    {
        $stockChange = factory(StockChange::class)->create();
        $editedStockChange = factory(StockChange::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/stock_changes/'.$stockChange->id,
            $editedStockChange
        );

        $this->assertApiResponse($editedStockChange);
    }

    /**
     * @test
     */
    public function test_delete_stock_change()
    {
        $stockChange = factory(StockChange::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/stock_changes/'.$stockChange->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/stock_changes/'.$stockChange->id
        );

        $this->response->assertStatus(404);
    }
}

<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CompanyBuses;

class CompanyBusesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_company_buses()
    {
        $companyBuses = factory(CompanyBuses::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/company_buses', $companyBuses
        );

        $this->assertApiResponse($companyBuses);
    }

    /**
     * @test
     */
    public function test_read_company_buses()
    {
        $companyBuses = factory(CompanyBuses::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/company_buses/'.$companyBuses->id
        );

        $this->assertApiResponse($companyBuses->toArray());
    }

    /**
     * @test
     */
    public function test_update_company_buses()
    {
        $companyBuses = factory(CompanyBuses::class)->create();
        $editedCompanyBuses = factory(CompanyBuses::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/company_buses/'.$companyBuses->id,
            $editedCompanyBuses
        );

        $this->assertApiResponse($editedCompanyBuses);
    }

    /**
     * @test
     */
    public function test_delete_company_buses()
    {
        $companyBuses = factory(CompanyBuses::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/company_buses/'.$companyBuses->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/company_buses/'.$companyBuses->id
        );

        $this->response->assertStatus(404);
    }
}

<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Seats;

class SeatsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_seats()
    {
        $seats = factory(Seats::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/seats', $seats
        );

        $this->assertApiResponse($seats);
    }

    /**
     * @test
     */
    public function test_read_seats()
    {
        $seats = factory(Seats::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/seats/'.$seats->id
        );

        $this->assertApiResponse($seats->toArray());
    }

    /**
     * @test
     */
    public function test_update_seats()
    {
        $seats = factory(Seats::class)->create();
        $editedSeats = factory(Seats::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/seats/'.$seats->id,
            $editedSeats
        );

        $this->assertApiResponse($editedSeats);
    }

    /**
     * @test
     */
    public function test_delete_seats()
    {
        $seats = factory(Seats::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/seats/'.$seats->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/seats/'.$seats->id
        );

        $this->response->assertStatus(404);
    }
}

<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ProductQuantity;

class ProductQuantityApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_product_quantity()
    {
        $productQuantity = factory(ProductQuantity::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/product_quantities', $productQuantity
        );

        $this->assertApiResponse($productQuantity);
    }

    /**
     * @test
     */
    public function test_read_product_quantity()
    {
        $productQuantity = factory(ProductQuantity::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/product_quantities/'.$productQuantity->id
        );

        $this->assertApiResponse($productQuantity->toArray());
    }

    /**
     * @test
     */
    public function test_update_product_quantity()
    {
        $productQuantity = factory(ProductQuantity::class)->create();
        $editedProductQuantity = factory(ProductQuantity::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/product_quantities/'.$productQuantity->id,
            $editedProductQuantity
        );

        $this->assertApiResponse($editedProductQuantity);
    }

    /**
     * @test
     */
    public function test_delete_product_quantity()
    {
        $productQuantity = factory(ProductQuantity::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/product_quantities/'.$productQuantity->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/product_quantities/'.$productQuantity->id
        );

        $this->response->assertStatus(404);
    }
}

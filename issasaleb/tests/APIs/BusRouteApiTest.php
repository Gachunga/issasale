<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\BusRoute;

class BusRouteApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_bus_route()
    {
        $busRoute = factory(BusRoute::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/bus_routes', $busRoute
        );

        $this->assertApiResponse($busRoute);
    }

    /**
     * @test
     */
    public function test_read_bus_route()
    {
        $busRoute = factory(BusRoute::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/bus_routes/'.$busRoute->id
        );

        $this->assertApiResponse($busRoute->toArray());
    }

    /**
     * @test
     */
    public function test_update_bus_route()
    {
        $busRoute = factory(BusRoute::class)->create();
        $editedBusRoute = factory(BusRoute::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/bus_routes/'.$busRoute->id,
            $editedBusRoute
        );

        $this->assertApiResponse($editedBusRoute);
    }

    /**
     * @test
     */
    public function test_delete_bus_route()
    {
        $busRoute = factory(BusRoute::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/bus_routes/'.$busRoute->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/bus_routes/'.$busRoute->id
        );

        $this->response->assertStatus(404);
    }
}

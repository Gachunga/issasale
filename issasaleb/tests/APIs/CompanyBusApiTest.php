<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CompanyBus;

class CompanyBusApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_company_bus()
    {
        $companyBus = factory(CompanyBus::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/company_buses', $companyBus
        );

        $this->assertApiResponse($companyBus);
    }

    /**
     * @test
     */
    public function test_read_company_bus()
    {
        $companyBus = factory(CompanyBus::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/company_buses/'.$companyBus->id
        );

        $this->assertApiResponse($companyBus->toArray());
    }

    /**
     * @test
     */
    public function test_update_company_bus()
    {
        $companyBus = factory(CompanyBus::class)->create();
        $editedCompanyBus = factory(CompanyBus::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/company_buses/'.$companyBus->id,
            $editedCompanyBus
        );

        $this->assertApiResponse($editedCompanyBus);
    }

    /**
     * @test
     */
    public function test_delete_company_bus()
    {
        $companyBus = factory(CompanyBus::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/company_buses/'.$companyBus->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/company_buses/'.$companyBus->id
        );

        $this->response->assertStatus(404);
    }
}

<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Passenger;

class PassengerApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_passenger()
    {
        $passenger = factory(Passenger::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/passengers', $passenger
        );

        $this->assertApiResponse($passenger);
    }

    /**
     * @test
     */
    public function test_read_passenger()
    {
        $passenger = factory(Passenger::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/passengers/'.$passenger->id
        );

        $this->assertApiResponse($passenger->toArray());
    }

    /**
     * @test
     */
    public function test_update_passenger()
    {
        $passenger = factory(Passenger::class)->create();
        $editedPassenger = factory(Passenger::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/passengers/'.$passenger->id,
            $editedPassenger
        );

        $this->assertApiResponse($editedPassenger);
    }

    /**
     * @test
     */
    public function test_delete_passenger()
    {
        $passenger = factory(Passenger::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/passengers/'.$passenger->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/passengers/'.$passenger->id
        );

        $this->response->assertStatus(404);
    }
}

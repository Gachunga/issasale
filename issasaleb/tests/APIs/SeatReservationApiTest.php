<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SeatReservation;

class SeatReservationApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_seat_reservation()
    {
        $seatReservation = factory(SeatReservation::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/seat_reservations', $seatReservation
        );

        $this->assertApiResponse($seatReservation);
    }

    /**
     * @test
     */
    public function test_read_seat_reservation()
    {
        $seatReservation = factory(SeatReservation::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/seat_reservations/'.$seatReservation->id
        );

        $this->assertApiResponse($seatReservation->toArray());
    }

    /**
     * @test
     */
    public function test_update_seat_reservation()
    {
        $seatReservation = factory(SeatReservation::class)->create();
        $editedSeatReservation = factory(SeatReservation::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/seat_reservations/'.$seatReservation->id,
            $editedSeatReservation
        );

        $this->assertApiResponse($editedSeatReservation);
    }

    /**
     * @test
     */
    public function test_delete_seat_reservation()
    {
        $seatReservation = factory(SeatReservation::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/seat_reservations/'.$seatReservation->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/seat_reservations/'.$seatReservation->id
        );

        $this->response->assertStatus(404);
    }
}

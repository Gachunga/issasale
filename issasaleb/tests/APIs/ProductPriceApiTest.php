<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ProductPrice;

class ProductPriceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_product_price()
    {
        $productPrice = factory(ProductPrice::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/product_prices', $productPrice
        );

        $this->assertApiResponse($productPrice);
    }

    /**
     * @test
     */
    public function test_read_product_price()
    {
        $productPrice = factory(ProductPrice::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/product_prices/'.$productPrice->id
        );

        $this->assertApiResponse($productPrice->toArray());
    }

    /**
     * @test
     */
    public function test_update_product_price()
    {
        $productPrice = factory(ProductPrice::class)->create();
        $editedProductPrice = factory(ProductPrice::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/product_prices/'.$productPrice->id,
            $editedProductPrice
        );

        $this->assertApiResponse($editedProductPrice);
    }

    /**
     * @test
     */
    public function test_delete_product_price()
    {
        $productPrice = factory(ProductPrice::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/product_prices/'.$productPrice->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/product_prices/'.$productPrice->id
        );

        $this->response->assertStatus(404);
    }
}

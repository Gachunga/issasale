<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DiscountType;

class DiscountTypeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_discount_type()
    {
        $discountType = factory(DiscountType::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/discount_types', $discountType
        );

        $this->assertApiResponse($discountType);
    }

    /**
     * @test
     */
    public function test_read_discount_type()
    {
        $discountType = factory(DiscountType::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/discount_types/'.$discountType->id
        );

        $this->assertApiResponse($discountType->toArray());
    }

    /**
     * @test
     */
    public function test_update_discount_type()
    {
        $discountType = factory(DiscountType::class)->create();
        $editedDiscountType = factory(DiscountType::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/discount_types/'.$discountType->id,
            $editedDiscountType
        );

        $this->assertApiResponse($editedDiscountType);
    }

    /**
     * @test
     */
    public function test_delete_discount_type()
    {
        $discountType = factory(DiscountType::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/discount_types/'.$discountType->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/discount_types/'.$discountType->id
        );

        $this->response->assertStatus(404);
    }
}

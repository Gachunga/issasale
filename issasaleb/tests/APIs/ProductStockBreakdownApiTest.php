<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ProductStockBreakdown;

class ProductStockBreakdownApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_product_stock_breakdown()
    {
        $productStockBreakdown = factory(ProductStockBreakdown::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/product_stock_breakdowns', $productStockBreakdown
        );

        $this->assertApiResponse($productStockBreakdown);
    }

    /**
     * @test
     */
    public function test_read_product_stock_breakdown()
    {
        $productStockBreakdown = factory(ProductStockBreakdown::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/product_stock_breakdowns/'.$productStockBreakdown->id
        );

        $this->assertApiResponse($productStockBreakdown->toArray());
    }

    /**
     * @test
     */
    public function test_update_product_stock_breakdown()
    {
        $productStockBreakdown = factory(ProductStockBreakdown::class)->create();
        $editedProductStockBreakdown = factory(ProductStockBreakdown::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/product_stock_breakdowns/'.$productStockBreakdown->id,
            $editedProductStockBreakdown
        );

        $this->assertApiResponse($editedProductStockBreakdown);
    }

    /**
     * @test
     */
    public function test_delete_product_stock_breakdown()
    {
        $productStockBreakdown = factory(ProductStockBreakdown::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/product_stock_breakdowns/'.$productStockBreakdown->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/product_stock_breakdowns/'.$productStockBreakdown->id
        );

        $this->response->assertStatus(404);
    }
}

<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TodaysDeal;

class TodaysDealApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_todays_deal()
    {
        $todaysDeal = factory(TodaysDeal::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/todays_deals', $todaysDeal
        );

        $this->assertApiResponse($todaysDeal);
    }

    /**
     * @test
     */
    public function test_read_todays_deal()
    {
        $todaysDeal = factory(TodaysDeal::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/todays_deals/'.$todaysDeal->id
        );

        $this->assertApiResponse($todaysDeal->toArray());
    }

    /**
     * @test
     */
    public function test_update_todays_deal()
    {
        $todaysDeal = factory(TodaysDeal::class)->create();
        $editedTodaysDeal = factory(TodaysDeal::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/todays_deals/'.$todaysDeal->id,
            $editedTodaysDeal
        );

        $this->assertApiResponse($editedTodaysDeal);
    }

    /**
     * @test
     */
    public function test_delete_todays_deal()
    {
        $todaysDeal = factory(TodaysDeal::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/todays_deals/'.$todaysDeal->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/todays_deals/'.$todaysDeal->id
        );

        $this->response->assertStatus(404);
    }
}

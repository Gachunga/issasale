<!-- Bus Id Field -->
<div class="form-group">
    {!! Form::label('bus_id', 'Bus Id:') !!}
    <p>{{ $busRoute->bus_id }}</p>
</div>

<!-- Route Id Field -->
<div class="form-group">
    {!! Form::label('route_id', 'Route Id:') !!}
    <p>{{ $busRoute->route_id }}</p>
</div>

<!-- Departs Field -->
<div class="form-group">
    {!! Form::label('departs', 'Departs:') !!}
    <p>{{ $busRoute->departs }}</p>
</div>

<!-- Arrival Field -->
<div class="form-group">
    {!! Form::label('arrival', 'Arrival:') !!}
    <p>{{ $busRoute->arrival }}</p>
</div>

<!-- Recurring Field -->
<div class="form-group">
    {!! Form::label('recurring', 'Recurring:') !!}
    <p>{{ $busRoute->recurring }}</p>
</div>

<!-- Cost Field -->
<div class="form-group">
    {!! Form::label('cost', 'Cost:') !!}
    <p>{{ $busRoute->cost }}</p>
</div>

<!-- Currency Field -->
<div class="form-group">
    {!! Form::label('currency', 'Currency:') !!}
    <p>{{ $busRoute->currency }}</p>
</div>

<!-- Desc Field -->
<div class="form-group">
    {!! Form::label('desc', 'Desc:') !!}
    <p>{{ $busRoute->desc }}</p>
</div>


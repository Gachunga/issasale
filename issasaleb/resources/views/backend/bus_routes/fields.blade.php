<!-- Bus Id Field -->
<?php
use App\Models\CompanyBus;
use App\Models\Route;
?>
<div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('bus_id', 'Bus :') !!}
    {!! Form::select('bus_id', CompanyBus::pluck('name', 'id'), null, ['placeholder' => 'Pick a bus...', 'class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('route_id', 'Route :') !!}
    {!! Form::select('route_id', Route::query()
    ->join('locations as l1', 'routes.from_location_id', '=', 'l1.id')
    ->join('locations as l2', 'routes.to_location_id', '=', 'l2.id')
    ->select([
        'routes.id as id',
        DB::raw("CONCAT(l1.name, ' - ', l2.name) as name"),
    ])
    ->pluck('name','id'), null, ['placeholder' => 'Pick a route...', 'class' => 'form-control']) !!}
</div>
</div>
<div class="row">
<!-- Departs Field -->
<div class="form-group col-sm-6">
    {!! Form::label('departs', 'Departs:') !!}
    {!! Form::datetime('departs', null, ['class' => 'form-control','id'=>'departs']) !!}
</div>
@section('scripts')
   <script type="text/javascript">
            console.log("datetime")
           $('#departs').datetimepicker({
               format: 'LT',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endsection

<!-- Recurring Field -->
<div class="form-group col-sm-6">
    {!! Form::label('desc', 'Description:') !!}
    {!! Form::text('desc', null, ['class' => 'form-control']) !!}
</div>
</div>

<div class="row">
@foreach ($seat_types as $seat_type)
<!-- Desc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('seat_type', "$seat_type->name price:") !!}
    {!! Form::text("seat_type[$seat_type->id]", null, ['class' => 'form-control']) !!}
</div>
@endforeach
</div>
<div class="row">
<div class="form-group col-sm-6">
    @foreach ($days as $day)
    {!! Form::label("days[$day->id]", " $day->day:") !!}
    <label class="checkbox-inline">
        {!! Form::hidden("days[$day->id]", 0) !!}
        {!! Form::checkbox("days[$day->id]", '1', false) !!}
    </label>
    <br>
    @endforeach
</div>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.busRoutes.index') }}" class="btn btn-secondary">Cancel</a>
</div>


<!-- Bus Route Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bus_route_id', 'Bus Route Id:') !!}
    {!! Form::number('bus_route_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Day Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('day_id', 'Day Id:') !!}
    {!! Form::number('day_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('busRouteDays.index') }}" class="btn btn-secondary">Cancel</a>
</div>

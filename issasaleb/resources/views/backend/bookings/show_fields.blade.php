<!-- Bus Route Id Field -->
<div class="form-group">
    {!! Form::label('bus_route_id', 'Bus Route Id:') !!}
    <p>{{ $bookings->bus_route_id }}</p>
</div>

<!-- Payment Method Id Field -->
<div class="form-group">
    {!! Form::label('payment_method_id', 'Payment Method Id:') !!}
    <p>{{ $bookings->payment_method_id }}</p>
</div>

<!-- Total Amount Field -->
<div class="form-group">
    {!! Form::label('total_amount', 'Total Amount:') !!}
    <p>{{ $bookings->total_amount }}</p>
</div>

<!-- Paid Field -->
<div class="form-group">
    {!! Form::label('paid', 'Paid:') !!}
    <p>{{ $bookings->paid }}</p>
</div>

<!-- Payment Phone Field -->
<div class="form-group">
    {!! Form::label('payment_phone', 'Payment Phone:') !!}
    <p>{{ $bookings->payment_phone }}</p>
</div>


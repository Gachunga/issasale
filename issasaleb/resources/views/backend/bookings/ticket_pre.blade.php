

{{-- <link href="{{ public_path('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> --}}
{{-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
<!------ Include the above in your HEAD tag ---------->
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="row p-5">
                        <div class="col-md-6">
                            <p class="font-weight-bold mb-1">From</p>
                        <p class="text-muted">{{$booking->busRoute->route->fromLocation->name}}</p>
                        </div>

                        <div class="col-md-6 text-right">
                            <p class="font-weight-bold mb-1">To</p>
                            <p class="text-muted">{{$booking->busRoute->route->toLocation->name}}</p>
                        </div>
                    </div>

                    <hr class="my-5">

                    <div class="row pb-5 p-5">
                        <div class="col-md-6">
                            <p class="font-weight-bold mb-4">Bus Name</p>
                            <p class="mb-1">{{$booking->busRoute->bus->name}}</p>
                            <p class="font-weight-bold mb-4">Ticket(s)</p>
                            @foreach ($booking->tickets as $ticket)
                                <p class="mb-1">{{$ticket->ticket_ref}}</p>
                            @endforeach
                            </div>

                        <div class="col-md-6 text-right">
                            <p class="font-weight-bold mb-4">Departure Time</p>
                            <p class="mb-1"> {{$booking->busRoute->departs}}</p>
                            <p class="font-weight-bold mb-4">Seat No(s)</p>
                                @foreach ($booking->tickets as $ticket)
                                    <p class="mb-1">{{$ticket->seat->seat_no}}</p>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="row p-5">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="border-0 text-uppercase small font-weight-bold">Ticket Ref</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Name</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Phone</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">ID No</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Quantity</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Unit Cost</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($booking->tickets as $ticket)
                                    <tr>
                                    <td>{{$ticket->ticket_ref}}</td>
                                    <td>{{$ticket->passenger->name}}</td>
                                    <td>{{$ticket->passenger->phone}}</td>
                                    <td>{{$ticket->passenger->national_id}}</td>
                                    <td>1</td>
                                    <td>{{$ticket->amount}}</td>
                                    <td>{{$ticket->amount}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="d-flex flex-row-reverse bg-dark text-white p-4">
                        {{-- <div class="py-3 px-5 text-right">
                            <div class="mb-2">Grand Total</div>
                            <div class="h2 font-weight-light">{{$booking->amount}}</div>
                        </div> --}}

                        {{-- <div class="py-3 px-5 text-right">
                            <div class="mb-2">Discount</div>
                            <div class="h2 font-weight-light">0%</div>
                        </div> --}}

                        <div class="py-3 px-5 text-right">
                            <div class="mb-2"> Total amount</div>
                            <div class="h2 font-weight-light">{{$booking->total_amount}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php ?>

</div>

<style>

    html, body{
        height: 100%;
        margin: 0;
        font-size: 10pt;

        /*position: fixed;*/

    }
    .container
    {

    /*width: 90%;*/
    margin-left:5%;
    margin-right:5%;
    min-height: 100%;
    margin-bottom: -50px;
    }
/*			@page {
        header: page-header;
        footer: page-footer;
    }

#page-header { position: fixed; top: -50px; left: 0px; right: 0px; height: 50px; background-color: orange; padding: .5em; text-align: center; }
*/
    table {
        border-collapse: collapse;
        width: 100%;
        position: inherit;

    }
/*	tr:nth-child(even){
        background-color: #f2f2f2;
    }*/
    th, td {
        text-align: left;
        padding: 3px;
        font-size: 11px;

    }

    tr {
        page-break-inside: avoid;
    }

    .push, .footer{
        position: fixed;
        height: 250px;
        bottom: 0;
        width: 100%;
    }
    /*tr:nth-child(even){background-color: #f2f2f2}*/
    /*h3   {color: blue;}*/
</style>

<!-- Bus Route Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bus_route_id', 'Bus Route Id:') !!}
    {!! Form::number('bus_route_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Method Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_method_id', 'Payment Method Id:') !!}
    {!! Form::number('payment_method_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total_amount', 'Total Amount:') !!}
    {!! Form::number('total_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Paid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('paid', 'Paid:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('paid', 0) !!}
        {!! Form::checkbox('paid', '1', null) !!}
    </label>
</div>


<!-- Payment Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_phone', 'Payment Phone:') !!}
    {!! Form::text('payment_phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('bookings.index') }}" class="btn btn-secondary">Cancel</a>
</div>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>A simple, clean, and responsive HTML invoice template</title>

    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }

    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }

    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }

    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }

    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }

    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }

    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }

    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.item.last td {
        border-bottom: none;
    }

    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }

    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }

        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }

    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }

    .rtl table {
        text-align: right;
    }

    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td >
                                {{-- <img src="https://www.sparksuite.com/images/logo.png" style="width:100%; max-width:300px;"> --}}
                                From<br>
                                {{$booking->busRoute->route->fromLocation->name}}
                            </td>
                        </tr>
                    </table>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <table>
                        <tr>
                            <td>
                                To<br>
                                {{$booking->busRoute->route->toLocation->name}}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <p class="font-weight-bold mb-4">Bus Name</p>
                                <p class="mb-1">{{$booking->busRoute->bus->name}}</p>
                                <p class="font-weight-bold mb-4">Ticket(s)</p>
                                @foreach ($booking->tickets as $ticket)
                                    <p class="mb-1">{{$ticket->ticket_ref}}</p>
                                @endforeach
                            </td>
                        </tr>
                    </table>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <table>
                        <tr>

                            <td>
                                <p class="font-weight-bold mb-4">Departure Time</p>
                                <p class="mb-1"> {{$booking->busRoute->departs}}</p>
                                <p class="font-weight-bold mb-4">Seat No(s)</p>
                                    @foreach ($booking->tickets as $ticket)
                                        <p class="mb-1">{{$ticket->seat->seat_no}}</p>
                                    @endforeach
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            {{-- <tr class="heading">
                <td>
                    Payment Method
                </td>

                <td>
                    Check #
                </td>
            </tr>

            <tr class="details">
                <td>
                    Check
                </td>

                <td>
                    1000
                </td>
            </tr> --}}

            <tr class="heading">
                    <td >Ticket Ref</td>
                    <td class="border-0 text-uppercase small font-weight-bold">Name</td>
                    <td class="border-0 text-uppercase small font-weight-bold">Phone</td>
                    <td class="border-0 text-uppercase small font-weight-bold">ID No</td>
                    <td class="border-0 text-uppercase small font-weight-bold">Quantity</td>
                    <td class="border-0 text-uppercase small font-weight-bold">Unit Cost</td>
                    <td class="border-0 text-uppercase small font-weight-bold">Total</td>
            </tr>

                @foreach ($booking->tickets as $ticket)
                <tr class="item">
                <td>{{$ticket->ticket_ref}}</td>
                <td>{{$ticket->passenger->name}}</td>
                <td>{{$ticket->passenger->phone}}</td>
                <td>{{$ticket->passenger->national_id}}</td>
                <td>1</td>
                <td>{{$ticket->amount}}</td>
                <td>{{$ticket->amount}}</td>
                </tr>
                @endforeach

            <tr class="heading">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                   Total: Ksh : {{$booking->total_amount}}
                </td>
            </tr>
        </table>
    </div>
</body>
</html>

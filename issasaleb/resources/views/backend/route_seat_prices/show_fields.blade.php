<!-- Bus Route Id Field -->
<div class="form-group">
    {!! Form::label('bus_route_id', 'Bus Route Id:') !!}
    <p>{{ $routeSeatPrice->bus_route_id }}</p>
</div>

<!-- Seat Type Id Field -->
<div class="form-group">
    {!! Form::label('seat_type_id', 'Seat Type Id:') !!}
    <p>{{ $routeSeatPrice->seat_type_id }}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $routeSeatPrice->price }}</p>
</div>


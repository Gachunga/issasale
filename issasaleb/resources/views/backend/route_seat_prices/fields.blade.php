<!-- Bus Route Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bus_route_id', 'Bus Route Id:') !!}
    {!! Form::number('bus_route_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Seat Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('seat_type_id', 'Seat Type Id:') !!}
    {!! Form::number('seat_type_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('routeSeatPrices.index') }}" class="btn btn-secondary">Cancel</a>
</div>

@extends('backend.layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('admin.companyBuses.index') !!}">Company Bus</a>
      </li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create Company Bus</strong>
                            </div>
                            <div class="card-body">
                                <div class="row mb-4">
                                    <div class="col">
                                        <seat-component></seat-component>
                                    </div><!--col-->
                                </div><!--row-->

                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection

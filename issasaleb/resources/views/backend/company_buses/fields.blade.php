<?php use App\Models\Company; ?>
<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Desc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('desc', 'Desc:') !!}
    {!! Form::text('desc', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
{{-- <div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::number('type', null, ['class' => 'form-control']) !!}
</div> --}}

<!-- Company Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('company', 'Company :') !!}
    {!! Form::select('company_id', Company::pluck('name', 'id'), null, ['placeholder' => 'Pick a company...', 'class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.companyBuses.index') }}" class="btn btn-secondary">Cancel</a>
</div>

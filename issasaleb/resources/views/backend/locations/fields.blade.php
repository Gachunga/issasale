<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<!-- Desc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pick_up_location', 'Pick up Location:') !!}
    {!! Form::text('pick_up_location', null, ['class' => 'form-control']) !!}
</div>
<!-- Desc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('desc', 'Description:') !!}
    {!! Form::text('desc', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.locations.index') }}" class="btn btn-secondary">Cancel</a>
</div>

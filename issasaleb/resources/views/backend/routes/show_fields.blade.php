<!-- From Location Id Field -->
<div class="form-group">
    {!! Form::label('from_location_id', 'From Location Id:') !!}
    <p>{{ $route->from_location_id }}</p>
</div>

<!-- To Location Id Field -->
<div class="form-group">
    {!! Form::label('to_location_id', 'To Location Id:') !!}
    <p>{{ $route->to_location_id }}</p>
</div>

<!-- Desc Field -->
<div class="form-group">
    {!! Form::label('desc', 'Desc:') !!}
    <p>{{ $route->desc }}</p>
</div>

<!-- Company Id Field -->
<div class="form-group">
    {!! Form::label('company_id', 'Company Id:') !!}
    <p>{{ $route->company_id }}</p>
</div>


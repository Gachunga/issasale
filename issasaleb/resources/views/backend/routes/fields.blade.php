<?php
use App\Models\Company;
use App\Models\Location;
?>
<!-- From Location Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('from_location_id', 'From :') !!}
    {!! Form::select('from_location_id', Location::pluck('name', 'id'), null, ['placeholder' => 'Pick a location...', 'class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('to_location_id', 'To :') !!}
    {!! Form::select('to_location_id', Location::pluck('name', 'id'), null, ['placeholder' => 'Pick a location...', 'class' => 'form-control']) !!}
</div>
<!-- To Location Id Field -->

<!-- Desc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('desc', 'Desc:') !!}
    {!! Form::text('desc', null, ['class' => 'form-control']) !!}
</div>

{{-- <!-- Company Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('company', 'Company :') !!}
    {!! Form::select('company_id', Company::pluck('name', 'id'), null, ['placeholder' => 'Pick a company...', 'class' => 'form-control']) !!}
</div> --}}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.routes.index') }}" class="btn btn-secondary">Cancel</a>
</div>

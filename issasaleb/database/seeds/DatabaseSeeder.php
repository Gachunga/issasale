<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    use TruncateTable;
    use DisableForeignKeys;

    /**
     * Seed the application's database.
     */
    public function run()
    {
        Model::unguard();

        $this->disableForeignKeys();
        $this->truncateMultiple([
            'cache',
            'failed_jobs',
            'ledgers',
            'jobs',
            'sessions',
            'discount_types',
            'payment_methods',
            'categories'
        ]);

        $this->call(AuthTableSeeder::class);
        $this->call(DiscountTypesTableSeeder::class);
        $this->call(PaymentMethodsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(StatusesTableSeeder::class);

        Model::reguard();
        $this->enableForeignKeys();
    }
}

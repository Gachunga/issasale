<?php

use App\Models\PaymentMethod;
use App\Models\Status;
use Illuminate\Database\Seeder;

/**
 * Class PermissionRoleTableSeeder.
 */
class StatusesTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Create Roles
        Status::create(['title' => 'Pending ', 'is_active' => 1]);
        Status::create(['title' => 'Awaiting Payment' , 'is_active' => 1]);
        Status::create(['title' => 'Awaiting Fulfillment' , 'is_active' => 1]);
        Status::create(['title' => 'Awaiting Shipment' , 'is_active' => 1]);
        Status::create(['title' => 'Awaiting Pickup' , 'is_active' => 1]);
        Status::create(['title' => 'Partially Shipped' , 'is_active' => 1]);
        Status::create(['title' => 'Completed' , 'is_active' => 1]);
        Status::create(['title' => 'Shipped' , 'is_active' => 1]);
        Status::create(['title' => 'Cancelled' , 'is_active' => 1]);
        Status::create(['title' => 'Declined' , 'is_active' => 1]);
        Status::create(['title' => 'Refunded' , 'is_active' => 1]);
        Status::create(['title' => 'Disputed' , 'is_active' => 1]);
        Status::create(['title' => 'Manual Verification Required' , 'is_active' => 1]);
        Status::create(['title' => 'Partially Refunded' , 'is_active' => 1]);

        $this->enableForeignKeys();
    }
}

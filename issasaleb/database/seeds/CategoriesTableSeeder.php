<?php

use App\Models\Category;
use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;

/**
 * Class PermissionRoleTableSeeder.
 */
class CategoriesTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Create Roles
        Category::create(['title' => 'all']);

        $this->enableForeignKeys();
    }
}

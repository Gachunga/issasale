<?php

use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;

/**
 * Class PermissionRoleTableSeeder.
 */
class PaymentMethodsTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Create Roles
        PaymentMethod::create(['title' => 'mpesa', 'is_active' => 1]);
        PaymentMethod::create(['title' => 'cash' , 'is_active' => 1]);
        PaymentMethod::create(['title' => 'card' , 'is_active' => 1]);

        $this->enableForeignKeys();
    }
}

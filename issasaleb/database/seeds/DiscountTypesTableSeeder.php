<?php

use App\Models\DiscountType;
use Illuminate\Database\Seeder;

/**
 * Class PermissionRoleTableSeeder.
 */
class DiscountTypesTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Create Roles
        DiscountType::create(['name' => 'percentage']);
        DiscountType::create(['name' => 'cash']);

        $this->enableForeignKeys();
    }
}

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SeatReservation;
use Faker\Generator as Faker;

$factory->define(SeatReservation::class, function (Faker $faker) {

    return [
        'seat_id' => $faker->word,
        'reserved_at' => $faker->date('Y-m-d H:i:s'),
        'reserved_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

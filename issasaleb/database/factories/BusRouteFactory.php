<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BusRoute;
use Faker\Generator as Faker;

$factory->define(BusRoute::class, function (Faker $faker) {

    return [
        'bus_id' => $faker->word,
        'route_id' => $faker->word,
        'departs' => $faker->date('Y-m-d H:i:s'),
        'arrival' => $faker->date('Y-m-d H:i:s'),
        'recurring' => $faker->word,
        'cost' => $faker->randomDigitNotNull,
        'currency' => $faker->word,
        'desc' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

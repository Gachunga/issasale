<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SeatType;
use Faker\Generator as Faker;

$factory->define(SeatType::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'desc' => $faker->word,
        'active' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

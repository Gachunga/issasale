<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Seats;
use Faker\Generator as Faker;

$factory->define(Seats::class, function (Faker $faker) {

    return [
        'bus_id' => $faker->word,
        'row_no' => $faker->randomDigitNotNull,
        'col_no' => $faker->randomDigitNotNull,
        'seat_no' => $faker->word,
        'seat_price' => $faker->randomDigitNotNull,
        'is_business' => $faker->word,
        'has_seat' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

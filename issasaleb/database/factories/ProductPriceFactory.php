<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProductPrice;
use Faker\Generator as Faker;

$factory->define(ProductPrice::class, function (Faker $faker) {

    return [
        'product_id' => $faker->randomDigitNotNull,
        'previous_price' => $faker->randomDigitNotNull,
        'price' => $faker->randomDigitNotNull,
        'discount_type_id' => $faker->randomDigitNotNull,
        'discount' => $faker->randomDigitNotNull,
        'active' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

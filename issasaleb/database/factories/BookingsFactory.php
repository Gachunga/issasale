<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Bookings;
use Faker\Generator as Faker;

$factory->define(Bookings::class, function (Faker $faker) {

    return [
        'bus_route_id' => $faker->word,
        'payment_method_id' => $faker->word,
        'total_amount' => $faker->randomDigitNotNull,
        'paid' => $faker->word,
        'payment_phone' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

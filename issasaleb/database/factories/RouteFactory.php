<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Route;
use Faker\Generator as Faker;

$factory->define(Route::class, function (Faker $faker) {

    return [
        'from_location_id' => $faker->word,
        'to_location_id' => $faker->word,
        'desc' => $faker->word,
        'company_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

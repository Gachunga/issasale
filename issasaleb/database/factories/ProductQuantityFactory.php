<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProductQuantity;
use Faker\Generator as Faker;

$factory->define(ProductQuantity::class, function (Faker $faker) {

    return [
        'product_id' => $faker->randomDigitNotNull,
        'quantity' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

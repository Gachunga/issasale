<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProductColor;
use Faker\Generator as Faker;

$factory->define(ProductColor::class, function (Faker $faker) {

    return [
        'product_id' => $faker->randomDigitNotNull,
        'color' => $faker->word,
        'name' => $faker->word,
        'available' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

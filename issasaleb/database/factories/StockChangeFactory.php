<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\StockChange;
use Faker\Generator as Faker;

$factory->define(StockChange::class, function (Faker $faker) {

    return [
        'product_id' => $faker->randomDigitNotNull,
        'action' => $faker->word,
        'initial' => $faker->randomDigitNotNull,
        'quantity' => $faker->randomDigitNotNull,
        'final' => $faker->randomDigitNotNull,
        'user_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

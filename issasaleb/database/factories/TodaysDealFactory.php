<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TodaysDeal;
use Faker\Generator as Faker;

$factory->define(TodaysDeal::class, function (Faker $faker) {

    return [
        'product_id' => $faker->word,
        'product_image_id' => $faker->word,
        'discount_type_id' => $faker->word,
        'discount' => $faker->randomDigitNotNull,
        'date' => $faker->word,
        'background_color' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

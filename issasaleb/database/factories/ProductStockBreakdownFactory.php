<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProductStockBreakdown;
use Faker\Generator as Faker;

$factory->define(ProductStockBreakdown::class, function (Faker $faker) {

    return [
        'product_quantity_id' => $faker->word,
        'product_size_id' => $faker->word,
        'product_color_id' => $faker->word,
        'quantity' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BusRouteDay;
use Faker\Generator as Faker;

$factory->define(BusRouteDay::class, function (Faker $faker) {

    return [
        'bus_route_id' => $faker->word,
        'day_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RouteSeatPrice;
use Faker\Generator as Faker;

$factory->define(RouteSeatPrice::class, function (Faker $faker) {

    return [
        'bus_route_id' => $faker->word,
        'seat_type_id' => $faker->randomDigitNotNull,
        'price' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SliderImage;
use Faker\Generator as Faker;

$factory->define(SliderImage::class, function (Faker $faker) {

    return [
        'title_caption' => $faker->word,
        'title' => $faker->word,
        'title_desc' => $faker->word,
        'img_url' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

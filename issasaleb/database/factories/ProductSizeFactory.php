<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProductSize;
use Faker\Generator as Faker;

$factory->define(ProductSize::class, function (Faker $faker) {

    return [
        'product_id' => $faker->word,
        'size' => $faker->word,
        'quantity' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

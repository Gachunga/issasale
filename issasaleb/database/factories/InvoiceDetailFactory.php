<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\InvoiceDetail;
use Faker\Generator as Faker;

$factory->define(InvoiceDetail::class, function (Faker $faker) {

    return [
        'invoice_id' => $faker->word,
        'product_id' => $faker->word,
        'size_id' => $faker->word,
        'color_id' => $faker->word,
        'quantity' => $faker->randomDigitNotNull,
        'discount' => $faker->randomDigitNotNull,
        'price' => $faker->randomDigitNotNull,
        'discounted_price' => $faker->randomDigitNotNull,
        'sub_total' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

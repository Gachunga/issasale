<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Invoice;
use Faker\Generator as Faker;

$factory->define(Invoice::class, function (Faker $faker) {

    return [
        'order_id' => $faker->word,
        'name' => $faker->word,
        'email' => $faker->word,
        'location' => $faker->word,
        'city' => $faker->word,
        'phone' => $faker->word,
        'status' => $faker->word,
        'is_order' => $faker->word,
        'is_paid' => $faker->word,
        'payment_method_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
